# Proyecto de grado - Facultad de Ingeniería - UdelaR

Este repositorio contiene las herramientas, informe final y códigos generados dentro del marco del Proyecto de Grado denominado "Soluciones interactivas para la danza".

Estudiantes:
+ Gonzalo Rosso
+ Ismael Pisano

Supervisores:
+ Dr. Ing. Daniel Calegari
+ Mag. Ewelina Bakala

## Resumen
Dado el incremento en la cantidad y calidad de sensores y actuadores que mejoran la interacción entre personas y computadoras, es que nos vemos tentados a explorar, investigar, e implementar nuevas herramientas que exploten la utilización de estos componentes. En estaocasión llevándolo al terreno del arte

El objetivo del proyecto es brindar, a artistas de la danza, una herramienta que permita autonomía a la hora de realizar obras donde se incluya la interacción con contenidos multimediaproyectados. Estos contenidos pueden estar enriquecidos mediante el uso de un sensor de pro-fundidad Kinect que permita identificar al artista y sus movimientos en escena. Por lo tanto,se permite generar contenidos dinámicos con los que se pueda interactuar en base a estosmovimientos.

## Contenido
+ **Informe**: documento donde se detalla todo lo relacionado al desarrollo del proyecto. Donde se analiza el estado del arte, se detalla la solución propuesta y desarrollada, se explica cada detalle del caso de estudio llevado a cabo y se resentan conclusiones sobre el resultado final.
+ **Manejador de escenas**: herramienta que permite crear una configuración mediante la creación de escenas, espacios de proyección y contenidos asociados a éstos últimos. Dentro del directorio Manejador de escena se encuentra:
    - Código fuente para Processing
    - Ejecutables: Linux, MacOS, Windows
+ **Caso de estudio**: obra desarrollada en conjunto con Espacio Pangea, utilizando las herramientas y soluciones desarrolladas. Asimismo, se añaden imágenes y videos de las pruebas y ejecución de la obra en cuestión.
+ **Contenidos**: se presentan ejempos de contenidos multimedia dentro de la carpeta Ejemplos de contenidos. A su vez, se detallan los pasos para permitir la adaptación de diferentes contenidos para ser utilizados por las herramientas desarrolladas.
+ **Herramienta de ejecución**: herramienta que permite la ejecución de una obra mediante la carga de un archivo de configuración generado con la herramienta Manejador de escenas.
