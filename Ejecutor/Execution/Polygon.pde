class Polygon implements Shape{

  PShape mPolygon;
  ArrayList<PVector> verticesArray = new ArrayList<PVector>();
  color mColor;

  Boolean selected;
  Integer selectedVertex;

  int VERTICES_RADIUS = 5;

  Polygon (color c) {
    mColor = c;
    selected = false;
    mPolygon = createShape();
  }

  int getColor(){
    return mColor;
  }

  String getType(){
    return "Polygon";
  }

  //Draw polygon
  PShape display() {
    return mPolygon;
  }
 //<>// //<>// //<>// //<>//
  PShape getVerticesGroup(){
    PShape group = createShape(GROUP);
    for (int i = 0; i < verticesArray.size(); i++) {

        //Create vertex circle
        ellipseMode(RADIUS);
        PShape elip = createShape(ELLIPSE, verticesArray.get(i).x, verticesArray.get(i).y, VERTICES_RADIUS, VERTICES_RADIUS);
        elip.setStroke(0);
        //if vertex is selected fill with red
        if (selectedVertex != null && selectedVertex == i) {
          elip.setFill(color(255, 0, 0));
        }

        //Draw vertex
        group.addChild(elip);
      }
    return group;
  }
  //Adds new vertex to polygon
  void addVertex(int mMouseX, int mMouseY) {

    // Create new polygon
    mPolygon = createShape(); //<>// //<>// //<>//
    mPolygon.beginShape();

    //Add new vertex to verticesArray
    PVector vertex = new PVector (mMouseX, mMouseY);
    verticesArray.add(vertex);

    //Add all vertices in verticesArray to polygon
    for (int i = 0; i < verticesArray.size(); i++) {
      mPolygon.vertex(verticesArray.get(i).x, verticesArray.get(i).y);
    };

    mPolygon.endShape(CLOSE);
    mPolygon.setFill(mColor);
    mPolygon.setStroke(color(255, 0, 0));
  }

  //Check if we clicked inside the ellipse
  Boolean inside(int mMouseX, int mMouseY,int c) {
    //If color in mouse position is equal to ellipse color, then is inside
    return c == mColor;
  }

  //Check if we clicked inside a vertex and set selectedVertex to vertex's index in vertexArray
  Boolean checkSelectedVertex(int mMouseX, int mMouseY) {
    selectedVertex = null;
    for (int i = 0; i < verticesArray.size(); i++) {
      if (dist(verticesArray.get(i).x, verticesArray.get(i).y, mMouseX, mMouseY) < (1.5)*VERTICES_RADIUS) {
        selectedVertex = i;
        return true;
      }
    }
    return false;
  }

  //Clones this Polygon
  Polygon clone() {

    //Create new Polygon with this color
    Polygon polygon = new Polygon(mColor);
    //Clone verticesArray
    polygon.verticesArray = (ArrayList) verticesArray.clone();

    //Create polygon
    polygon.mPolygon = createShape();
    polygon.mPolygon.beginShape();


    //Remove last added vertices as we used doble click to finish polygon shape, so an additional vertex was added
    polygon.verticesArray.remove(polygon.verticesArray.size() - 1);

    //Add all vertices in verticesArray to polygon
    for (int i = 0; i < polygon.verticesArray.size(); i++) {
      polygon.mPolygon.vertex(polygon.verticesArray.get(i).x, polygon.verticesArray.get(i).y);
    };

    polygon.mPolygon.endShape(CLOSE);
    polygon.mPolygon.setFill(mColor);
    polygon.mPolygon.setStroke(color(255, 0, 0));

    return polygon;
  }

  //Move polygon or selectedVertex
  void move(int x, int y, int pmx,int pmy) {

    //Check if moving selectedVertex
    if (selectedVertex == null) {

      //Move all vertices in mPolygon
      for (int i = 0; i < mPolygon.getVertexCount(); i++) {
        PVector newxy = new PVector (mPolygon.getVertex(i).x + (x - pmx), mPolygon.getVertex(i).y + (y - pmy));
        mPolygon.setVertex(i, newxy);
      }
      //Move all vertices in verticesArray
      for (int i = 0; i < verticesArray.size(); i++) {
        verticesArray.get(i).x = verticesArray.get(i).x + (x - pmx);
        verticesArray.get(i).y = verticesArray.get(i).y + (y - pmy);
      }
    }
    else {
      //Move vertex
      verticesArray.get(selectedVertex).x = verticesArray.get(selectedVertex).x + (x - pmx);
      verticesArray.get(selectedVertex).y = verticesArray.get(selectedVertex).y + (y - pmy);
      mPolygon.setVertex(selectedVertex, verticesArray.get(selectedVertex));
    }
  }
  void moveStep(int x,int y){

    if (selectedVertex == null) {
      //Move all vertices in mPolygon
      for (int i = 0; i < mPolygon.getVertexCount(); i++) {
        PVector newxy = new PVector (mPolygon.getVertex(i).x + x, mPolygon.getVertex(i).y + y);
        mPolygon.setVertex(i, newxy);
      }
      //Move all vertices in verticesArray
      for (int i = 0; i < verticesArray.size(); i++) {
        verticesArray.get(i).x = verticesArray.get(i).x + x;
        verticesArray.get(i).y = verticesArray.get(i).y + y;
      }
    }
    else{

    }
  }

  //Set if polygon is selected
  void setSelected(Boolean sel) {
    selected = sel;
  }

  //Moves the selectedVertex
  void moveSelectedVertex(int direction) {
    if (selectedVertex != null) {

      //Update selectedVertex in verticesArray
      switch(direction) {
        case DOWN:
        verticesArray.get(selectedVertex).y++;
        break;
        case UP:
        verticesArray.get(selectedVertex).y--;
        break;
        case LEFT:
        verticesArray.get(selectedVertex).x--;
        break;
        case RIGHT:
        verticesArray.get(selectedVertex).x++;
        break;
      }

      //Updates selectedVertex in polygon
      mPolygon.setVertex(selectedVertex, verticesArray.get(selectedVertex));
    }
  }

  JSONArray getCoords(){
    JSONArray coords = new JSONArray();

    //Add vertices to coords
    for (int i = 0; i < verticesArray.size(); i++) {
      JSONObject vertexCoords = new JSONObject();
      vertexCoords.setFloat("x", verticesArray.get(i).x);
      vertexCoords.setFloat("y", verticesArray.get(i).y);
      coords.append(vertexCoords);
    }
    return coords;
  }

  ArrayList<PVector> getVerticesArray(){
    return verticesArray;
  }

  //gets the max X coord
 int getMaxX(){
   int maxX = 0;
   for (int i = 0; i < verticesArray.size(); i++) {
     maxX = (int) max(maxX, verticesArray.get(i).x);
   }
   return maxX;
 }

 //gets the min X coord
 int getMinX(){
   int minX = 100000;
   for (int i = 0; i < verticesArray.size(); i++) {
     minX = (int) min(minX, verticesArray.get(i).x);
   }
   return minX;
 }

 //gets the max Y coord
 int getMaxY(){
   int maxY = 0;
   for (int i = 0; i < verticesArray.size(); i++) {
     maxY = (int) max(maxY, verticesArray.get(i).y);
   }
   return maxY;
 }

 //gets the min Y coord
 int getMinY(){
   int minY = 100000;
   for (int i = 0; i < verticesArray.size(); i++) {
     minY = (int) min(minY, verticesArray.get(i).y);
   }
   return minY;
 }
}
