class Screen {
    String name;
    int canvasWidth, canvasHeight, finalImageWidth, finalImageHeight;
    float  positionX, positionY, scale;
    PApplet parent;
    PVector contentTopLeft;
    PVector contentBottomRight;
    String resizeMode = "Cover"; // Contain //Noresize
    String mask;
    String contentClass = "ContentVideo";
    PImage maskImage;
    PGraphics imageInCanvas;
    boolean adjustedByWidth = false;
    ContentView content;
    ArrayList<Shape> shapes;
    Ellipse ellipse;
    Polygon polygon;
    Kinect kinect;

    Screen(PApplet parent, String name, int canvasWidth, int canvasHeight, Kinect kinect) {
        this.parent = parent;
        this.name = name;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.shapes = new ArrayList<Shape>();
        this.ellipse = null;
        this.polygon = null;
        this.scale = -1;
        this.kinect = kinect;
    }

    public void setContentTopLeft(int x, int y) {
        this.contentTopLeft = new PVector(x,y);
    }

    public void setContentBottomRight(int x, int y) {
        this.contentBottomRight = new PVector(x,y);
    }

    public void setMask(String _mask) {
        this.mask = _mask;
        maskImage = loadImage(this.mask);
    }

    public void setContentClass(String cc) {
        this.contentClass = cc;
        switch (this.getContentClass()){
            case "ContentBalls":
            content = new ContentBalls(this.parent,this.canvasWidth, this.canvasHeight);
            break;
            case "ContentVideoStars":
            content = new ContentVideoStars(this.parent);
            break;
            case "ContentVideoBunny":
            content = new ContentVideoBunny(this.parent);
            break;
            case "ContentVideoShow":
            content = new ContentVideoShow(this.parent);
            break;
            case "ContentFluid":
            if (kinect != null) {
              content = new ContentFluid(this.parent, kinect);
            } else {
              println("No kinect found");
            }
            break;
            case "ContentMePicture":
            if (kinect != null) {
              content = new ContentMePicture(this.parent, kinect);
            } else {
              println("No kinect found");
            }
            break;
            case "ContentImage":
            content = new ContentImage(this.parent);
            break;
        }
    }

    public void setResizeMode(String rm) {
        this.resizeMode = rm;
    }

    String getName( ){
        return this.name;
    }

    String getContentClass() {
        println(this.contentClass);
        return this.contentClass;
    }

    String getResizeMode() {
        return resizeMode;
    }

    PVector getPosition(){
        return contentTopLeft;
    }

    public void start(){
        this.content.play();
    }
    public void stop(){
        this.content.stop();
    }
    PImage draw() {
        PImage contentImage = content.draw();
        if (this.scale == -1 && contentImage.width > 0 && contentImage.height > 0){
            //Compute scale only once
            this.adjustedByWidth = computeScale(contentImage);
            print("Adjust:");
            println(adjustedByWidth);
        }
        if (!this.resizeMode.equals("Noresize")){
            resizeImage(contentImage);
        }

        this.imageInCanvas = createGraphics(int(contentBottomRight.x - contentTopLeft.x),int(contentBottomRight.y - contentTopLeft.y));

        imageInCanvas.beginDraw();
        this.imageInCanvas.background(0,255);
        switch(this.resizeMode){
            case "Cover":
            imageInCanvas.imageMode(CENTER);
            imageInCanvas.image(contentImage, (contentBottomRight.x - contentTopLeft.x)/2, (contentBottomRight.y - contentTopLeft.y)/2);
            break;
            case "Contain":
            imageInCanvas.imageMode(CENTER);
            imageInCanvas.image(contentImage, (contentBottomRight.x - contentTopLeft.x)/2, (contentBottomRight.y - contentTopLeft.y)/2);
            break;
            default:
            imageInCanvas.imageMode(CORNER);
            imageInCanvas.image(contentImage, 0, 0);
            break;
        }
        imageInCanvas.endDraw();
        imageInCanvas.mask(maskImage);
        return imageInCanvas;
    }

    void resizeImage(PImage image){
        if (image.width > 0 && image.height > 0){
            image.resize(this.finalImageWidth, this.finalImageHeight);
        }
    }

    void computePosition(PImage image){
        positionX = contentTopLeft.x;
        positionY = contentTopLeft.y;
        switch(this.resizeMode) {
            case "Cover":
            if (adjustedByWidth) {
                positionY = contentTopLeft.y - (image.height - (contentBottomRight.y - contentTopLeft.y))/2;
            }
            else {
                positionX = contentTopLeft.x - (image.width - (contentBottomRight.x - contentTopLeft.x))/2;
            }
            break;
            case "Contain":
            if (adjustedByWidth) {
                positionY =  contentTopLeft.y + (((contentBottomRight.y - contentTopLeft.y) - image.height)/2);
            }
            else {
                positionX =  contentTopLeft.x + (((contentBottomRight.x - contentTopLeft.x) - image.width)/2);
            }
            break;
        }
    }
 //<>//
    boolean computeScale(PImage image){
        float boundingBoxWidth = this.contentBottomRight.x - this.contentTopLeft.x; //<>// //<>//
        float boundingBoxHeight = this.contentBottomRight.y - this.contentTopLeft.y;
        float boundingBoxRatio = boundingBoxWidth / boundingBoxHeight;

        float imageRatio = (float)image.width / (float)image.height;

        switch(this.resizeMode) {
            case "Cover":
            if (boundingBoxRatio > imageRatio) {
                this.scale = boundingBoxWidth / image.width;
                } else {
                    this.scale = boundingBoxHeight / image.height;
                }
                break;
                case "Contain":
                if (boundingBoxRatio > imageRatio) {
                    this.scale =  boundingBoxHeight / image.height;
                    } else {
                        this.scale = boundingBoxWidth / image.width;
                    }
                    break;
                    case "Noresize":
                    this.scale = 1;
                    break;
                }

                this.finalImageWidth = (int)Math.floor(image.width * this.scale);
                this.finalImageHeight = (int)Math.floor(image.height * this.scale);

                return false;
            }
            public void setShapes(JSONArray _shapes){
                println("Setting shapes");
                for (int i = 0; i < _shapes.size(); i++) {

                    JSONObject _s = (JSONObject) _shapes.getJSONObject(i);
                    JSONArray coords = _s.getJSONArray("coords");
                    int c = _s.getInt("color");
                    switch(_s.getString("shapeType")){
                        case "Ellipse":
                        int x,y,x1,y1;
                        x = coords.getJSONObject(0).getInt("x");
                        y = coords.getJSONObject(0).getInt("y");
                        x1 = coords.getJSONObject(1).getInt("x");
                        y1 = coords.getJSONObject(1).getInt("y");

                        this.ellipse = new Ellipse(c, x, y);
                        this.ellipse.setBottomRight(x1, y1);
                        this.shapes.add(this.ellipse);
                        break;
                        case "Polygon":
                        this.polygon = new Polygon(c);
                        shapes.add(this.polygon);
                        for(int v = 0; v < coords.size(); v++){
                            this.polygon.addVertex(coords.getJSONObject(v).getInt("x"), coords.getJSONObject(v).getInt("y"));
                        };
                        break;
                    };
                }
            }
            void saveMask(String filename){
                println("Saving mask image"); //<>//
                ArrayList<PVector> verticesArray;
                PGraphics mask = createGraphics(ceil(abs(contentBottomRight.x - contentTopLeft.x)), ceil(abs(contentBottomRight.y - contentTopLeft.y))); //<>// //<>// //<>//
                //PGraphics mask = createGraphics(1024, 768);
                mask.beginDraw();
                mask.background(0);
                mask.fill(255);
                mask.noStroke();
                for (int i = 0; i < shapes.size(); i++){
                    Shape shape = shapes.get(i);
                    verticesArray = shape.getVerticesArray();
                    switch (shape.getType()){
                        case "Polygon":
                        mask.beginShape();
                        for (int j = 0; j < verticesArray.size(); j++){
                            mask.vertex(round(verticesArray.get(j).x - contentTopLeft.x), round(verticesArray.get(j).y - contentTopLeft.y));
                        }
                        mask.vertex(verticesArray.get(0).x - contentTopLeft.x , verticesArray.get(0).y - contentTopLeft.y);
                        mask.endShape(CLOSE);
                        break;
                        case "Ellipse":
                        mask.ellipseMode(CORNERS);
                        mask.ellipse(shape.getMinX() - contentTopLeft.x, shape.getMinY() - contentTopLeft.y, shape.getMaxX() - contentTopLeft.x, shape.getMaxY() - contentTopLeft.y);
                        break;
                    }
                }
                mask.beginDraw();
                mask.save(filename);
                println("End Saving mask image");
            }
        }
