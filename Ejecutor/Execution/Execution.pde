import org.openkinect.freenect.*;
import org.openkinect.processing.*;
import gab.opencv.*;
import controlP5.*;
import java.util.Arrays;

int canvasWidth, canvasHeight;
ArrayList<Scene> scenes;
Canvas canvas = null;
Boolean draw = false;
String filename = "", maskName;
Kinect kinect;

void settings(){
    size(240,250);
}
void setup() {
  scenes = new ArrayList<Scene>();

  // Try to add Kinect
  kinect = new Kinect(this);
  if (kinect != null) {
    kinect.initDepth();
    kinect.enableMirror(false);
  } else {
    println("No kinect found");
  }

  ControlP5 cp5 = new ControlP5(this);

  cp5.addButton("loadConfig")
  .setLabel("Cargar configuracion")
  .setPosition(20, 20)
  .setSize(200, 50);


    cp5.addTextlabel("controles")
  .setText("Controles")
  .setPosition(20,80)
   .setFont(createFont("Arial",18))
   .setMultiline(true)
  .setSize(200,50);

  cp5.addTextlabel("comenzar")
  .setText("S : comenzar")
  .setPosition(40,120)
   .setFont(createFont("Arial",16))
   .setMultiline(true)
  .setSize(200,100);

    cp5.addTextlabel("reinicar")
  .setText("R : reiniciar")
  .setPosition(40,140)
   .setFont(createFont("Arial",16))
   .setMultiline(true)
  .setSize(200,100);

    cp5.addTextlabel("siguiente")
  .setText("-> : escena siguiente")
  .setPosition(40,160)
   .setFont(createFont("Arial",16))
   .setMultiline(true)
  .setSize(200,100);

    cp5.addTextlabel("anterior")
  .setText("<- : escena previa")
  .setPosition(40,180)
   .setFont(createFont("Arial",16))
   .setMultiline(true)
  .setSize(200,100);

}

void draw() {
  background(0);
}


void controlEvent(ControlEvent theEvent) {
  //Get every event name
  switch(theEvent.getName()){
    case "loadConfig":
    loadScenes();
    break;
  }
}
void loadScenes() {
  selectInput("Select a file to process:", "fileSelected");
}

public void fileSelected(File selection) {
  if (selection == null) {
    println("No file selected");
  } else {
    //1 - Get file
    JSONObject fileJ = loadJSONObject(selection.getPath());
    //2- Create folder with data
    filename = selection.getName();
    filename = filename.replace(".json","");
    saveJSONObject(fileJ, "data/" + filename + "/" + filename + ".json");

    //3 - get JSON data
    //GET canvas height & width
    this.canvasWidth = fileJ.getInt("canvasWidth");
    this.canvasHeight = fileJ.getInt("canvasHeight");


    //4 - Get Scenes
    JSONArray scenesJ = fileJ.getJSONArray("scenes");
    for (int i = 0; i < scenesJ.size(); i++) {
      JSONObject sceneJ = (JSONObject) scenesJ.getJSONObject(i);
      Scene scene = new Scene(sceneJ.getString("name"));

      //Get & add sceens to scene
      JSONArray screensJ = sceneJ.getJSONArray("screens");
      for (int j = 0; j < screensJ.size(); j++) {
        JSONObject screenJ = screensJ.getJSONObject(j);
        Screen screen = new Screen(this, screenJ.getString("name"), this.canvasWidth,this.canvasHeight, kinect);

        JSONObject contentTopLeftJ = screenJ.getJSONObject("contentTopLeft");
        JSONObject contentBottomRightJ = screenJ.getJSONObject("contentBottomRight");
        screen.setContentTopLeft(contentTopLeftJ.getInt("x"),contentTopLeftJ.getInt("y"));
        screen.setContentBottomRight(contentBottomRightJ.getInt("x"),contentBottomRightJ.getInt("y"));

        screen.setShapes(screenJ.getJSONArray("shapes"));
        screen.setResizeMode(screenJ.getString("resizeMode"));
        screen.setContentClass(screenJ.getString("contentClass"));
        //Screen mask
        maskName = "data/" + filename +"/maskE" + i + "S" + j + ".tif";
        screen.saveMask(maskName);
        screen.setMask(maskName);
        scene.addScreen(screen);
      }
      //Add scene
      scenes.add(scene);
    }
   if(canvas == null){
      canvas = new Canvas(this, this.canvasWidth, this.canvasHeight, "Canvas",scenes);
    }else{
      canvas.setSize( this.canvasWidth, this.canvasHeight);
    }
  }
 }
