class Scene {
  String name;
  ArrayList<Screen> screens;


  Scene(String name) {
    this.name = name;
    this.screens = new ArrayList<Screen>();
  }

  void draw() {
    for (int i = 0; i < this.screens.size(); i++){
      this.screens.get(i).draw();
    }
  }

  public void addScreen(Screen screen) {
    this.screens.add(screen);
  }
  
  public ArrayList<Screen> getScreens() {
    return this.screens;
  }
  
  public String getName() {
    return this.name;
  }
}
