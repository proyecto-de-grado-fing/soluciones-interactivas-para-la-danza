class Ellipse implements Shape{

  PShape mEllipse;
  PVector mTopLeft;
  PVector mBottomRight;
  color mColor;
  PVector[] verticesArray = new PVector[4];

  Boolean selected;
  Integer selectedVertex;

  int VERTICES_RADIUS = 5;

  Ellipse (color c, int mMouseX, int mMouseY) {
    mTopLeft = new PVector(mMouseX, mMouseY);
    mBottomRight = new PVector(mMouseX, mMouseY);
    mColor = c;
    updateEllipse();
    updateVerticesFromCorners();
    selected = false;
  }

  int getColor(){
    return mColor;
  }

  String getType(){
    return "Ellipse";
  }

  //Draw ellipse
  PShape display() {
    return mEllipse;
  }

  //Check if we clicked inside the ellipse
  Boolean inside(int mMouseX, int mMouseY,int c) {
    //If color in mouse position is equal to ellipse color, then is inside
    return c == mColor;
  }

  //Check if we clicked inside a vertex and set selectedVertex to vertex's index in vertexArray
  Boolean checkSelectedVertex(int mMouseX, int mMouseY) {
    selectedVertex = null;
    for (int i = 0; i < verticesArray.length; i++) {
      if (dist(verticesArray[i].x, verticesArray[i].y, mMouseX, mMouseY) < (1.5)*VERTICES_RADIUS) {
        selectedVertex = i;
        return true;
      }
    }
    return false;
  }

  PShape getVerticesGroup(){
    PShape group =createShape(GROUP);
    for (int i = 0; i < verticesArray.length; i++) {

      //Create vertex circle
      ellipseMode(RADIUS);
      PShape elip = createShape(ELLIPSE, verticesArray[i].x, verticesArray[i].y, VERTICES_RADIUS, VERTICES_RADIUS);
      elip.setStroke(0);
      //if vertex is selected fill with red
      if (selectedVertex != null && selectedVertex == i) {
        elip.setFill(color(255, 0, 0));
      }
      group.addChild(elip);
    }
    return group;
  }
  //Update bottom-right corner of ellipse
  void setBottomRight(int mMouseX, int mMouseY) {
    mBottomRight = new PVector (mMouseX, mMouseY);
    updateEllipse();
    updateVerticesFromCorners();
  }

  //Move ellipse or selectedVertex
  void move(int x, int y, int pmx,int pmy) {

    //Check if moving selectedVertex
    if (selectedVertex == null) {
      //Update ellipse's corners
      mBottomRight.x = mBottomRight.x + (x-pmx);
      mBottomRight.y = mBottomRight.y + (y-pmy);
      mTopLeft.x = mTopLeft.x + (x - pmx);
      mTopLeft.y = mTopLeft.y + (y - pmy);
    }
    else {

      //Move selectedVertex
      switch(selectedVertex) {
        //Top vertex
        case 0:
        mTopLeft.y = verticesArray[selectedVertex].y + (y - pmy);
        break;
        //Right vertex
        case 1:
        mBottomRight.x = verticesArray[selectedVertex].x + (x - pmx);
        break;
        //Bottom vertex
        case 2:
        mBottomRight.y = verticesArray[selectedVertex].y + (y - pmy);
        break;
        //Left vertex
        case 3:
        mTopLeft.x = verticesArray[selectedVertex].x + (x - pmx);
        break;
      }
    }
    updateEllipse();
    updateVerticesFromCorners();
  }

  void moveStep(int x,int y){
    if(selectedVertex == null){
      mBottomRight.x = mBottomRight.x + x;
      mBottomRight.y = mBottomRight.y + y;
      mTopLeft.x = mTopLeft.x + x;
      mTopLeft.y = mTopLeft.y + y;
      updateEllipse();
      updateVerticesFromCorners();
    }
  }

  //Recreate ellipse from corners
  void updateEllipse() {
    ellipseMode(CORNERS);
    pushStyle();
    beginShape();
    mEllipse = createShape(ELLIPSE, mTopLeft.x, mTopLeft.y, mBottomRight.x, mBottomRight.y);
    mEllipse.setFill(mColor);
    //mEllipse.noStroke();
    endShape();
    popStyle();
  }

  //Set if ellipse is selected
  void setSelected(Boolean sel) {
    this.selected = sel;
  }

  //Updates verticesArray from ellipse's corners
  void updateVerticesFromCorners() {
    verticesArray[0] = new PVector((mBottomRight.x - mTopLeft.x) / 2 + mTopLeft.x, mTopLeft.y);
    verticesArray[1] = new PVector(mBottomRight.x, (mBottomRight.y - mTopLeft.y) / 2 + mTopLeft.y);
    verticesArray[2] = new PVector((mBottomRight.x - mTopLeft.x) / 2 + mTopLeft.x, mBottomRight.y);
    verticesArray[3] = new PVector(mTopLeft.x, (mBottomRight.y - mTopLeft.y) / 2 + mTopLeft.y);
  }

  //Updates ellipse's corners from verticesArray
  void updateCornersFromVertices() {
    mBottomRight.x =  verticesArray[1].x;
    mBottomRight.y = verticesArray[2].y;
    mTopLeft.x = verticesArray[3].x;
    mTopLeft.y = verticesArray[0].y;
  }

  //Moves the selectedVertex
  void moveSelectedVertex(int direction) {

    //Check if we have a selected vertex
    if (selectedVertex != null) {

      //if selected vertex is the left or right one
      if (selectedVertex == 1 || selectedVertex == 3) {
        switch(direction) {
          case LEFT:
          verticesArray[selectedVertex].x--;
          break;
          case RIGHT:
          verticesArray[selectedVertex].x++;
          break;
        }
      }
      else {
        //Selected vertex is the top or bottom one
        switch(direction) {
          case DOWN:
          verticesArray[selectedVertex].y++;
          break;
          case UP:
          verticesArray[selectedVertex].y--;
          break;
        }
      }
      updateCornersFromVertices();
      updateVerticesFromCorners();
      updateEllipse();
    }
  }

  JSONArray getCoords() {
    JSONArray coords = new JSONArray();

    //Add topLeft coords
    JSONObject topLeftCoords = new JSONObject();
    topLeftCoords.setFloat("x", mTopLeft.x);
    topLeftCoords.setFloat("y", mTopLeft.y);
    coords.append(topLeftCoords);

    //Add bottomRight coords
    JSONObject bottomRightCoords = new JSONObject();
    bottomRightCoords.setFloat("x", mBottomRight.x);
    bottomRightCoords.setFloat("y", mBottomRight.y);
    coords.append(bottomRightCoords);
    return coords;
  }

  ArrayList<PVector> getVerticesArray(){
    ArrayList<PVector> ret = new ArrayList<PVector>(Arrays.asList(verticesArray));
    return ret;
  }

  //gets the max X coord
  int getMaxX(){
    return (int) max(mTopLeft.x, mBottomRight.x);
  }

  //gets the min X coord
  int getMinX(){
    return (int) min(mTopLeft.x, mBottomRight.x);
  }

  //gets the max Y coord
  int getMaxY(){
    return (int) max(mTopLeft.y, mBottomRight.y);
  }

  //gets the min Y coord
  int getMinY(){
    return (int) min(mTopLeft.y, mBottomRight.y);
  }
}
