class Canvas extends PApplet  {

  PApplet parent;
  int canvasWidth,canvasHeight,sceneIndex = 0;
  String name;
  PShape tmp_shape;
  PVector pos;
  ArrayList<Scene> scenes;
  Screen activeScreen;
  Scene activeScene;


  public Canvas(PApplet _parent, int _w, int _h, String _name,ArrayList<Scene> scenes) {
    super();
    this.parent = _parent;
    this.canvasWidth=_w;
    this.canvasHeight=_h;
    this.name = _name;
    this.activeScene = null;
    this.scenes = scenes;
    PApplet.runSketch(new String[]{ "--display=1",this.getClass().getName()}, this);
  }

  public void setActiveScene(Scene scene){
     background(0);
    this.activeScene = scene;
    for (int i = 0; i < this.activeScene.screens.size(); i++){
      this.activeScene.screens.get(i).start();
    }
  }
    public void stopActiveScene(){
    for (int i = 0; i < this.activeScene.screens.size(); i++){
      this.activeScene.screens.get(i).stop();
    }
  }
  public void setup() {
    surface.setTitle(this.name);
    surface.setLocation(0, 0);
    background(0);
  }
  public void settings(){
    size(this.canvasWidth, this.canvasHeight);
  }
  public void setSize(int w,int h){
    size(this.canvasWidth, this.canvasHeight);
  }
  void draw() {
    if(activeScene != null){
      draw_scene();
    }
  }

  void draw_scene(){
    PImage img = null;
    for (int i = 0; i < this.activeScene.screens.size(); i++){
      img = this.activeScene.screens.get(i).draw();
      pos = this.activeScene.screens.get(i).getPosition();
      image(img,(int)pos.x,(int)pos.y);
    }
  }
void keyPressed() {

  if ((key == 's') || (key == 's')) {
    this.setActiveScene(scenes.get(sceneIndex));
  }else if ((key == 'r') || (key == 'R')) {
     this.stopActiveScene();
     sceneIndex = 0;
    this.setActiveScene(scenes.get(sceneIndex));
  }else{
   if (keyCode == RIGHT) this.nextScene();
   if (keyCode == LEFT) this.prevScene();
  }
}
void nextScene(){
  println("Next scene");
  sceneIndex = (sceneIndex + 1) % this.scenes.size();
  println(sceneIndex);
  this.stopActiveScene();
  this.setActiveScene(scenes.get(sceneIndex));
}
void prevScene(){
   println("Prev scene");
  sceneIndex--;
  if(sceneIndex<0) sceneIndex=0;
  this.stopActiveScene();
  this.setActiveScene(scenes.get(sceneIndex));
}

}
