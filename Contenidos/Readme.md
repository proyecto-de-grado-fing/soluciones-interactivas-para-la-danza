Esta adaptación se basa en seguir los siguientes pasos:
+  Convertir el contenido a una clase.
+  Extender a la claseContentView, encargada de brindar el contenido de cada espaciode proyección.
+  El constructor de la clase debe aceptar como parámetrosPApplet container,int canvasWidth, int canvasHeight.
+  En el constructor agregar la línea aparte de cualquier inicialización que se desee:super(container, canvasWidth, canvasHeight)
+  Sobrescribir el métodoPImage draw(). Este método debe retornar una imagen deltipoPImage.6.  Sobrescribir el métodovoid destroy().