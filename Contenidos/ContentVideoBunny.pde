import processing.video.*;

class ContentVideoBunny extends ContentView
{
    Movie movie;
    Movie myMovie;

    ContentVideoBunny(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);

        myMovie = new Movie(container, "bunny.mp4");
        myMovie.stop();
    }

    ContentVideoBunny(PApplet container) {
        super(container);
        myMovie = new Movie(container, "bunny.mp4");
        myMovie.stop();
    }

    public void play(){
      myMovie.loop();
    }
    public void stop(){
        myMovie.stop();
    }

    @Override
    PImage draw() {
        if (myMovie.available()) {
            myMovie.read();
        }
        // image (myMovie, 500, 0);
        return myMovie.get();
    }
    @Override
    void destroy(){
      println("Destroy movie");
      this.myMovie.stop();
      this.myMovie.dispose();
    }

}
