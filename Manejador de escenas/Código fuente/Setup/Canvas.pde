class Canvas extends PApplet  {

    PApplet parent;
    int canvasWidth,canvasHeight;
    String name;
    Shape active;
    Ellipse ellipse;
    Polygon polygon;
    PShape tmp_shape;
    JSONObject pos;

    ArrayList<Shape> shapesList = new ArrayList<Shape>();
    Screen activeScreen;
    Scene activeScene;
    Preview preview;

    public Canvas(PApplet _parent, int _w, int _h, String _name) {
        super();
        this.parent = _parent;
        this.canvasWidth=_w;
        this.canvasHeight=_h;
        this.name = _name;
        // PApplet.runSketch(new String[]{ "--display=2", "--present",this.getClass().getName()}, this);
        PApplet.runSketch(new String[]{ "--display=1",this.getClass().getName()}, this);
    }
    public void loadShapes(ArrayList<Shape> shapes){
        this.shapesList = shapes;
    }

    public void setup() {
        surface.setTitle(this.name);
        surface.setLocation(0, 0);
    }
    public void settings(){
        size(this.canvasWidth, this.canvasHeight);
    }
    void draw() {
        background(0);
        //Draw mouse coords
        drawCoords();
        //Draw shapes
        draw_screen();
        //If previewMode enable then draw content with mask
        if (gui.getPreviewMode() == true){
            draw_preview();
        }

    }

    //Draw horizontal and vertical lines in mouse position
    void drawCoords() {
        pushStyle();
        stroke(0, 255, 255);
        //4 lines so exact mouse position has nothing behind
        line(0, mouseY, mouseX-1, mouseY-1);
        line(mouseX+1, mouseY, width, mouseY);
        line(mouseX, 0, mouseX, mouseY-1);
        line(mouseX, mouseY+1, mouseX, height);
        popStyle();
    }
    //Draw created screens
    void draw_screen() {
        activeScreen = gui.getActiveScreen(); //<>//
        if(activeScreen != null){
            pos = activeScreen.getContentTopLeft();
            this.shapesList = activeScreen.getShapesList();
            for (int i = 0; i < this.shapesList.size(); i++) {
                if(this.shapesList.get(i) != this.active){
                    this.tmp_shape = this.shapesList.get(i).display();
                    this.tmp_shape.setStroke(color(0));
                    shape(this.tmp_shape, 0, 0);
                }
            }

            if(active != null){
                this.tmp_shape = active.display();
                shape(this.tmp_shape);
                PShape v = active.getVerticesGroup();
                shape(v);
            }
        }
    }
    void draw_preview(){
        PImage img = preview.draw();
        image(img,(int)pos.get("x"),(int)pos.get("y"));
    }
    void mouseClicked(MouseEvent evt) {
        println("click");
        if (evt.getCount() == 2 && gui.shapeType == "Polygon") {
            closePolygon();
        };
    }
    //Finish editing polygon
    void closePolygon() {
        println("close polygon");
        if (polygon != null) {
            Polygon newPolygon = polygon.clone();
            activeScreen.addShape(newPolygon);
            this.shapesList.remove(polygon);
            polygon = null;
            println("closed polygon");
        }
    }
    void controlEvent(ControlEvent theEvent) {
        println("canvas");
    }
    void keyPressed() {
        //Check if changing screen
        switch (key) {
            case 'E':
            case 'e':
            closePolygon();
            gui.shapeType = "Ellipse";
            // gui.cf.activateToolSwitch(0);
            break;
            case 'P':
            case 'p':
            closePolygon();
            gui.shapeType = "Polygon";
            // gui.cf.activateToolSwitch(1);
            break;
            case 'm':
            case 'M':
            closePolygon();
            gui.shapeType = "Mouse";
            // gui.activateToolSwitch(2);
            break;
            case 'C':
            case 'c':
            this.shapesList.clear();
            polygon = null;
            break;
            //Delete active screen
            case DELETE:
            case BACKSPACE:
            if (active != null) {
                int activeIndex = this.shapesList.indexOf(active);
                if (activeIndex > -1) {
                    this.shapesList.remove(activeIndex);
                    this.active = null;
                    activeScreen.updateContainer();
                }
            }
            break;
            // case 'S':
            // case 's':
            // save_screens(gui.cf.getFilename());
            // break;
            // case 'L':
            // case 'l':
            // load_screens();
            // break;
            case ESC:
            key = 0;
            break;
        }

        //Check if moving some vertex
        if (active != null) {
            active.moveSelectedVertex(keyCode);
        }

        //Move shape
        if((active != null)&&(gui.shapeType == "Mouse")){
            switch(keyCode){
                case LEFT:
                active.moveStep(-1,0);
                break;
                case RIGHT:
                active.moveStep(1,0);
                break;
                case UP:
                active.moveStep(0,-1);
                break;
                case DOWN:
                active.moveStep(0,1);
                break;

            }
        }
    }
    void mousePressed() {

        if (active != null) {
            active.setSelected(false);
            active = null;
        }
        //If clicked outside a screen or vertex check if we are creating a new screen
        switch(gui.shapeType) {
            case "Mouse":
            for (int i = 0; i < this.shapesList.size(); i++) {
                //Check if we clicked inside a screen
                if (this.shapesList.get(i).inside(mouseX, mouseY,get(mouseX, mouseY))) {
                    active = this.shapesList.get(i);
                    active.setSelected(true);
                };
                //Check if we clicked inside a vertex
                if (this.shapesList.get(i).checkSelectedVertex(mouseX, mouseY)) {
                    active = this.shapesList.get(i);
                    active.setSelected(true);
                };
            };
            break;
            case "Ellipse":
            //Create new ellipse
            gui.screenColor = color(random(255), random(255), random(255));
            ellipse = new Ellipse(gui.screenColor, mouseX, mouseY);
            this.activeScreen.addShape(ellipse);
            break;
            case "Polygon":
            if (polygon == null) {
                //Create new polygon
                gui.screenColor = color(random(255), random(255), random(255));
                polygon = new Polygon(gui.screenColor);
                this.activeScreen.addShape(polygon);
            }
            //Add vertex to current polygon
            polygon.addVertex(mouseX, mouseY);
            break;
        }
    }
    //Move active screen or change ellipse's size if we are creating it
    void mouseDragged() {
        if (active != null) {
            active.move(mouseX,mouseY,pmouseX,pmouseY);
        }
        switch(gui.shapeType) {
            case "Ellipse":
            ellipse.setBottomRight(mouseX, mouseY);
            break;
        }
        this.activeScreen.updateContainer();
    }

}
