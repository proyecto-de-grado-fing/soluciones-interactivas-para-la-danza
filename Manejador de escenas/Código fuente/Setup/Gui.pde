class Gui {
  public Controls controls;
  public Canvas canvas;
  public String shapeType = "Mouse";
  public int screenColor;
  public Boolean previewMode = false;
  public PApplet context;
  public ContentView content;
  public ArrayList<String> contentViews;
  public Kinect kinect;

  Gui(PApplet context, int width, int height, ArrayList<String> contentViews, Kinect kinect) {
    this.controls = new Controls(context, width, height, "Controles", contentViews);
    this.canvas = new Canvas(context, width, height, "Lienzo");
    this.context = context;
    this.contentViews = contentViews;
    this.kinect = kinect;
  }

  Screen getActiveScreen(){
    return this.controls.getActiveScreen();
  }
  Scene getActiveScene(){
    return this.controls.getActiveScene();
  }

  Boolean getPreviewMode(){
    return this.previewMode;
  }
  void setPreviewMode(Boolean value, Screen screen){
    if(value == true){
      canvas.preview = new Preview(screen, this.context, contentViews, kinect);
      canvas.preview.saveMask();
    }else{
      if (canvas.preview != null) {
        canvas.preview.destroy();
        canvas.preview = null;
      }
    }
    this.previewMode = value;
  }
  void setShapeType(String type) {
    this.shapeType = type;
  }

}
