class ContentMePicture extends ContentView
{
    Kinect kinect;
    BlobDetection blobDetection;
    int[] background;

    PImage backPic;
    PImage bImg;
    PImage img;

    int canvasWidth;
    int canvasHeight;
    int finished = 0;
    int backgroundColor = color(17,51,77);
    float backgroundTransparency = 255;
    float startTime,timeRelative,timeRunning;
    float opacity = 255;

    ContentMePicture(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background, PImage backPic){
        super(container, canvasWidth, canvasHeight);
        this.kinect = kinect;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
        //Load image and redim to fit kinect size
        PImage img1 = createImage(640, 480,ARGB);
        img1.copy(backPic, 0, 0, backPic.width, backPic.height, 0, 0, kinect.width, kinect.height);
        this.backPic = img1;
    };
     ContentMePicture(PApplet container, Kinect kinect) {
        super(container);
        this.kinect = kinect;

        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
        backPic = loadImage("fondoEstrellas.jpg");
        PImage img1 = createImage(640, 480,ARGB);
        img1.copy(backPic, 0, 0, backPic.width, backPic.height, 0, 0, kinect.width, kinect.height);
        this.backPic = img1;
        // backPic.loadPixels();
        // for (int i = 0; i < backPic.pixels.length; i++) {
        //   backPic.pixels[i] = color(0, 90, 102);
        // }
        // backPic.updatePixels();
        // PImage img1 = createImage(640, 480,ARGB);
        // img1.copy(backPic, 0, 0, backPic.width, backPic.height, 0, 0, kinect.width, kinect.height);
        // this.backPic = img1;
    }

    int isFinished(){
        return finished;
    }

    public void play(){
      //take photo
      this.loadInitialDepth();
    }
    public void stop(){

    }

    @Override
    PImage draw() {
        if (background == null) {
            this.loadInitialDepth();
        }
        bImg = getMeImg();
        return bImg;
    };

    PImage getMeImg(){

        PGraphics img = createGraphics(kinect.width, kinect.height);
        img.beginDraw();
        PShape user = createShape();
        user.beginShape();
        ArrayList<PVector> contour = blobDetection.getContour(background, kinect.getRawDepth(), true);
        if (contour != null){
            user.fill(255,255,255);
            user.noStroke();
            for (PVector point : contour){
                user.vertex(point.x, point.y);
            }
        }
        user.endShape(CLOSE);
        img.shape(user);
        img.endDraw();
        PImage cleanBack = backPic.copy();
        cleanBack.mask(img);
        //cleanBack.resize(canvasWidth, canvasHeight);
        cleanBack.resize(1024, 768);
        return cleanBack;
    }

    void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
    }

}
