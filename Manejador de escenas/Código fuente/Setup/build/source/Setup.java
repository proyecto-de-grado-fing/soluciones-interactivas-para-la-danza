import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import org.openkinect.freenect.*; 
import org.openkinect.processing.*; 
import controlP5.*; 
import processing.video.*; 
import processing.video.*; 
import processing.video.*; 
import processing.video.*; 
import java.util.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Setup extends PApplet {





Gui gui = null;
int canvasWidth = 1024;
int canvasHeight = 768;
Textfield canvasWidthInput,canvasHeightInput;
ControlP5 cp5;
ArrayList<Scene> scenesList = new ArrayList<Scene>();
// ContentView content;

public void settings(){
  // size(canvasWidth, canvasHeight);
  size(240,500);
}

public void setup() {
  // content = new ContentVideo1(this);
  getContentFiles();
  this.cp5 = new ControlP5(this);

  this.cp5.addTextlabel("dancing")
  .setText("DANCING")
  .setColor(0)
  .setFont(createFont("arial",15))
  .setPosition(85,20);
  this.cp5.addTextlabel("with")
  .setText("with")
  .setColor(0)
  .setFont(createFont("arial",15))
  .setPosition(105,40);
  this.cp5.addTextlabel("processing")
  .setText("PROCESSING")
  .setColor(0)
  .setFont(createFont("arial",15))
  .setPosition(70,60);

  //Canvas width and height
  canvasWidthInput = cp5.addTextfield("canvasWidthInput")
  .setLabel("Ancho")
  .setFont(createFont("arial",14))
  .setPosition(20, 110)
  .setText("1024")
  .setSize(200,50);

  Label labelCanvaswidth = canvasWidthInput.getCaptionLabel();
  labelCanvaswidth.setColor(0);
  labelCanvaswidth.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

  canvasHeightInput = cp5.addTextfield("canvasHeightInput")
  .setLabel("Alto")
  .setFont(createFont("arial",14))
  .setPosition(20, 190)
  .setText("768")
  .setSize(200,50);

  Label labelCanvasHeight = canvasHeightInput.getCaptionLabel();
  labelCanvasHeight.setColor(0);
  labelCanvasHeight.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);


  cp5.addButton("newConfig")
  .setLabel("Crear configuracion")
  .setPosition(20, 270)
  .setSize(200, 50);

  cp5.addButton("loadConfig")
  .setLabel("Cargar configuracion")
  .setPosition(20, 330)
  .setSize(200, 50);

  cp5.addButton("exit")
  .setLabel("Salir")
  .setPosition(70, 440)
  .setSize(100, 50);
}

public void draw() {
  //Black background
  background(255);
}

public void keyPressed() {
}

public void controlEvent(ControlEvent theEvent) {
  //Get every event name
  switch(theEvent.getName()){
    case "loadConfig":
    loadScenes();
    break;
    case "newConfig":
    newConfig();
    break;
  }
}

public void newConfig() {
  if(this.gui == null){
    this.canvasWidth = Integer.parseInt(trim(canvasWidthInput.getText()));
    this.canvasHeight = Integer.parseInt(trim(canvasHeightInput.getText()));
    this.gui = new Gui(this, this.canvasWidth, this.canvasHeight);
  }
  else
  {
    //TODO: destroy and create?
  }
}

public void loadScenes() {

  selectInput("Select a file to process:", "fileSelected");
}

public void fileSelected(File selection) {
  if (selection == null) {
    println("No file selected");
  }
  else
  {

    String fileRelative = selection.getPath().substring(dataPath("").length(), selection.getPath().length());

    //Load config from file
    //1 - Get file
    JSONObject file = loadJSONObject("data" + fileRelative);
    //2 - get JSON data
    // JSONObject values = file.getJSONObject(0);
    //GET canvas height & width
    this.canvasWidth = file.getInt("canvasWidth");
    this.canvasHeight = file.getInt("canvasHeight");

    if(this.gui == null){
      this.gui = new Gui(this, this.canvasWidth, this.canvasHeight);
    }
    //3 - Get Scenes
    JSONArray scenes = file.getJSONArray("scenes");
    for (int i = 0; i < scenes.size(); i++) {
      JSONObject scene = (JSONObject) scenes.getJSONObject(i);

      Scene tmp = new Scene(scene.getString("name"));
      this.gui.controls.addScene(scene.getString("name"));
      //Get & add sceens to scene
      JSONArray tmp_screens = scene.getJSONArray("screens");
      for (int j = 0; j < tmp_screens.size(); j++) {
        JSONObject scr = tmp_screens.getJSONObject(j);
        Screen tmp_screen = new Screen(scr.getString("name"));
        gui.controls.addScreen(scr.getString("name"),scene.getString("name"));

        JSONObject _contentTopLeft = scr.getJSONObject("contentTopLeft");
        JSONObject _contentBottomRight = scr.getJSONObject("contentBottomRight");
        tmp_screen.setContentTopLeft(_contentTopLeft.getInt("x"),_contentTopLeft.getInt("y"));
        tmp_screen.setContentBottomRight(_contentBottomRight.getInt("x"),_contentBottomRight.getInt("y"));

        tmp_screen.setMask(scr.getString("mask"));
        tmp_screen.setResizeMode(scr.getString("resizeMode"));
        tmp_screen.setContentClass(scr.getString("contentClass"));

        JSONArray _shapes = (JSONArray) scr.getJSONArray("shapes");

        tmp_screen.setShapes(_shapes);
        tmp.addScreen(tmp_screen);
      }
      //Add scene
      this.scenesList.add(tmp);
    }
  }

}

public boolean saveConfigSetup(JSONObject json, String filename){
  return saveJSONObject(json, "data/" + filename + "/" + filename + ".json");
}
class Canvas extends PApplet  {

  PApplet parent;
  int canvasWidth,canvasHeight;
  String name;
  Shape active;
  Ellipse ellipse;
  Polygon polygon;
  PShape tmp_shape;
  JSONObject pos;

  ArrayList<Shape> shapesList = new ArrayList<Shape>();
  Screen activeScreen;
  Scene activeScene;
  Preview preview;

  public Canvas(PApplet _parent, int _w, int _h, String _name) {
    super();
    this.parent = _parent;
    this.canvasWidth=_w;
    this.canvasHeight=_h;
    this.name = _name;
    // PApplet.runSketch(new String[]{ "--display=2", "--present",this.getClass().getName()}, this);
    PApplet.runSketch(new String[]{ "--display=1",this.getClass().getName()}, this);
  }
  public void loadShapes(ArrayList<Shape> shapes){
    this.shapesList = shapes;
  }

  public void setup() {
    surface.setTitle(this.name);
    surface.setLocation(0, 0);
  }
  public void settings(){
    size(this.canvasWidth, this.canvasHeight);
  }
  public void draw() {
    background(0);
    //Draw mouse coords
    drawCoords();
    //Draw shapes
    draw_screen();

    //If previewMode enable then draw content with mask
    if (gui.getPreviewMode() == true){
      draw_preview();
    }

  }

  //Draw horizontal and vertical lines in mouse position
  public void drawCoords() {
    pushStyle();
    stroke(0, 255, 255);
    //4 lines so exact mouse position has nothing behind
    line(0, mouseY, mouseX-1, mouseY-1);
    line(mouseX+1, mouseY, width, mouseY);
    line(mouseX, 0, mouseX, mouseY-1);
    line(mouseX, mouseY+1, mouseX, height);
    popStyle();
  }
  //Draw created screens
  public void draw_screen() {
    activeScreen = gui.getActiveScreen();
    if(activeScreen != null){
      pos = activeScreen.getContentTopLeft();
      this.shapesList = activeScreen.getShapesList();
      for (int i = 0; i < this.shapesList.size(); i++) {
        if(this.shapesList.get(i) != this.active){
          this.tmp_shape = this.shapesList.get(i).display();
          shape(this.tmp_shape,0,0);
        }
      }

      if(active != null){
        this.tmp_shape = active.display();
        shape(this.tmp_shape);
        PShape v = active.getVerticesGroup();
        shape(v);
      }

    }
  }
  public void draw_preview(){
    PImage img = preview.draw();
    image(img,(int)pos.get("x"),(int)pos.get("y"));
  }
  public void mouseClicked(MouseEvent evt) {
    if (evt.getCount() == 2 && gui.shapeType == "Polygon") {
      closePolygon();
    };
  }
  //Finish editing polygon
  public void closePolygon() {
    if (polygon != null) {
      Polygon newPolygon = polygon.clone();
      activeScreen.addShape(newPolygon);
      this.shapesList.remove(polygon);
      polygon = null;
    }
  }
  public void controlEvent(ControlEvent theEvent) {
    println("canvas");
  }
  public void keyPressed() {
    //Check if changing screen
    switch (key) {
      case 'E':
      case 'e':
      closePolygon();
      gui.shapeType = "Ellipse";
      // gui.cf.activateToolSwitch(0);
      break;
      case 'P':
      case 'p':
      closePolygon();
      gui.shapeType = "Polygon";
      // gui.cf.activateToolSwitch(1);
      break;
      case 'm':
      case 'M':
      closePolygon();
      gui.shapeType = "Mouse";
      // gui.activateToolSwitch(2);
      break;
      case 'C':
      case 'c':
      this.shapesList.clear();
      polygon = null;
      break;
      //Delete active screen
      case DELETE:
      case BACKSPACE:
      if (active != null) {
        int activeIndex = this.shapesList.indexOf(active);
        if (activeIndex > -1) {
          this.shapesList.remove(activeIndex);
          this.active = null;
          activeScreen.updateContainer();
        }
      }
      break;
      // case 'S':
      // case 's':
      // save_screens(gui.cf.getFilename());
      // break;
      // case 'L':
      // case 'l':
      // load_screens();
      // break;
      case ESC:
      key = 0;
      break;
    }

    //Check if moving some vertex
    if (active != null) {
      active.moveSelectedVertex(keyCode);
    }

    //Move shape
    if((active != null)&&(gui.shapeType == "Mouse")){
      switch(keyCode){
        case LEFT:
        active.moveStep(-1,0);
        break;
        case RIGHT:
        active.moveStep(1,0);
        break;
        case UP:
        active.moveStep(0,-1);
        break;
        case DOWN:
        active.moveStep(0,1);
        break;

      }
    }
  }
  public void mousePressed() {

    if (active != null) {
      active.setSelected(false);
      active = null;
    }
    //If clicked outside a screen or vertex check if we are creating a new screen
    switch(gui.shapeType) {
      case "Mouse":
      for (int i = 0; i < this.shapesList.size(); i++) {
        //Check if we clicked inside a screen
        if (this.shapesList.get(i).inside(mouseX, mouseY,get(mouseX, mouseY))) {
          active = this.shapesList.get(i);
          active.setSelected(true);
        };
        //Check if we clicked inside a vertex
        if (this.shapesList.get(i).checkSelectedVertex(mouseX, mouseY)) {
          active = this.shapesList.get(i);
          active.setSelected(true);
        };
      };
      break;
      case "Ellipse":
      //Create new ellipse
      gui.screenColor = color(random(255), random(255), random(255));
      ellipse = new Ellipse(gui.screenColor, mouseX, mouseY);
      this.activeScreen.addShape(ellipse);
      break;
      case "Polygon":
      if (polygon == null) {
        //Create new polygon
        gui.screenColor = color(random(255), random(255), random(255));
        polygon = new Polygon(gui.screenColor);
        this.activeScreen.addShape(polygon);
      }
      //Add vertex to current polygon
      polygon.addVertex(mouseX, mouseY);
      break;
    }
  }
  //Move active screen or change ellipse's size if we are creating it
  public void mouseDragged() {
    if (active != null) {
      active.move(mouseX,mouseY,pmouseX,pmouseY);
    }
    switch(gui.shapeType) {
      case "Ellipse":
      ellipse.setBottomRight(mouseX, mouseY);
      break;
    }
  }

}
class ContentBalls extends ContentView
{
  int numBalls = 5;
  float spring = 0.05f; //0.05;
  float gravity = 0.13f; //0.03
  float friction = -0.9f; //-0.9;
  Ball[] balls = new Ball[numBalls];

  ContentBalls(PApplet container, int canvasWidth, int canvasHeight) {
    super(container, canvasWidth, canvasHeight);
    for (int i = 0; i < numBalls; i++) {
      balls[i] = new Ball(random(canvasWidth), random(canvasHeight), random(100, 200), i, balls);
    }
    noStroke();
    fill(255, 204);
  }

  ContentBalls(PApplet container) {
    super(container);
    for (int i = 0; i < numBalls; i++) {
      balls[i] = new Ball(random(100), random(200), random(30, 60), i, balls);
    }
    noStroke();
    fill(255, 204);
  }
  public void setCanvasDimens(int w,int h){
    this.canvasHeight = h;
    this.canvasWidth = w;
  }
  public @Override
  PImage draw() {
    PGraphics pg = createGraphics(this.canvasWidth, this.canvasHeight);
    pg.beginDraw();
    pg.background(0);
    for (Ball ball : balls) {
      ball.collide();
      ball.move(canvasWidth, canvasHeight);
      ball.display(pg);
    }
    pg.endDraw();
    PImage pi = pg.get();
    return pi;
  }
  public @Override
  void destroy(){
    println("Destroy balls");
    balls = null;
  }
  class Ball {

    float x, y;
    float diameter;
    float vx = 0;
    float vy = 0;
    int id;
    Ball[] others;

    Ball(float xin, float yin, float din, int idin, Ball[] oin) {
      x = xin;
      y = yin;
      diameter = din;
      id = idin;
      others = oin;
    }

    public void collide() {
      for (int i = id + 1; i < numBalls; i++) {
        float dx = others[i].x - x;
        float dy = others[i].y - y;
        float distance = sqrt(dx*dx + dy*dy);
        float minDist = others[i].diameter/2 + diameter/2;
        if (distance <= minDist) {
          float angle = atan2(dy, dx);
          float targetX = x + cos(angle) * minDist;
          float targetY = y + sin(angle) * minDist;
          float ax = (targetX - others[i].x) * spring;
          float ay = (targetY - others[i].y) * spring;
          vx -= ax;
          vy -= ay;
          others[i].vx += ax;
          others[i].vy += ay;
        }
      }
    }

    public void move(int canvasWidth, int canvasHeight) {
      vy += gravity;
      x += vx;
      y += vy;
      if (x + diameter/2 > canvasWidth) {
        x = canvasWidth - diameter/2;
        vx *= friction;
        } else if (x - diameter/2 < 0) {
          x = diameter/2;
          vx *= friction;
        }
        if (y + diameter/2 > canvasHeight) {
          y = canvasHeight - diameter/2;
          vy *= friction;
          } else if (y - diameter/2 < 0) {
            y = diameter/2;
            vy *= friction;
          }
        }

        public void display(PGraphics pg) {
          pg.ellipse(x, y, diameter, diameter);
        }
      }
    }


class ContentVideo extends ContentView
{
    Movie movie;
    Movie myMovie;

    ContentVideo(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);

        myMovie = new Movie(container, "2.mp4");
        myMovie.loop();
    }

    ContentVideo(PApplet container) {
        super(container);
        myMovie = new Movie(container, "2.mp4");
        myMovie.loop();
    }

    public @Override
    PImage draw() {
        if (myMovie.available()) {
            myMovie.read();
        }
        // image (myMovie, 500, 0);
        return myMovie.get();
    }
    public @Override
    void destroy(){
      println("Destroy movie");
      this.myMovie.stop();
      this.myMovie.dispose();
    }
}


class ContentVideo1 extends ContentView
{
    Movie movie;
    Movie myMovie;

    ContentVideo1(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);

        myMovie = new Movie(container, "1.mp4");
        myMovie.loop();
    }

    ContentVideo1(PApplet container) {
        super(container);
        myMovie = new Movie(container, "1.mp4");
        myMovie.loop();
    }

    public @Override
    PImage draw() {
        if (myMovie.available()) {
            myMovie.read();
        }
        // image (myMovie, 500, 0);
        return myMovie.get();
    }
    public @Override
    void destroy(){
      println("Destroy movie");
      this.myMovie.stop();
      this.myMovie.dispose();
    }

}


class ContentVideo2 extends ContentView
{
  Movie movie;
  Movie myMovie;

  ContentVideo2(PApplet container, int canvasWidth, int canvasHeight) {
    super(container, canvasWidth, canvasHeight);

    myMovie = new Movie(container, "3.mp4");
    myMovie.loop();
  }

  ContentVideo2(PApplet container) {
    super(container);
    myMovie = new Movie(container, "3.mp4");
    myMovie.loop();
  }

  public @Override
  PImage draw() {
    if (myMovie.available()) {
      myMovie.read();
    }
    // image (myMovie, 500, 0);
    return myMovie.get();
  }
  public @Override
  void destroy(){
    println("Destroy movie");
    this.myMovie.stop();
    this.myMovie.dispose();
  }
}


class ContentVideo3 extends ContentView
{
    Movie movie;
    Movie myMovie;

    ContentVideo3(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);

        myMovie = new Movie(container, "4.mp4");
        myMovie.loop();
    }

    ContentVideo3(PApplet container) {
        super(container);
        myMovie = new Movie(container, "4.mp4");
        myMovie.loop();
    }

    public @Override
    PImage draw() {
        if (myMovie.available()) {
            myMovie.read();
        }
        // image (myMovie, 500, 0);
        return myMovie.get();
    }
    public @Override
    void destroy(){
      println("Destroy movie");
      this.myMovie.stop();
      this.myMovie.dispose();
    }
}
class ContentView
{
  int canvasWidth;
  int canvasHeight;
  PApplet container;

  ContentView (PApplet container, int canvasWidth, int canvasHeight){
    this.container = container;
    this.canvasWidth = canvasWidth;
    this.canvasHeight = canvasHeight;
  };
  ContentView (PApplet container){
    this.container = container;
  };

  public PImage draw(){
    return createGraphics(canvasWidth, canvasHeight);
  };

  public void destroy(){

  }
}


public class Controls extends PApplet {

  PApplet parent;
  int canvasWidth,canvasHeight;
  ControlP5 cp5;
  String name;
  Textfield jsonName,newSceneName,newScreenName;
  String filename;
  Textlabel filenameMsg;
  ScrollableList scenesDropdown,screensDropdown,contentDropdown;
  Group screensGroup,toolsGroup,sceneActionsGroup;
  RadioButton toolSwitch,resizeSwitch;
  Scene activeScene;
  Screen activeScreen;
  Button previewBtn,previewStopBtn;

  ArrayList<Scene> scenesList = new ArrayList<Scene>();
  List scenesListDropdown = new ArrayList();
  ArrayList<Screen> screensList = new ArrayList<Screen>();
  List screensListDropdown = new ArrayList();
  ArrayList<String> contentListDropdown = getContentFiles();

  public Controls(PApplet _parent, int _w, int _h, String _name) {
    super();
    this.parent = _parent;
    this.canvasWidth=_w;
    this.canvasHeight=_h;
    this.name = _name;
    PApplet.runSketch(new String[]{"--display=1",this.getClass().getName()}, this);
  }
  public void settings() {
    size(this.canvasWidth, this.canvasHeight);
  }

  public void setup() {

    surface.setTitle(this.name);
    surface.setLocation(0, 0);
    cp5 = new ControlP5(this);
    cp5.setFont(createFont("arial",14));

    cp5.addTextlabel("titleScenes")
    .setText("ESCENAS")
    .setPosition(100,100);

    scenesDropdown = cp5.addScrollableList("scenesDropdown")
    // .plugTo(this,"scenesDropdown")
    .setLabel("Escenas")
    .setPosition(100, 120)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(scenesListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN);

    newSceneName = cp5.addTextfield("newSceneName")
    // .plugTo(this,"newSceneName")
    .setLabel("Nueva escena")
    .setPosition(330, 120)
    .setSize(150,50);

    Label labelNewSceneName = newSceneName.getCaptionLabel();
    labelNewSceneName.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

    cp5.addButton("addScene")
    // .plugTo(this,"addScene")
    .setLabel("Agregar escena")
    .setPosition(500, 120)
    .setSize(150, 50);

    sceneActionsGroup = cp5.addGroup("sceneActionsGroup");
    sceneActionsGroup.setLabel("");
    sceneActionsGroup.setVisible(false);

    cp5.addButton("duplicateScene")
    // .plugTo(this,"addScene")
    .setLabel("Duplicar escena")
    .setPosition(660, 120)
    .moveTo(sceneActionsGroup)
    .setSize(150, 50);

    cp5.addButton("deleteScene")
    // .plugTo(this,"deleteScene")
    .setLabel("Quitar escena")
    .setPosition(870, 120)
    .setColorLabel(color(255,0,0))
    .setSize(150, 50)
    .moveTo(sceneActionsGroup);

    cp5.addTextlabel("titleScreens")
    .setText("PANTALLAS")
    .setPosition(100,260);

    screensGroup = cp5.addGroup("screensGroup");
    screensGroup.setVisible(false);
    screensGroup.setLabel("");

    screensDropdown = cp5.addScrollableList("screensDropdown")
    // .plugTo(this,"screensDropdown")
    .setLabel("Pantallas")
    .setPosition(100, 280)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(screensListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN)
    .moveTo(screensGroup);


    newScreenName = cp5.addTextfield("newScreenName")
    // .plugTo(this,"newScreenName")
    .setLabel("Nueva pantalla")
    .setPosition(330, 280)
    .setSize(150,50)
    .moveTo(screensGroup);

    Label labelNewScreenName = newScreenName.getCaptionLabel();
    labelNewScreenName.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

    cp5.addButton("addScreen")
    // .plugTo(this,"addScreen")
    .setLabel("Agregar pantalla")
    .setPosition(500, 280)
    .setSize(150, 50)
    .moveTo(screensGroup);

    cp5.addButton("clearScreen")
    // .plugTo(this,"clear")
    .setLabel("Limpiar")
    .setPosition(750, 280)
    .setSize(100, 50)
    .moveTo(screensGroup);

    cp5.addButton("deleteScreen")
    // .plugTo(this,"deleteScreen")
    .setLabel("Quitar pantalla")
    .setPosition(870, 280)
    .setSize(150, 50)
    .setColorLabel(color(255,0,0))
    .moveTo(screensGroup);

    toolsGroup = cp5.addGroup("toolsGroup");
    toolsGroup.setVisible(false);
    toolsGroup.setLabel("");

    cp5.addTextlabel("titleTools")
    .setText("HERRAMIENTAS")
    .setPosition(100,450);

    toolSwitch = cp5.addRadioButton("toolSwitch")
    // .plugTo(this,"toolSwitch")
    .setPosition(100,490)
    .setSize(40,20)
    .setColorForeground(color(120))
    .setColorActive(color(255))
    .setColorLabel(color(255))
    .setItemsPerRow(1)
    .setSpacingColumn(100)
    .addItem("Elipse",1)
    .addItem("Polígono",2)
    .addItem("Mouse",3)
    .moveTo(toolsGroup);

    cp5.addTextlabel("titleContents")
    .setText("CONTENIDOS")
    .setPosition(320,450);

    contentDropdown = cp5.addScrollableList("contentDropdown")
    .setLabel("Contenido")
    .setPosition(320, 490)
    .setSize(200, 150)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(contentListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN)
    .moveTo(toolsGroup);

    cp5.addTextlabel("titleResize")
    .setText("REDIMENSIONADO")
    .setPosition(560,450);

    resizeSwitch = cp5.addRadioButton("resizeSwitch")
    // .plugTo(this,"toolSwitch")
    .setPosition(560,490)
    .setSize(40,20)
    .setColorForeground(color(120))
    .setColorActive(color(255))
    .setColorLabel(color(255))
    .setItemsPerRow(1)
    .setSpacingColumn(100)
    .addItem("Cubrir",1)
    .addItem("Contener",2)
    .addItem("No redimensionar",3)
    .moveTo(toolsGroup);

    previewBtn = cp5.addButton("preview")
    .setLabel("Previsualizar")
    .setPosition(850,490)
    .setSize(150, 50)
    .setColorLabel(color(255,0,0))
    .moveTo(toolsGroup);

    previewStopBtn = cp5.addButton("previewStop")
    .setLabel("Detener")
    .setPosition(850,490)
    .setSize(150, 50)
    .setColorLabel(color(255,0,0))
    .setVisible(false)
    .moveTo(toolsGroup);

    jsonName = cp5.addTextfield("jsonName")
    // .plugTo(this,"jsonName")
    .setLabel("Nombre del archivo")
    .setPosition(100,690)
    .setSize(200,50);

    Label labelJsonName = jsonName.getCaptionLabel();
    labelJsonName.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

    cp5.addButton("saveConfig")
    // .plugTo(this,"saveConfig")
    .setLabel("Guardar configuracion")
    .setPosition(320, 690)
    .setSize(200, 50);

    filenameMsg = cp5.addTextlabel("filenameMsg")
    // .plugTo(this,"filenameEmpty")
    .setText("Ingresa un nombre para el archivo")
    .setColorValue(0xFFFF0000)
    .setFont(createFont("arial",20))
    .setPosition(540,700)
    .setSize(200,50)
    .setVisible(false);
  }


  public void controlEvent(ControlEvent theEvent) {
    //Get every event name
    println("controls");
    String value = nf(theEvent.getValue());
    println(theEvent.getName());
    String sceneName;
    switch(theEvent.getName()){
      case "toolSwitch":
      switch (value){
        case "1":
        gui.shapeType = "Ellipse";
        break;
        case "2":
        gui.shapeType = "Polygon";
        break;
        case "3":
        gui.shapeType = "Mouse";
        break;
        case "-1":
        gui.shapeType = "Mouse";
        toolSwitch.activate(2);
        break;
      }
      break;
      case "loadConfig":
      // this.parent.loadConfig();
      // loadScenes();
      break;
      case "saveConfig":
      filename = trim(jsonName.getText());
      //In case no filename specified
      if(filename.equals("")){
        filenameMsg.setVisible(true);
        filenameMsg.setColorLabel(color(255,0,0))
        .setColorValue(0xFFFF0000)
        .setText("Ingresa un nombre para el archivo.");
        jsonName.setColorLabel(color(255,0,0));
      }
      else
      {
        filenameMsg.setVisible(false)
        .setColorLabel(color(255,255,255));
        jsonName.setColorLabel(color(255,255,255));
        Boolean result = this.saveConfig(filename);
        if(result){
          println("success saving file");
          jsonName.setColorLabel(color(0,255,0));
          filenameMsg.setVisible(true)
          .setColorValue(0xFF00FF00)
          .setText("Archivo guardado correctamente.");
        }
        else
        {
          println("error saving file");
          jsonName.setColorLabel(color(255,0,0));
          filenameMsg.setVisible(true)
          .setColorLabel(color(0,255,0))
          .setText("Error al guardar el archivo.");
        }
      }
      break;
      case "addScene":
      sceneName = trim(newSceneName.getText());
      if(sceneName.equals("")){
        newSceneName.setColorLabel(color(255,0,0));
      }
      else{
        newSceneName.setColorLabel(color(255,255,255));
        addScene(sceneName);
      }

      break;
      case "deleteScene":
      this.deleteScene();
      break;
      case "duplicateScene":
      sceneName = trim(newSceneName.getText());
      if(sceneName.equals("")){
        newSceneName.setColorLabel(color(255,0,0));
      }
      else{
        newSceneName.setColorLabel(color(255,255,255));
        duplicateScene(sceneName);
      }
      case "addScreen":
      String screenName = trim(newScreenName.getText());
      if(screenName.equals("")){
        newScreenName.setColorLabel(color(255,0,0));
      }
      else{
        newScreenName.setColorLabel(color(255,255,255));
        addScreen(screenName,scenesDropdown.getStringValue());
      }
      break;
      case "clearScreen":
      activeScreen.clearShapes();
      break;
      case "deleteScreen":
      this.deleteScreen();
      break;
      case "scenesDropdown":
      screensGroup.setVisible(true);
      sceneActionsGroup.setVisible(true);
      activeScene = scenesList.get(PApplet.parseInt(theEvent.getValue()));
      this.updateScreensDropdown();
      this.activeScreen = null;
      previewContent(false);
      break;
      case "screensDropdown":
      toolsGroup.setVisible(true);
      activeScreen = screensList.get(PApplet.parseInt(theEvent.getValue()));
      gui.canvas.active = null;
      gui.canvas.closePolygon();
      activeScreen.setContentClass(contentListDropdown.get(PApplet.parseInt(theEvent.getValue())));
      previewContent(false);

      //Set resize switch
      switch (activeScreen.getResizeMode()){
        case "Cover":
        resizeSwitch.activate(0);
        break;
        case "Contain":
        resizeSwitch.activate(1);
        break;
        case "Noresize":
        resizeSwitch.activate(2);
        break;
        default:
        resizeSwitch.activate(-1);
        break;
      }
      //Set content dropdown
      switch (activeScreen.getContentClass()){
        case "ContentBalls":
        contentDropdown.setValue(0);
        break;
        case "ContentVideo":
        contentDropdown.setValue(1);
        break;
        case "ContentVideo1":
        contentDropdown.setValue(2);
        break;
        case "ContentVideo2":
        contentDropdown.setValue(3);
        break;
        case "ContentVideo3":
        contentDropdown.setValue(4);
        break;

      }
      //End screensDropdown
      break;
      case "contentDropdown":
      println(value);
      switch (value){
        case "0":
        activeScreen.setContentClass("ContentBalls");
        break;
        case "1":
        activeScreen.setContentClass("ContentVideo");
        break;
        case "2":
        activeScreen.setContentClass("ContentVideo1");
        break;
        case "3":
        activeScreen.setContentClass("ContentVideo2");
        break;
        case "4":
        activeScreen.setContentClass("ContentVideo3");
        break;
      }
      break;
      case "resizeSwitch":
        switch (value){
          case "1":
          activeScreen.setResizeMode("Cover");
          break;
          case "2":
          activeScreen.setResizeMode("Contain");
          break;
          case "3":
          activeScreen.setResizeMode("Noresize");
          break;
          case "-1":
          activeScreen.setResizeMode("Noresize");
          break;
        }
      break;
      case "preview":
      previewContent(true);
      break;
      case "previewStop":
      previewContent(false);
      break;
    }
  }
  public void addScene (String sceneName){
    Scene s = new Scene(sceneName);
    scenesList.add(s);
    scenesListDropdown.add(sceneName);
    updateScenesDropdown();

    //Clear textfield input
    newSceneName.clear();
  }
  public void duplicateScene (String sceneName){
    Scene s = new Scene(sceneName);
    //Clone Screens
    ArrayList<Screen> screensList = activeScene.getScreens();
    ArrayList<Screen> clone = new ArrayList<Screen>(screensList.size());
    for(int i = 0; i< screensList.size();i++){
      clone.add(screensList.get(i).clone());
    }
    s.setScreens(clone);

    scenesList.add(s);
    scenesListDropdown.add(sceneName);
    updateScenesDropdown();

    //Clear textfield input
    newSceneName.clear();
  }
  public void updateScenesDropdown(){
    cp5.remove("scenesDropdown");
    scenesDropdown = cp5.addScrollableList("scenesDropdown")
    .plugTo(this.parent,"scenesDropdown")
    .setLabel("Escenas")
    .setPosition(100, 120)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(scenesListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN);
  }
  public void addScreen (String screenName,String sceneName){
    Screen s = new Screen(screenName);
    Scene scene = null;
    for (int i = 0; i < scenesList.size(); i++){
      if (sceneName.equals(scenesList.get(i).getName())){
        scene = scenesList.get(i);
      }
    }
    if (scene != null){
      scene.addScreen(s);
    }
    activeScene.addScreen(s);

    //Clear textfield input
    newScreenName.clear();
    this.updateScreensDropdown();
  }
  public void updateScreensDropdown(){
    screensListDropdown =  new ArrayList();

    if(this.activeScene != null){
      this.screensList = activeScene.getScreens();
      // println(this.screensList.get(0).getShapesList().size());
      for(int i =0; i < this.screensList.size() ;i++){
        screensListDropdown.add(this.screensList.get(i).getName());
      }
    }
    cp5.remove("screensDropdown");
    screensDropdown = cp5.addScrollableList("screensDropdown")
    .plugTo(this.parent,"screensDropdown")
    .setLabel("Pantallas")
    .setPosition(100, 280)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(screensListDropdown)
    .moveTo(screensGroup)
    .close()
    .setType(ScrollableList.DROPDOWN);
  }
  public void deleteScene(){
    for(int i = 0 ; i<this.scenesList.size();i++){
      if (this.scenesList.get(i).getName() == this.activeScene.getName()){
        scenesList.remove(i);
        scenesListDropdown.remove(i);
      }
    }
    this.updateScenesDropdown();
    screensGroup.setVisible(false);
    toolsGroup.setVisible(false);
    sceneActionsGroup.setVisible(false);
  }
  public void deleteScreen(){
    for(int i = 0 ; i<this.screensList.size();i++){
      if (this.screensList.get(i).getName() == this.activeScreen.getName()){
        screensList.remove(i);
        screensListDropdown.remove(i);
      }
    }
    this.updateScreensDropdown();
    toolsGroup.setVisible(false);
    this.activeScreen = null;
  }
  public void loadScenes() {
    // parent.loadScenes();
    // selectInput("Select a file to process:", "fileSelected");
  }

  public Boolean saveConfig(String filename){
    println("saving JSON config file");

    //Main Config
    JSONObject conf = new JSONObject();
    conf.setInt("canvasWidth",this.canvasWidth);
    conf.setInt("canvasHeight",this.canvasHeight);

    //Scenes
    println("saving scenesList");
    JSONArray scenes = new JSONArray();
    Scene scene;
    for (int i = 0; i < scenesList.size(); i++){
      scene = scenesList.get(i);
      JSONObject sceneObj = new JSONObject();
      sceneObj.setString("name", scene.getName());

      //Screens
      println("saving Screens");
      JSONArray screens = new JSONArray();
      Screen screen;
      String maskName;
      for (int j = 0; j < scene.getScreens().size(); j++) {
        println(j);
        screen = scene.getScreens().get(j);
        JSONObject screenObj = new JSONObject();
        screenObj.setString("name", screen.getName());
        screenObj.setInt("color", screen.getColor());
        screenObj.setString("contentClass", screen.getContentClass());
        screenObj.setJSONObject("contentTopLeft", screen.getContentTopLeft());
        screenObj.setJSONObject("contentBottomRight", screen.getContentBottomRight());
        screenObj.setString("resizeMode", screen.getResizeMode());
        screenObj.setJSONArray("shapes", screen.getCoords());

        //Screen mask
        maskName = "data/" + filename +"/maskE" + i + "S" + j + ".tif";
        screen.saveMask(maskName);
        screenObj.setString("mask", maskName);
        screens.append(screenObj);
      }
      sceneObj.setJSONArray("screens", screens);
      scenes.append(sceneObj);
      println("scenes saved");
    }

    conf.setJSONArray("scenes", scenes);

    return saveConfigSetup(conf, filename);
  }
  public void keyPressed() {
    //Check if changing screen
    switch (key) {
      case ESC:
      key = 0;
      break;
    }
  }
  public Screen getActiveScreen(){
    return activeScreen;
  }
  public Scene getActiveScene(){
    return activeScene;
  }
  public void draw() {
    background(0);
    pushStyle();
    stroke(255, 255, 255);
    line(0, 85, 1024,85);
    line(0, 250, 1024,250);
    line(0, 435, 1024,435);
    line(0, 655, 1024,655);
    popStyle();
  }
  public void previewContent(Boolean value){
    if(value == true){
      previewBtn.hide();
      previewStopBtn.show();
      gui.setPreviewMode(true,this.activeScreen);
    }else{
      previewBtn.show();
      previewStopBtn.hide();
      gui.setPreviewMode(false,null);
    }
  }

}
public ArrayList<String> getContentFiles() {
  String[] filenames = listFileNames(dataPath(""));
  ArrayList<String> ret = new ArrayList();
  for(int i = 0; i<filenames.length; i++){
    if (filenames[i].toLowerCase().startsWith("content")){
      ret.add(filenames[i].substring(0, filenames[i].lastIndexOf('.')));
    };
  }
  return ret;
}

public String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
    } else {
      // If it's not a directory
      return null;
    }
  }
class Ellipse implements Shape{

  PShape mEllipse;
  PVector mTopLeft;
  PVector mBottomRight;
  int mColor;
  PVector[] verticesArray = new PVector[4];

  Boolean selected;
  Integer selectedVertex;

  int VERTICES_RADIUS = 5;

  Ellipse (int c, int mMouseX, int mMouseY) {
    mTopLeft = new PVector(mMouseX, mMouseY);
    mBottomRight = new PVector(mMouseX, mMouseY);
    mColor = c;
    updateEllipse();
    updateVerticesFromCorners();
    selected = false;
  }

  public int getColor(){
    return mColor;
  }

  public String getType(){
    return "Ellipse";
  }

  //Draw ellipse
  public PShape display() {
    return mEllipse;
  }

  //Check if we clicked inside the ellipse
  public Boolean inside(int mMouseX, int mMouseY,int c) {
    //If color in mouse position is equal to ellipse color, then is inside
    return c == mColor;
  }

  //Check if we clicked inside a vertex and set selectedVertex to vertex's index in vertexArray
  public Boolean checkSelectedVertex(int mMouseX, int mMouseY) {
    selectedVertex = null;
    for (int i = 0; i < verticesArray.length; i++) {
      if (dist(verticesArray[i].x, verticesArray[i].y, mMouseX, mMouseY) < (1.5f)*VERTICES_RADIUS) {
        selectedVertex = i;
        return true;
      }
    }
    return false;
  }

  public PShape getVerticesGroup(){
    PShape group =createShape(GROUP);
    for (int i = 0; i < verticesArray.length; i++) {

      //Create vertex circle
      ellipseMode(RADIUS);
      PShape elip = createShape(ELLIPSE, verticesArray[i].x, verticesArray[i].y, VERTICES_RADIUS, VERTICES_RADIUS);
      elip.setStroke(0);
      //if vertex is selected fill with red
      if (selectedVertex != null && selectedVertex == i) {
        elip.setFill(color(255, 0, 0));
      }
      group.addChild(elip);
    }
    return group;
  }
  //Update bottom-right corner of ellipse
  public void setBottomRight(int mMouseX, int mMouseY) {
    mBottomRight = new PVector (mMouseX, mMouseY);
    updateEllipse();
    updateVerticesFromCorners();
  }

  //Move ellipse or selectedVertex
  public void move(int x, int y, int pmx,int pmy) {

    //Check if moving selectedVertex
    if (selectedVertex == null) {
      //Update ellipse's corners
      mBottomRight.x = mBottomRight.x + (x-pmx);
      mBottomRight.y = mBottomRight.y + (y-pmy);
      mTopLeft.x = mTopLeft.x + (x - pmx);
      mTopLeft.y = mTopLeft.y + (y - pmy);
    }
    else {

      //Move selectedVertex
      switch(selectedVertex) {
        //Top vertex
        case 0:
        mTopLeft.y = verticesArray[selectedVertex].y + (y - pmy);
        break;
        //Right vertex
        case 1:
        mBottomRight.x = verticesArray[selectedVertex].x + (x - pmx);
        break;
        //Bottom vertex
        case 2:
        mBottomRight.y = verticesArray[selectedVertex].y + (y - pmy);
        break;
        //Left vertex
        case 3:
        mTopLeft.x = verticesArray[selectedVertex].x + (x - pmx);
        break;
      }
    }
    updateEllipse();
    updateVerticesFromCorners();
  }

  public void moveStep(int x,int y){
    if(selectedVertex == null){
      mBottomRight.x = mBottomRight.x + x;
      mBottomRight.y = mBottomRight.y + y;
      mTopLeft.x = mTopLeft.x + x;
      mTopLeft.y = mTopLeft.y + y;
      updateEllipse();
      updateVerticesFromCorners();
    }
  }

  //Recreate ellipse from corners
  public void updateEllipse() {
    ellipseMode(CORNERS);
    pushStyle();
    beginShape();
    mEllipse = createShape(ELLIPSE, mTopLeft.x, mTopLeft.y, mBottomRight.x, mBottomRight.y);
    mEllipse.setFill(mColor);
    mEllipse.noStroke();
    endShape();
    popStyle();
  }

  //Set if ellipse is selected
  public void setSelected(Boolean sel) {
    this.selected = sel;
  }

  //Updates verticesArray from ellipse's corners
  public void updateVerticesFromCorners() {
    verticesArray[0] = new PVector((mBottomRight.x - mTopLeft.x) / 2 + mTopLeft.x, mTopLeft.y);
    verticesArray[1] = new PVector(mBottomRight.x, (mBottomRight.y - mTopLeft.y) / 2 + mTopLeft.y);
    verticesArray[2] = new PVector((mBottomRight.x - mTopLeft.x) / 2 + mTopLeft.x, mBottomRight.y);
    verticesArray[3] = new PVector(mTopLeft.x, (mBottomRight.y - mTopLeft.y) / 2 + mTopLeft.y);
  }

  //Updates ellipse's corners from verticesArray
  public void updateCornersFromVertices() {
    mBottomRight.x =  verticesArray[1].x;
    mBottomRight.y = verticesArray[2].y;
    mTopLeft.x = verticesArray[3].x;
    mTopLeft.y = verticesArray[0].y;
  }

  //Moves the selectedVertex
  public void moveSelectedVertex(int direction) {

    //Check if we have a selected vertex
    if (selectedVertex != null) {

      //if selected vertex is the left or right one
      if (selectedVertex == 1 || selectedVertex == 3) {
        switch(direction) {
          case LEFT:
          verticesArray[selectedVertex].x--;
          break;
          case RIGHT:
          verticesArray[selectedVertex].x++;
          break;
        }
      }
      else {
        //Selected vertex is the top or bottom one
        switch(direction) {
          case DOWN:
          verticesArray[selectedVertex].y++;
          break;
          case UP:
          verticesArray[selectedVertex].y--;
          break;
        }
      }
      updateCornersFromVertices();
      updateVerticesFromCorners();
      updateEllipse();
    }
  }

  public JSONArray getCoords() {
    JSONArray coords = new JSONArray();

    //Add topLeft coords
    JSONObject topLeftCoords = new JSONObject();
    topLeftCoords.setFloat("x", mTopLeft.x);
    topLeftCoords.setFloat("y", mTopLeft.y);
    coords.append(topLeftCoords);

    //Add bottomRight coords
    JSONObject bottomRightCoords = new JSONObject();
    bottomRightCoords.setFloat("x", mBottomRight.x);
    bottomRightCoords.setFloat("y", mBottomRight.y);
    coords.append(bottomRightCoords);
    return coords;
  }

  public ArrayList<PVector> getVerticesArray(){
    ArrayList<PVector> ret = new ArrayList<PVector>(Arrays.asList(verticesArray));
    return ret;
  }

  //gets the max X coord
  public int getMaxX(){
    println(mTopLeft.x+" "+mTopLeft.y+" "+mBottomRight.x+" "+mBottomRight.y);
    return (int) max(mTopLeft.x, mBottomRight.x);
  }

  //gets the min X coord
  public int getMinX(){
    return (int) min(mTopLeft.x, mBottomRight.x);
  }

  //gets the max Y coord
  public int getMaxY(){
    return (int) max(mTopLeft.y, mBottomRight.y);
  }

  //gets the min Y coord
  public int getMinY(){
    return (int) min(mTopLeft.y, mBottomRight.y);
  }
}
class Gui {
  public Controls controls;
  public Canvas canvas;
  public String shapeType = "Mouse";
  public int screenColor;
  public Boolean previewMode = false;
  public PApplet context;
  public ContentView content;

  Gui(PApplet context, int width, int height) {
    this.controls = new Controls(context, 1024, 768, "Controles");
    this.canvas = new Canvas(context, width, height, "Lienzo");
    this.context = context;
  }

  public Screen getActiveScreen(){
    return this.controls.getActiveScreen();
  }
  public Scene getActiveScene(){
    return this.controls.getActiveScene();
  }

  public Boolean getPreviewMode(){
    return this.previewMode;
  }
  public void setPreviewMode(Boolean value,Screen screen){
    if(value == true){
      canvas.preview = new Preview(screen,this.context);
      canvas.preview.saveMask();
    }else{
      canvas.preview.destroy();
      canvas.preview = null;
    }
    this.previewMode = value;
  }

}
class Polygon implements Shape{

  PShape mPolygon;
  ArrayList<PVector> verticesArray = new ArrayList<PVector>();
  int mColor;

  Boolean selected;
  Integer selectedVertex;

  int VERTICES_RADIUS = 5;

  Polygon (int c) {
    mColor = c;
    selected = false;
    mPolygon = createShape();
  }

  public int getColor(){
    return mColor;
  }

  public String getType(){
    return "Polygon";
  }

  //Draw polygon
  public PShape display() {
    return mPolygon;
  }
 //<>//
  public PShape getVerticesGroup(){
    PShape group = createShape(GROUP);
    for (int i = 0; i < verticesArray.size(); i++) {

        //Create vertex circle
        ellipseMode(RADIUS);
        PShape elip = createShape(ELLIPSE, verticesArray.get(i).x, verticesArray.get(i).y, VERTICES_RADIUS, VERTICES_RADIUS);
        elip.setStroke(0);
        //if vertex is selected fill with red
        if (selectedVertex != null && selectedVertex == i) {
          elip.setFill(color(255, 0, 0));
        }

        //Draw vertex
        group.addChild(elip);
      }
    return group;
  }
  //Adds new vertex to polygon
  public void addVertex(int mMouseX, int mMouseY) {

    // Create new polygon
    mPolygon = createShape(); //<>//
    mPolygon.beginShape();

    //Add new vertex to verticesArray
    PVector vertex = new PVector (mMouseX, mMouseY);
    verticesArray.add(vertex);

    //Add all vertices in verticesArray to polygon
    for (int i = 0; i < verticesArray.size(); i++) {
      mPolygon.vertex(verticesArray.get(i).x, verticesArray.get(i).y);
    };

    mPolygon.endShape(CLOSE);
    mPolygon.setFill(mColor);
    mPolygon.setStroke(color(255, 0, 0));
  }

  //Check if we clicked inside the ellipse
  public Boolean inside(int mMouseX, int mMouseY,int c) {
    //If color in mouse position is equal to ellipse color, then is inside
    return c == mColor;
  }

  //Check if we clicked inside a vertex and set selectedVertex to vertex's index in vertexArray
  public Boolean checkSelectedVertex(int mMouseX, int mMouseY) {
    selectedVertex = null;
    for (int i = 0; i < verticesArray.size(); i++) {
      if (dist(verticesArray.get(i).x, verticesArray.get(i).y, mMouseX, mMouseY) < (1.5f)*VERTICES_RADIUS) {
        selectedVertex = i;
        return true;
      }
    }
    return false;
  }

  //Clones this Polygon
  public Polygon clone() {

    //Create new Polygon with this color
    Polygon polygon = new Polygon(mColor);
    //Clone verticesArray
    polygon.verticesArray = (ArrayList) verticesArray.clone();

    //Create polygon
    polygon.mPolygon = createShape();
    polygon.mPolygon.beginShape();


    //Remove last added vertices as we used doble click to finish polygon shape, so an additional vertex was added
    polygon.verticesArray.remove(polygon.verticesArray.size() - 1);

    //Add all vertices in verticesArray to polygon
    for (int i = 0; i < polygon.verticesArray.size(); i++) {
      polygon.mPolygon.vertex(polygon.verticesArray.get(i).x, polygon.verticesArray.get(i).y);
    };

    polygon.mPolygon.endShape(CLOSE);
    polygon.mPolygon.setFill(mColor);
    polygon.mPolygon.setStroke(color(255, 0, 0));

    return polygon;
  }

  //Move polygon or selectedVertex
  public void move(int x, int y, int pmx,int pmy) {

    //Check if moving selectedVertex
    if (selectedVertex == null) {

      //Move all vertices in mPolygon
      for (int i = 0; i < mPolygon.getVertexCount(); i++) {
        PVector newxy = new PVector (mPolygon.getVertex(i).x + (x - pmx), mPolygon.getVertex(i).y + (y - pmy));
        mPolygon.setVertex(i, newxy);
      }
      //Move all vertices in verticesArray
      for (int i = 0; i < verticesArray.size(); i++) {
        verticesArray.get(i).x = verticesArray.get(i).x + (x - pmx);
        verticesArray.get(i).y = verticesArray.get(i).y + (y - pmy);
      }
    }
    else {
      //Move vertex
      verticesArray.get(selectedVertex).x = verticesArray.get(selectedVertex).x + (x - pmx);
      verticesArray.get(selectedVertex).y = verticesArray.get(selectedVertex).y + (y - pmy);
      mPolygon.setVertex(selectedVertex, verticesArray.get(selectedVertex));
    }
  }
  public void moveStep(int x,int y){

    if (selectedVertex == null) {
      //Move all vertices in mPolygon
      for (int i = 0; i < mPolygon.getVertexCount(); i++) {
        PVector newxy = new PVector (mPolygon.getVertex(i).x + x, mPolygon.getVertex(i).y + y);
        mPolygon.setVertex(i, newxy);
      }
      //Move all vertices in verticesArray
      for (int i = 0; i < verticesArray.size(); i++) {
        verticesArray.get(i).x = verticesArray.get(i).x + x;
        verticesArray.get(i).y = verticesArray.get(i).y + y;
      }
    }
    else{

    }
  }

  //Set if polygon is selected
  public void setSelected(Boolean sel) {
    selected = sel;
  }

  //Moves the selectedVertex
  public void moveSelectedVertex(int direction) {
    if (selectedVertex != null) {

      //Update selectedVertex in verticesArray
      switch(direction) {
        case DOWN:
        verticesArray.get(selectedVertex).y++;
        break;
        case UP:
        verticesArray.get(selectedVertex).y--;
        break;
        case LEFT:
        verticesArray.get(selectedVertex).x--;
        break;
        case RIGHT:
        verticesArray.get(selectedVertex).x++;
        break;
      }

      //Updates selectedVertex in polygon
      mPolygon.setVertex(selectedVertex, verticesArray.get(selectedVertex));
    }
  }

  public JSONArray getCoords(){
    JSONArray coords = new JSONArray();

    //Add vertices to coords
    for (int i = 0; i < verticesArray.size(); i++) {
      JSONObject vertexCoords = new JSONObject();
      vertexCoords.setFloat("x", verticesArray.get(i).x);
      vertexCoords.setFloat("y", verticesArray.get(i).y);
      coords.append(vertexCoords);
    }
    return coords;
  }

  public ArrayList<PVector> getVerticesArray(){
    return verticesArray;
  }

  //gets the max X coord
 public int getMaxX(){
   int maxX = 0;
   for (int i = 0; i < verticesArray.size(); i++) {
     maxX = (int) max(maxX, verticesArray.get(i).x);
   }
   return maxX;
 }

 //gets the min X coord
 public int getMinX(){
   int minX = 100000;
   for (int i = 0; i < verticesArray.size(); i++) {
     minX = (int) min(minX, verticesArray.get(i).x);
   }
   return minX;
 }

 //gets the max Y coord
 public int getMaxY(){
   int maxY = 0;
   for (int i = 0; i < verticesArray.size(); i++) {
     maxY = (int) max(maxY, verticesArray.get(i).y);
   }
   return maxY;
 }

 //gets the min Y coord
 public int getMinY(){
   int minY = 100000;
   for (int i = 0; i < verticesArray.size(); i++) {
     minY = (int) min(minY, verticesArray.get(i).y);
   }
   return minY;
 }
}
class Preview
{
  PVector contentTopLeft;
  PVector contentBottomRight;
  String resizeMode = "Noresize"; // Contain //Noresize
  String mask,contentClass;
  PImage maskImage;
  PImage contentImage;
  PGraphics imageInCanvas;
  Screen screen;
  float positionX = 0;
  float positionY = 0;
  float scale = -1;
  int finalImageWidth;
  int finalImageHeight;
  boolean adjustedByWidth = false;
  ContentView content;
  PApplet parent;

  Preview(Screen screen, PApplet parent){
    this.screen = screen;
    //Set bounds
    JSONObject topLeft = screen.getContentTopLeft();
    JSONObject bottomRight = screen.getContentBottomRight();
    contentTopLeft = new PVector((int)topLeft.get("x"),(int)topLeft.get("y"));
    contentBottomRight = new PVector((int)bottomRight.get("x"),(int)bottomRight.get("y"));

    this.imageInCanvas = createGraphics(gui.canvas.canvasWidth, gui.canvas.canvasHeight);
    this.parent = parent;
    switch (screen.getContentClass()){
      case "ContentBalls":
      content = new ContentBalls(this.parent,gui.canvas.canvasWidth, gui.canvas.canvasHeight);
      break;
      case "ContentVideo":
      content = new ContentVideo(this.parent);
      break;
      case "ContentVideo1":
      content = new ContentVideo1(this.parent);
      break;
      case "ContentVideo2":
      content = new ContentVideo2(this.parent);
      break;
      case "ContentVideo3":
      content = new ContentVideo3(this.parent);
      break;
    }
  }
  public void setContentTopLeft(int x, int y){
    this.contentTopLeft = new PVector(x,y);
  };
  public void setContentBottomRight(int x, int y){
    this.contentBottomRight = new PVector(x,y);
  };
  public void setMask(String _mask){
    this.mask = _mask;
  };
  public void setContentClass(String cc){
    this.contentClass = cc;
  };
  public void setContent(ContentView content){
    this.content = content;
  };
  public void setResizeMode(String rm){
    this.resizeMode = rm;
  };

  public PImage draw(){
    //Get content image
    contentImage = content.draw();

    if (this.scale == -1 && contentImage.width > 0 && contentImage.height > 0){
      // Compute scale only once
      this.adjustedByWidth = computeScale(contentImage);
    }
    else {
      //Resize image so it best fit screen container
      // println(this.screen.resizeMode);
      if (!this.screen.resizeMode.equals("Noresize")){
        resizeImage(contentImage);
      }

      //Compute position in canvas only once
      if (positionX == -1 && positionY == -1){
        computePosition(contentImage);
      }
      //insert image in canvas size
      imageInCanvas.beginDraw();
      // // imageInCanvas.clear();
      imageInCanvas.image(contentImage, 0, 0);
      imageInCanvas.endDraw();
      // println("ori: " + imageInCanvas.width + " " + imageInCanvas.height + " - "+ mask.width + " " + mask.height);
      imageInCanvas.mask(maskImage);

      // image(imageInCanvas, positionX, positionY);
    }
    return imageInCanvas;
  }

  public String getContentClass(){
    return this.contentClass;
  }

  public JSONObject getContentTopLeft(){
    JSONObject topLeft = new JSONObject();
    topLeft.put("x", PApplet.parseInt(contentTopLeft.x));
    topLeft.put("y", PApplet.parseInt(contentTopLeft.y));
    return topLeft;
  }

  public JSONObject getContentBottomRight(){
    JSONObject bottomRight = new JSONObject();
    bottomRight.setInt("x", PApplet.parseInt(contentBottomRight.x));
    bottomRight.setInt("y", PApplet.parseInt(contentBottomRight.y));
    return bottomRight;
  }

  public String getResizeMode(){
    return this.screen.resizeMode;
  }

  public void saveMask(){
    println("Saving mask image");
    ArrayList<PVector> verticesArray;
    // PGraphics mask = createGraphics(ceil(abs(contentBottomRight.x - contentTopLeft.x)), ceil(abs(contentBottomRight.y - contentTopLeft.y))); //<>//
    // PGraphics mask = createGraphics(1024,768);
    PGraphics mask = createGraphics(gui.canvas.canvasWidth, gui.canvas.canvasHeight);
    ArrayList<Shape> shapes = this.screen.getShapesList();

    mask.beginDraw();
    mask.background(0);
    mask.fill(255);
    mask.noStroke();
    for (int i = 0; i < shapes.size(); i++){

      Shape shape = shapes.get(i);
      verticesArray = shape.getVerticesArray();
      switch (shape.getType()){
        case "Polygon":
        mask.beginShape();
        for (int j = 0; j < verticesArray.size(); j++){
          mask.vertex(round(verticesArray.get(j).x - contentTopLeft.x), round(verticesArray.get(j).y - contentTopLeft.y));
        }
        mask.vertex(verticesArray.get(0).x - contentTopLeft.x , verticesArray.get(0).y - contentTopLeft.y);
        mask.endShape(CLOSE);
        break;
        case "Ellipse":
        mask.ellipseMode(CORNERS);
        mask.ellipse(shape.getMinX() - contentTopLeft.x, shape.getMinY() - contentTopLeft.y, shape.getMaxX() - contentTopLeft.x, shape.getMaxY() - contentTopLeft.y);
        break;
      }
    }
    //Save mask
    mask.beginDraw();
    mask.save("data/tmp_mask.tif");
    maskImage = loadImage("data/tmp_mask.tif");
    println("End Saving mask image");
  }

  public boolean computeScale(PImage image){
    float boundingBoxWidth = this.contentBottomRight.x - this.contentTopLeft.x;
    float boundingBoxHeight = this.contentBottomRight.y - this.contentTopLeft.y;

    float widthScale = 0;
    float heightScale = 0;

    if (image.width != 0){
      widthScale = boundingBoxWidth / image.width;
    }
    if (image.height != 0){
      heightScale = boundingBoxHeight / image.height;
    }
    this.scale = 0;
    this.finalImageWidth = 0;
    this.finalImageHeight = 0;
    boolean ret = false;
    switch(this.screen.resizeMode){
      case "Cover":
      this.scale = Math.max(widthScale, heightScale);
      this.finalImageWidth = (int)Math.floor(image.width * this.scale);
      ret = (this.scale == widthScale);
      break;
      case "Contain":
      this.scale = Math.min(widthScale, heightScale);
      this.finalImageHeight = (int)Math.floor(image.height * this.scale);
      ret = (this.scale == widthScale);
      break;
    }
    return ret;
  }

  public void resizeImage(PImage image){
    if (image.width > 0 && image.height > 0){
      image.resize(this.finalImageWidth, this.finalImageHeight);
    }
  }

  public void computePosition(PImage image){
    positionX = contentTopLeft.x;
    positionY = contentTopLeft.y;
    switch(this.screen.resizeMode) {
      case "Cover":
      if (adjustedByWidth) {
        positionY = contentTopLeft.y - (image.height - (contentBottomRight.y - contentTopLeft.y))/2;
      }
      else {
        positionX = contentTopLeft.x - (image.width - (contentBottomRight.x - contentTopLeft.x))/2;
      }
      break;
      case "Contain":
      if (adjustedByWidth) {
        positionY =  contentTopLeft.y + (((contentBottomRight.y - contentTopLeft.y) - image.height)/2);
      }
      else {
        positionX =  contentTopLeft.x + (((contentBottomRight.x - contentTopLeft.x) - image.width)/2);
      }
      break;
    }
  }
  public void destroy(){
    content.destroy();
    content = null;
  }
}
class Scene {
  String name;
  ArrayList<Screen> screens;


  Scene(String name){
    this.name = name;
    this.screens = new ArrayList<Screen>();
  }

  public void draw(){
    for (int i = 0; i < this.screens.size(); i++){
      this.screens.get(i).draw();
    }
  }

  public void addScreen(Screen screen){
    this.screens.add(screen);
  }
  public void loadScreens(ArrayList<Screen> scr){
    this.screens = scr;
  }
  public void setScreens(ArrayList<Screen> screens){
     this.screens = screens;
  }
  public ArrayList<Screen> getScreens(){
    return this.screens;
  }
  public String getName(){
    return this.name;
  }
}
class Screen
{
  PVector contentTopLeft;
  PVector contentBottomRight;
  ArrayList<Shape> shapes;
  Shape activeShape;
  Ellipse ellipse;
  Polygon polygon;
  String resizeMode = "Cover"; // Contain //Noresize
  String mask,contentClass;
  String name;

  Screen(String name){
    this.name = name;
    this.contentTopLeft = new PVector(0,0);
    this.contentBottomRight = new PVector(gui.canvas.canvasHeight,gui.canvas.canvasWidth);
    this.shapes = new ArrayList<Shape>();
    this.activeShape = null;
    this.ellipse = null;
    this.polygon = null;
  }
  public void setContentTopLeft(int x, int y){
    this.contentTopLeft = new PVector(x,y);
  };
  public void setContentBottomRight(int x, int y){
    this.contentBottomRight = new PVector(x,y);
  };
  public void setMask(String _mask){
    this.mask = _mask;
  };
  public void setContentClass(String cc){
    this.contentClass = cc;
  };
  public void setResizeMode(String rm){
    this.resizeMode = rm;
  };
  public void setShapes(JSONArray _shapes){
    for (int i = 0; i < _shapes.size(); i++) {
      JSONObject _s = (JSONObject) _shapes.getJSONObject(i);
      JSONArray coords = _s.getJSONArray("coords");
      int c = _s.getInt("color");

      switch(_s.getString("shapeType")){
        case "Ellipse":
        int x,y,x1,y1;
        x = coords.getJSONObject(0).getInt("x");
        y = coords.getJSONObject(0).getInt("y");
        x1 = coords.getJSONObject(1).getInt("x");
        y1 = coords.getJSONObject(1).getInt("y");

        this.ellipse = new Ellipse(c, x, y);
        this.ellipse.setBottomRight(x1, y1);
        this.shapes.add(this.ellipse);
        break;
        case "Polygon":
        this.polygon = new Polygon(c);
        shapes.add(this.polygon);
        for(int v = 0; v < coords.size(); v++){
          this.polygon.addVertex(coords.getJSONObject(v).getInt("x"), coords.getJSONObject(v).getInt("y"));
        };
        break;
      };
    }
  }
  public ArrayList<Shape> getShapesList(){
    return this.shapes;
  }
  public void clearShapes(){
    this.shapes = new ArrayList<Shape>();
  }

  public void draw(){
    for (int i = 0; i < shapes.size(); i++){
      shapes.get(i).display();
    }
  }

  //Adds a Shape to shapes List
  public void addShape(Shape shape){
    shapes.add(shape);
    updateContainer();
  }

  public void updateContainer(){
    int maxX = 0;
    int minX = 100000;
    int maxY = 0;
    int minY = 100000;

    for(int i = 0; i < shapes.size(); i++){
      Shape shape = shapes.get(i);

      maxX = max(maxX, shape.getMaxX());
      minX = min(minX, shape.getMinX());
      maxY = max(maxY, shape.getMaxY());
      minY = min(minY, shape.getMinY());

    }

    if(shapes.size() == 1 ){
      maxX = minX + shapes.get(0).getMaxX();
      maxY = minY + shapes.get(0).getMaxY();
    }
    contentTopLeft = new PVector(minX, minY);
    contentBottomRight = new PVector(maxX, maxY);
  }

  public void mousePressed() {

    if (this.activeShape != null) {
      this.activeShape.setSelected(false);
      this.activeShape = null;
    }

    for (int i = 0; i < this.shapes.size(); i++) {
      //Check if we clicked inside a screen
      // if (this.shapes.get(i).inside(mouseX, mouseY, get(mouseX,mouseY))) {
      //     this.activeShape = this.shapes.get(i);
      //     this.activeShape.setSelected(true);
      // };
      //Check if we clicked inside a vertex
      if (this.shapes.get(i).checkSelectedVertex(mouseX, mouseY)) {
        this.activeShape = this.shapes.get(i);
        this.activeShape.setSelected(true);
      };
    };

    //If clicked outside a screen or vertex check if we are creating a new screen
    switch(gui.shapeType) {
      case "Ellipse":
      //Create new ellipse
      gui.screenColor = color(random(255), random(255), random(255));
      this.ellipse = new Ellipse(gui.screenColor, mouseX, mouseY);
      this.shapes.add(this.ellipse);
      break;
      case "Polygon":
      if (polygon == null) {
        //Create new polygon
        gui.screenColor = color(random(255), random(255), random(255));
        this.polygon = new Polygon(gui.screenColor);
        this.shapes.add(this.polygon);
      }
      //Add vertex to current polygon
      this.polygon.addVertex(mouseX, mouseY);
      break;
    }
  }

  //Listen to double click to close polygon
  public void mouseClicked(MouseEvent evt) {
    if (evt.getCount() == 2 && gui.shapeType == "Polygon") {
      //closePolygon();
    };
  }

  public String getName(){
    return this.name;
  }

  public Screen clone(){
    Screen newScreen = new Screen(this.name);
    return newScreen;
  }

  public JSONArray getCoords(){
    JSONArray coords = new JSONArray();
    JSONObject shapeObj;
    Shape shape;
    for (int i = 0; i < shapes.size(); i++){
      shapeObj = new JSONObject();
      shape = shapes.get(i);
      shapeObj.setString("shapeType", shape.getClass().getSimpleName());
      shapeObj.setInt("color", shape.getColor());
      shapeObj.setJSONArray("coords", shape.getCoords());
      coords.append(shapeObj);
    }
    return coords;
  }

  public int getColor(){
    return 255;
  }

  public String getContentClass(){
    return this.contentClass;
  }

  public JSONObject getContentTopLeft(){
    JSONObject topLeft = new JSONObject();
    topLeft.put("x", PApplet.parseInt(contentTopLeft.x));
    topLeft.put("y", PApplet.parseInt(contentTopLeft.y));
    return topLeft;
  }

  public JSONObject getContentBottomRight(){
    JSONObject bottomRight = new JSONObject();
    bottomRight.setInt("x", PApplet.parseInt(contentBottomRight.x));
    bottomRight.setInt("y", PApplet.parseInt(contentBottomRight.y));
    return bottomRight;
  }

  public String getResizeMode(){
    return resizeMode;
  }

  public void saveMask(String filename){
    println("Saving mask image");
    ArrayList<PVector> verticesArray;
    PGraphics mask = createGraphics(ceil(abs(contentBottomRight.x - contentTopLeft.x)), ceil(abs(contentBottomRight.y - contentTopLeft.y))); //<>//
     //PGraphics mask = createGraphics(1024, 768);
    mask.beginDraw();
    mask.background(0);
    mask.fill(255);
    mask.noStroke();
    for (int i = 0; i < shapes.size(); i++){
      Shape shape = shapes.get(i);
      verticesArray = shape.getVerticesArray();
      switch (shape.getType()){
        case "Polygon":
        mask.beginShape();
        for (int j = 0; j < verticesArray.size(); j++){
          mask.vertex(round(verticesArray.get(j).x - contentTopLeft.x), round(verticesArray.get(j).y - contentTopLeft.y));
        }
        mask.vertex(verticesArray.get(0).x - contentTopLeft.x , verticesArray.get(0).y - contentTopLeft.y);
        mask.endShape(CLOSE);
        break;
        case "Ellipse":
        mask.ellipseMode(CORNERS);
        mask.ellipse(shape.getMinX() - contentTopLeft.x, shape.getMinY() - contentTopLeft.y, shape.getMaxX() - contentTopLeft.x, shape.getMaxY() - contentTopLeft.y);
        break;
      }
    }
    mask.beginDraw();
    mask.save(filename);
    println("End Saving mask image");
  }
}
interface Shape
{
  public PShape display();
  public PShape getVerticesGroup();
  public void move(int x,int y,int pmx,int pmy);
  public void moveStep(int x, int y);
  public int getColor();
  public String getType();
  public Boolean inside(int x, int y,int c);
  public void setSelected(Boolean selected);
  public Boolean checkSelectedVertex(int x, int y);
  public void moveSelectedVertex(int direction);
  public JSONArray getCoords();
  public ArrayList<PVector> getVerticesArray();
  public int getMaxX();
  public int getMinX();
  public int getMaxY();
  public int getMinY();
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Setup" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
