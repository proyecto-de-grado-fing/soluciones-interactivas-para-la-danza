class ContentImage extends ContentView
{
    PImage image;
    ContentImage(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);
        PImage image = loadImage("fing.jpg");
    }

    ContentImage(PApplet container) {
        super(container);
        this.image = loadImage("fing.jpg");
    }

    @Override
    PImage draw() {
        return image;
    }
    @Override
    void destroy(){
    }
}
