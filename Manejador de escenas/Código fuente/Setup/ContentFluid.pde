class ContentFluid extends ContentView{
    Fluid f;
    boolean hasDancers;
    public PGraphics pg;
    boolean reject = false;
    int backColFluid = color(0);
    float strokeColorR = 255;
    float strokeColorG = 0;
    float strokeColorB = 0;
    int strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
    float sWeightFluid = 1;
    PVector dPos;
    boolean lines = true;
    int influenceRadius = 100;
    boolean gravity = false;
    float psico = 0.02;

    int particleCount = 500;
    //
    BlobDetection blobDetection;
    Kinect kinect;
    int[] background;
    int opacity = 255;

    int status = 1;
    PImage ret;
    int finished = 0;

    ContentFluid(PApplet container, Kinect kinect){
        super(container);
        f = new Fluid();
        f.initialScene();
        this.kinect = kinect;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }

    ContentFluid(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background){
        super(container,canvasWidth,canvasHeight);
        f = new Fluid();
        f.initialScene();
        this.kinect = kinect;
        // this.kinect.initDepth();
        // this.kinect.enableMirror(false);
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }

    public void play(){
      this.loadInitialDepth();
    }

    public void stop(){}

    public void destroy(){}

    void setStatus(int status){
        this.status = status;
    }

    PImage draw(){
        if (background == null) {
            this.loadInitialDepth();
        }
        pg = createGraphics(640, 480);
        pg.beginDraw();
        // pg.rect(0,0);
        pg.background(0,0,0);
        this.mainContent();
        pg.endDraw();
        return pg.get();
    }

    void mainContent(){
        opacity = 255;
        strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
        f.drawScene(pg);
    }

    int isFinished(){
        return finished;
    }

    void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
    }

    class Fluid
    {
        // en total 1500 particulas
        Particle[] particles = new Particle[particleCount];

        public Fluid(){};

        void closeScene(){};
        void initialScene(){
            reject = true;
            for (int i = 0; i < particleCount; i++) {
                particles[i] = new Particle();
            }
        };
        void drawScene(PGraphics pg){
            //    blendMode(BLEND);
            // background(backColFluid);

            Blob blob = blobDetection.getBlob(background, kinect.getRawDepth());
            // buscamos al usuario
            if (blob != null){
                hasDancers = true;
                dPos = blob.getCenter();
                // blob.show(width, height);
                dPos.x = map(dPos.x,0,640,0,640);
                dPos.y = map(dPos.y,0,480,0,480);
                // definimos color del fondo y del trazo
                pg.fill(backColFluid);
                pg.stroke(255);
                //    stroke (255 - backColFluid);
                // definimos el grosor del trazo
                pg.strokeWeight(sWeightFluid);
                // actualizamos las posiciones de las particulas
                for (int i = 0; i < particleCount; i++) {
                    Particle particle = (Particle) particles[i];
                    particle.update(i);
                }
            }
            // return pg;
        };
        String getSceneName(){return "Fluid";};

        class Particle {
            // coordenada x,y
            float x;
            float y;
            // velocidad en eje x y y
            float vx;
            float vy;
            // se incia en una posicion randomin=ca en la pantalla
            Particle() {
                x = random(0, 640);
                y = random(0, 480);
            }
            // devuelve la posicion x
            float getx() {
                return x;
            }
            // devuelve la posicion y
            float gety() {
                return y;
            }

            // para definir si dibujamos la linea o no
            boolean drawthis;
            Particle particle;
            float tx;
            float ty;
            float radius;
            float angle;
            int px;
            int py;
            int i;
            void update(int num) {
                // simulamos la friccion
                vx *= 0.90;
                vy *= 0.90;
                // vx *= 0.87;
                // vy *= 0.87;
                // Loopeamos para checkear la influencia entre si de las particulas
                for (i = 0; i < particleCount; i++) {
                    // i = num no nos interesa porque seria calcular influencia de uno a uno mismo
                    if (i != num) {
                        drawthis = false;
                        // particula vecina
                        particle = (Particle) particles[i];
                        tx = particle.getx();
                        ty = particle.gety();
                        // la distancia entre la particula actual y la particula vecina
                        radius = dist(x,y,tx,ty);
                        // las lineas las dibujamos solo entre las particulas en radio menor de 35
                        if (radius < 35) {
                            drawthis = true;
                            // el angulo para poder dibujar la linea
                            angle = atan2(y-ty,x-tx);
                            // si esta muy cerca - se desvia
                            if (radius < 30) {
                                // cambiamos las velocidades
                                vx += (30 - radius) * 0.07 * cos(angle);
                                vy += (30 - radius) * 0.07 * sin(angle);
                            }
                            if (radius > 25) {
                                // si estan entre 25 y 30 se acercan
                                // vx -= (25 - radius) * 0.005 * cos(angle);
                                // vy -= (25 - radius) * 0.005 * sin(angle);
                                vx -= (25 - radius) * psico * cos(angle);
                                vy -= (25 - radius) * psico * sin(angle);
                            }
                        }
                        // si dibujamos lineas y deberiamos dibujar - dibujamos!
                        if (lines) {
                            if (drawthis) {
                                pg.stroke(strokeColor);
                                //               stroke(255-backColFluid,lineTransparency);
                                pg.line(x,y,tx,ty);
                            }
                        }
                    }
                }
                // aca consideramos la influencia del usuario
                if (hasDancers) {
                    // la posicion del usuario
                    tx = dPos.x;
                    ty = dPos.y;
                    radius = dist(x,y,tx,ty);
                    if (radius < influenceRadius) {
                        angle = atan2(ty-y,tx-x);
                        // atraemos o rebotamos?
                        if (reject) {
                            // rebotamos
                            vx -= radius * 0.07 * cos(angle);
                            vy -= radius * 0.07 * sin(angle);
                        }
                        else {
                            // atraemos
                            vx += radius * 0.07 * cos(angle);
                            vy += radius * 0.07 * sin(angle);
                        }
                    }
                }
                /* Previox x and y coordinates are set, for drawing the trail */
                px = (int)x;
                py = (int)y;
                // actualizamos las coordenadas de la particula
                x += vx;
                y += vy;
                // aplicamos la gravedad
                if (gravity == true) vy += 0.7;
                // rebotamos contra los bordes
                if (x > 640) {
                    if (abs(vx) == vx) vx *= -1.0;
                    x = 640;
                }
                if (x < 0) {
                    if (abs(vx) != vx) vx *= -1.0;
                    x = 11;
                }
                if (y < 0) {
                    if (abs(vy) != vy) vy *= -1.0;
                    y = 10;
                }
                if (y > 480) {
                    if (abs(vy) == vy) vy *= -1.0;
                    vx *= 0.6;
                    y = 480;
                }
                // si no dibujamos lineas, dibujamos particulas
                if (!lines) {
                    pg.stroke(color(255,0,0));
                    pg.fill(color(255,0,0));
                    pg.strokeWeight(20);
                    //stroke (255 - backColFluid,lineTransparency);
                    pg.line(px,py,int(x),int(y));
                    //ellipse(px,py,5,5);
                }
            }
        }
    }
}
