class BlobDetection{
    private int tolerance = 10;
    private ArrayList<Blob> blobs;
    private int offset;
    private BlobFinder finder;
    private int kinectWidth;
    private int kinectHeight;

    private boolean[] img;
    private Blob biggest = null;

    private OpenCV opencv;

    public BlobDetection(PApplet container, int kinectWidth, int kinectHeight) {
        this.kinectWidth = kinectWidth;
        this.kinectHeight = kinectHeight;
        this.finder = new BlobFinder(kinectWidth, kinectHeight);
        opencv = new OpenCV(container, kinectWidth, kinectHeight);
    }

    public Blob getBlob(int[] background, int[] foreground){
        img = new boolean[foreground.length];
        offset = 0;
        for (int x = 0; x < kinectWidth; x++) {
            for (int y = 0; y < kinectHeight; y++) {
                offset = x + y*kinectWidth; //<>//
                
                if ((foreground[offset] < 1500) && (abs(foreground[offset] - background[offset]) > tolerance)){
                    img[offset] = true;
                }
                else {
                    img[offset] = false;
                }
            }
        }
        if (blobs != null){
            blobs.clear();
        }

        //Using Blob Finder
        BlobFinder finder = new BlobFinder(kinectWidth, kinectHeight);
        blobs = finder.detectBlobs(img, 1000, -1);

        biggest = null;
        for (Blob b : blobs) {
            if (biggest == null || b.size() > biggest.size()){
                biggest = b;
            }
        }
        if (biggest != null){
            return biggest;
        }
        return null;
    }

    ArrayList<PVector> getContour(int[] background, int[] foreground, boolean reduce){
        int[] img = new int[foreground.length];
        offset = 0;
        for (int x = 0; x < kinectWidth; x++) {
            for (int y = 0; y < kinectHeight; y++) {
                offset = x + y * kinectWidth;

                if ((foreground[offset] < 1500) && (abs(foreground[offset] - background[offset]) > tolerance)){
                    img[offset] = 255;
                }
                else {
                    img[offset] = 0;
                }
            }
        }
        PImage image = createImage(kinectWidth, kinectHeight, RGB);
        image.loadPixels();
        for (int i = 0; i < image.pixels.length; i++) {
            image.pixels[i] = img[i];
        }
        image.updatePixels();
        opencv.loadImage(image);
        ArrayList<Contour> contours = opencv.findContours();
        Contour biggest = null;
        for (Contour c : contours){
            if (biggest == null || c.area() > biggest.area()){
                biggest = c;
            }
        }
        if (biggest != null){
            ArrayList<PVector> points = biggest.getPoints();
            if (reduce){
                points = reduceSize(points, points.size()/2);
            }
            return points;
        } else {
            return null;
        }
    }

    ArrayList<PVector> reduceSize(ArrayList<PVector> contour, int newSize){
        int index;
        int size = contour.size();
        while (size > newSize){
            index = int(random(0, size - 1));
            contour.remove(index);
            size--;
        }
        return contour;
    }
}
