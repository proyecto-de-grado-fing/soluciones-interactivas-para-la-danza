class Screen
{
  PVector contentTopLeft;
  PVector contentBottomRight;
  ArrayList<Shape> shapes;
  Shape activeShape;
  Ellipse ellipse;
  Polygon polygon;
  String resizeMode = "Cover"; // Contain //Noresize
  String mask;
  String contentClass = "ContentImage";
  String name;

  Screen(String name){
    this.name = name;
    this.contentTopLeft = new PVector(0,0);
    this.contentBottomRight = new PVector(gui.canvas.canvasHeight,gui.canvas.canvasWidth);

    this.shapes = new ArrayList<Shape>();
    this.activeShape = null;
    this.ellipse = null;
    this.polygon = null;
  }
  public void setContentTopLeft(int x, int y){
    this.contentTopLeft = new PVector(x,y);
  };
  public void setContentBottomRight(int x, int y){
    this.contentBottomRight = new PVector(x,y);
  };
  public void setMask(String _mask){
    this.mask = _mask;
  };
  public void setContentClass(String cc){
    this.contentClass = cc;
  };
  public void setResizeMode(String rm){
    this.resizeMode = rm;
  };
  public void setShapes(JSONArray _shapes){
    for (int i = 0; i < _shapes.size(); i++) {
      JSONObject _s = (JSONObject) _shapes.getJSONObject(i);
      JSONArray coords = _s.getJSONArray("coords");
      int c = _s.getInt("color");

      switch(_s.getString("shapeType")){
        case "Ellipse":
        int x,y,x1,y1;
        x = coords.getJSONObject(0).getInt("x");
        y = coords.getJSONObject(0).getInt("y");
        x1 = coords.getJSONObject(1).getInt("x");
        y1 = coords.getJSONObject(1).getInt("y");

        this.ellipse = new Ellipse(c, x, y);
        this.ellipse.setBottomRight(x1, y1);
        this.shapes.add(this.ellipse);
        break;
        case "Polygon":
        this.polygon = new Polygon(c);
        shapes.add(this.polygon);
        for(int v = 0; v < coords.size(); v++){
          this.polygon.addVertex(coords.getJSONObject(v).getInt("x"), coords.getJSONObject(v).getInt("y"));
        };
        break;
      };
    }
  }
  public ArrayList<Shape> getShapesList(){
    return this.shapes;
  }
  public void clearShapes(){
    this.shapes = new ArrayList<Shape>();
  }

  void draw(){
    for (int i = 0; i < shapes.size(); i++){
      shapes.get(i).display();
    }
  }

  //Adds a Shape to shapes List
  public void addShape(Shape shape){
    shapes.add(shape);
    updateContainer();
  }

  void updateContainer(){
    int maxX = 0;
    int minX = 100000;
    int maxY = 0;
    int minY = 100000;

    for(int i = 0; i < shapes.size(); i++){
      Shape shape = shapes.get(i);

      maxX = max(maxX, shape.getMaxX());
      minX = min(minX, shape.getMinX());
      maxY = max(maxY, shape.getMaxY());
      minY = min(minY, shape.getMinY());
    }

    //if(shapes.size() == 1 ){
    //  maxX = minX + shapes.get(0).getMaxX();
    //  maxY = minY + shapes.get(0).getMaxY();
    //}
    contentTopLeft = new PVector(minX, minY);
    contentBottomRight = new PVector(maxX, maxY);
  }

  void mousePressed() {
    if (this.activeShape != null) {
      this.activeShape.setSelected(false);
      this.activeShape = null;
    }

    for (int i = 0; i < this.shapes.size(); i++) {
      //Check if we clicked inside a vertex
      if (this.shapes.get(i).checkSelectedVertex(mouseX, mouseY)) {
        this.activeShape = this.shapes.get(i);
        this.activeShape.setSelected(true);
      };
    };

    //If clicked outside a screen or vertex check if we are creating a new screen
    switch(gui.shapeType) {
      case "Ellipse":
      //Create new ellipse
      gui.screenColor = color(random(255), random(255), random(255));
      this.ellipse = new Ellipse(gui.screenColor, mouseX, mouseY);
      this.shapes.add(this.ellipse);
      break;
      case "Polygon":
      if (polygon == null) {
        //Create new polygon
        gui.screenColor = color(random(255), random(255), random(255));
        this.polygon = new Polygon(gui.screenColor);
        this.shapes.add(this.polygon);
      }
      //Add vertex to current polygon
      this.polygon.addVertex(mouseX, mouseY);
      break;
    }
  }

  //Listen to double click to close polygon
  void mouseClicked(MouseEvent evt) {
    if (evt.getCount() == 2 && gui.shapeType == "Polygon") {
      //closePolygon();
    };
  }

  String getName(){
    return this.name;
  }

  Screen clone(){
    Screen newScreen = new Screen(this.name);
    return newScreen;
  }

  JSONArray getCoords(){
    JSONArray coords = new JSONArray();
    JSONObject shapeObj;
    Shape shape;
    for (int i = 0; i < shapes.size(); i++){
      shapeObj = new JSONObject();
      shape = shapes.get(i);
      shapeObj.setString("shapeType", shape.getClass().getSimpleName());
      shapeObj.setInt("color", shape.getColor());
      shapeObj.setJSONArray("coords", shape.getCoords());
      coords.append(shapeObj);
    }
    return coords;
  }

  int getColor(){
    return 255;
  }

  String getContentClass(){
    println(this.contentClass);
    return this.contentClass;
  }

  JSONObject getContentTopLeft(){
    JSONObject topLeft = new JSONObject();
    topLeft.put("x", int(contentTopLeft.x));
    topLeft.put("y", int(contentTopLeft.y));
    return topLeft;
  }

  JSONObject getContentBottomRight(){
    JSONObject bottomRight = new JSONObject();
    bottomRight.setInt("x", int(contentBottomRight.x));
    bottomRight.setInt("y", int(contentBottomRight.y));
    return bottomRight;
  }

  String getResizeMode(){
    return resizeMode;
  }

  void saveMask(String filename){
    println("Saving mask image");
    ArrayList<PVector> verticesArray;
    PGraphics mask = createGraphics(ceil(abs(contentBottomRight.x - contentTopLeft.x)), ceil(abs(contentBottomRight.y - contentTopLeft.y))); //<>// //<>// //<>//
     //PGraphics mask = createGraphics(1024, 768);
    mask.beginDraw();
    mask.background(0);
    mask.fill(255);
    mask.noStroke();
    for (int i = 0; i < shapes.size(); i++){
      Shape shape = shapes.get(i);
      verticesArray = shape.getVerticesArray();
      switch (shape.getType()){
        case "Polygon":
        mask.beginShape();
        for (int j = 0; j < verticesArray.size(); j++){
          mask.vertex(round(verticesArray.get(j).x - contentTopLeft.x), round(verticesArray.get(j).y - contentTopLeft.y));
        }
        mask.vertex(verticesArray.get(0).x - contentTopLeft.x , verticesArray.get(0).y - contentTopLeft.y);
        mask.endShape(CLOSE);
        break;
        case "Ellipse":
        mask.ellipseMode(CORNERS);
        mask.ellipse(shape.getMinX() - contentTopLeft.x, shape.getMinY() - contentTopLeft.y, shape.getMaxX() - contentTopLeft.x, shape.getMaxY() - contentTopLeft.y);
        break;
      }
    }
    mask.beginDraw();
    mask.save(filename);
    println("End Saving mask image");
  }
}
