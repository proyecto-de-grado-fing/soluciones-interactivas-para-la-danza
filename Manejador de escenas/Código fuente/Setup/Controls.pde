import java.util.*;

public class Controls extends PApplet {

  PApplet parent;
  int canvasWidth,canvasHeight;
  ControlP5 cp5;
  String name;
  Textfield jsonName,newSceneName,newScreenName;
  String filename;
  Textlabel filenameMsg;
  ScrollableList scenesDropdown,screensDropdown,contentDropdown;
  Group screensGroup,toolsGroup,sceneActionsGroup;
  RadioButton toolSwitch,resizeSwitch;
  Scene activeScene;
  Screen activeScreen;
  Button previewBtn,previewStopBtn;

  ArrayList<Scene> scenesList = new ArrayList<Scene>();
  List scenesListDropdown = new ArrayList();
  ArrayList<Screen> screensList = new ArrayList<Screen>();
  List screensListDropdown = new ArrayList();
  ArrayList<String> contentListDropdown;


  public Controls(PApplet _parent, int _w, int _h, String _name, ArrayList<String> contentViews) {
    super();
    this.parent = _parent;
    this.canvasWidth = _w;
    this.canvasHeight = _h;
    this.name = _name;
    this.contentListDropdown = contentViews;
    PApplet.runSketch(new String[]{"--display=1",this.getClass().getName()}, this);
  }
  public void settings() {
    size(1024, 768);
  }

  public void setup() {

    surface.setTitle(this.name);
    surface.setLocation(0, 0);
    cp5 = new ControlP5(this);
    cp5.setFont(createFont("arial",14));

    cp5.addTextlabel("titleScenes")
    .setText("ESCENAS")
    .setPosition(100,100);

    scenesDropdown = cp5.addScrollableList("scenesDropdown")
    // .plugTo(this,"scenesDropdown")
    .setLabel("Escenas")
    .setPosition(100, 120)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(scenesListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN);

    newSceneName = cp5.addTextfield("newSceneName")
    // .plugTo(this,"newSceneName")
    .setLabel("Nueva escena")
    .setPosition(330, 120)
    .setSize(150,50);

    Label labelNewSceneName = newSceneName.getCaptionLabel();
    labelNewSceneName.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

    cp5.addButton("addScene")
    // .plugTo(this,"addScene")
    .setLabel("Agregar escena")
    .setPosition(500, 120)
    .setSize(150, 50);

    sceneActionsGroup = cp5.addGroup("sceneActionsGroup");
    sceneActionsGroup.setLabel("");
    sceneActionsGroup.setVisible(false);

    cp5.addButton("duplicateScene")
    // .plugTo(this,"addScene")
    .setLabel("Duplicar escena")
    .setPosition(660, 120)
    .moveTo(sceneActionsGroup)
    .setSize(150, 50);

    cp5.addButton("deleteScene")
    // .plugTo(this,"deleteScene")
    .setLabel("Quitar escena")
    .setPosition(870, 120)
    .setColorLabel(color(255,0,0))
    .setSize(150, 50)
    .moveTo(sceneActionsGroup);

    screensGroup = cp5.addGroup("screensGroup");
    screensGroup.setVisible(false);
    screensGroup.setLabel("");

    cp5.addTextlabel("titleScreens")
    .setText("ESPACIOS DE PROYECCIÓN")
    .setPosition(100,260)
    .moveTo(screensGroup);

    screensDropdown = cp5.addScrollableList("screensDropdown")
    // .plugTo(this,"screensDropdown")
    .setLabel("Espacios")
    .setPosition(100, 280)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(screensListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN)
    .moveTo(screensGroup);


    newScreenName = cp5.addTextfield("newScreenName")
    // .plugTo(this,"newScreenName")
    .setLabel("Nuevo espacio")
    .setPosition(330, 280)
    .setSize(150,50)
    .moveTo(screensGroup);

    Label labelNewScreenName = newScreenName.getCaptionLabel();
    labelNewScreenName.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

    cp5.addButton("addScreen")
    // .plugTo(this,"addScreen")
    .setLabel("Agregar espacio")
    .setPosition(500, 280)
    .setSize(150, 50)
    .moveTo(screensGroup);

    cp5.addButton("clearScreen")
    // .plugTo(this,"clear")
    .setLabel("Limpiar")
    .setPosition(750, 280)
    .setSize(100, 50)
    .moveTo(screensGroup);

    cp5.addButton("deleteScreen")
    // .plugTo(this,"deleteScreen")
    .setLabel("Quitar espacio")
    .setPosition(870, 280)
    .setSize(150, 50)
    .setColorLabel(color(255,0,0))
    .moveTo(screensGroup);

    toolsGroup = cp5.addGroup("toolsGroup");
    toolsGroup.setVisible(false);
    toolsGroup.setLabel("");

    cp5.addTextlabel("titleTools")
    .setText("HERRAMIENTAS")
    .setPosition(100,450)
    .moveTo(toolsGroup);

    toolSwitch = cp5.addRadioButton("toolSwitch")
    // .plugTo(this,"toolSwitch")
    .setPosition(100,490)
    .setSize(40,20)
    .setColorForeground(color(120))
    .setColorActive(color(255))
    .setColorLabel(color(255))
    .setItemsPerRow(1)
    .setSpacingColumn(100)
    .addItem("Elipse",1)
    .addItem("Polígono",2)
    .addItem("Mouse",3)
    .moveTo(toolsGroup);

    cp5.addTextlabel("titleContents")
    .setText("CONTENIDOS")
    .setPosition(320,450)
    .moveTo(toolsGroup);

    contentDropdown = cp5.addScrollableList("contentDropdown")
    .setLabel("Contenido")
    .setPosition(320, 490)
    .setSize(200, 150)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(contentListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN)
    .moveTo(toolsGroup);

    cp5.addTextlabel("titleResize")
    .setText("REDIMENSIONADO")
    .setPosition(560,450)
    .moveTo(toolsGroup);

    resizeSwitch = cp5.addRadioButton("resizeSwitch")
    // .plugTo(this,"toolSwitch")
    .setPosition(560,490)
    .setSize(40,20)
    .setColorForeground(color(120))
    .setColorActive(color(255))
    .setColorLabel(color(255))
    .setItemsPerRow(1)
    .setSpacingColumn(100)
    .addItem("Cubrir",1)
    .addItem("Contener",2)
    .addItem("No redimensionar",3)
    .moveTo(toolsGroup);

    previewBtn = cp5.addButton("preview")
    .setLabel("Previsualizar")
    .setPosition(850,490)
    .setSize(150, 50)
    .setColorLabel(color(255,0,0))
    .moveTo(toolsGroup);

    previewStopBtn = cp5.addButton("previewStop")
    .setLabel("Detener")
    .setPosition(850,490)
    .setSize(150, 50)
    .setColorLabel(color(255,0,0))
    .setVisible(false)
    .moveTo(toolsGroup);

    jsonName = cp5.addTextfield("jsonName")
    // .plugTo(this,"jsonName")
    .setLabel("Nombre del archivo")
    .setPosition(100,690)
    .setSize(200,50);

    Label labelJsonName = jsonName.getCaptionLabel();
    labelJsonName.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

    cp5.addButton("saveConfig")
    // .plugTo(this,"saveConfig")
    .setLabel("Guardar configuracion")
    .setPosition(320, 690)
    .setSize(200, 50);

    filenameMsg = cp5.addTextlabel("filenameMsg")
    // .plugTo(this,"filenameEmpty")
    .setText("Ingresa un nombre para el archivo")
    .setColorValue(0xFFFF0000)
    .setFont(createFont("arial",20))
    .setPosition(540,700)
    .setSize(200,50)
    .setVisible(false);
  }


  void controlEvent(ControlEvent theEvent) {
    //Get every event name
    String value = nf(theEvent.getValue());
    String sceneName;
    switch(theEvent.getName()){
      case "toolSwitch":
      switch (value){
        case "1":
        gui.shapeType = "Ellipse";
        break;
        case "2":
        gui.shapeType = "Polygon";
        break;
        case "3":
        gui.shapeType = "Mouse";
        break;
        case "-1":
        gui.shapeType = "Mouse";
        toolSwitch.activate(2);
        break;
      }
      break;
      case "loadConfig":
      // this.parent.loadConfig();
      // loadScenes();
      break;
      case "saveConfig":
      filename = trim(jsonName.getText());
      //In case no filename specified
      if(filename.equals("")){
        filenameMsg.setVisible(true);
        filenameMsg.setColorLabel(color(255,0,0))
        .setColorValue(0xFFFF0000)
        .setText("Ingresa un nombre para el archivo.");
        jsonName.setColorLabel(color(255,0,0));
      }
      else
      {
        filenameMsg.setVisible(false)
        .setColorLabel(color(255,255,255));
        jsonName.setColorLabel(color(255,255,255));
        Boolean result = this.saveConfig(filename);
        if(result){
          println("success saving file");
          jsonName.setColorLabel(color(0,255,0));
          filenameMsg.setVisible(true)
          .setColorValue(0xFF00FF00)
          .setText("Archivo guardado correctamente.");
        }
        else
        {
          println("error saving file");
          jsonName.setColorLabel(color(255,0,0));
          filenameMsg.setVisible(true)
          .setColorLabel(color(0,255,0))
          .setText("Error al guardar el archivo.");
        }
      }
      break;
      case "addScene":
      sceneName = trim(newSceneName.getText());
      if(sceneName.equals("")){
        newSceneName.setColorLabel(color(255,0,0));
      }
      else{
        newSceneName.setColorLabel(color(255,255,255));
        addScene(sceneName);
      }

      break;
      case "deleteScene":
      this.deleteScene();
      break;
      case "duplicateScene":
      sceneName = trim(newSceneName.getText());
      if(sceneName.equals("")){
        newSceneName.setColorLabel(color(255,0,0));
      }
      else{
        newSceneName.setColorLabel(color(255,255,255));
        duplicateScene(sceneName);
      }
      case "addScreen":
      String screenName = trim(newScreenName.getText());
      if(screenName.equals("")){
        newScreenName.setColorLabel(color(255,0,0));
      }
      else{
        newScreenName.setColorLabel(color(255,255,255));
        addScreen(screenName,scenesDropdown.getStringValue());
      }
      break;
      case "clearScreen":
      activeScreen.clearShapes();
      break;
      case "deleteScreen":
      this.deleteScreen();
      break;
      case "scenesDropdown":
      screensGroup.setVisible(true);
      sceneActionsGroup.setVisible(true);
      activeScene = scenesList.get(int(theEvent.getValue()));
      if (activeScene.getScreens().size() > 0) {
        this.activeScreen = activeScene.getScreens().get(activeScene.getScreens().size() - 1);
      } else {
        this.activeScreen = null;
      }
      this.updateScreensDropdown();
      previewContent(false);
      break;
      case "screensDropdown":
        this.activateScreen(int(theEvent.getValue()));
        //End screensDropdown
      break;
      case "contentDropdown":
        activeScreen.setContentClass(contentViews.get(int(value)));
      // switch (value){
      //   case "0":
      //   activeScreen.setContentClass("ContentVideo");
      //   break;
      //   case "1":
      //   activeScreen.setContentClass("ContentBalls");
      //   break;
      //   case "2":
      //   activeScreen.setContentClass("ContentVideo1");
      //   break;
      //   case "3":
      //   activeScreen.setContentClass("ContentVideo2");
      //   break;
      //   case "4":
      //   activeScreen.setContentClass("ContentVideo3");
      //   break;
      // }
      break;
      case "resizeSwitch":
        switch (value){
          case "1":
          activeScreen.setResizeMode("Cover");
          break;
          case "2":
          activeScreen.setResizeMode("Contain");
          break;
          case "3":
          activeScreen.setResizeMode("Noresize");
          break;
          case "-1":
          activeScreen.setResizeMode("Noresize");
          break;
        }
      break;
      case "preview":
      previewContent(true);
      break;
      case "previewStop":
      previewContent(false);
      break;
    }
  }

  public void addScene (Scene s){
    scenesList.add(s);
    scenesListDropdown.add(s.name);
    updateScenesDropdown();

    int pos = scenesListDropdown.indexOf(s.name);

    scenesDropdown.setValue(pos);
    screensGroup.setVisible(true);
    sceneActionsGroup.setVisible(true);
    activeScene = s;
    if (activeScene.getScreens().size() > 0) {
      this.activeScreen = activeScene.getScreens().get(activeScene.getScreens().size() - 1);
    } else {
      this.activeScreen = null;
    }
    previewContent(false);

    this.updateScreensDropdown();
    //Clear textfield input
    newSceneName.clear();
  }

  public void addScene (String sceneName){
    Scene s = new Scene(sceneName);
    scenesList.add(s);
    scenesListDropdown.add(sceneName);
    updateScenesDropdown();

    int pos = scenesListDropdown.indexOf(sceneName);

    scenesDropdown.setValue(pos);
    screensGroup.setVisible(true);
    sceneActionsGroup.setVisible(true);
    activeScene = s;
    if (activeScene.getScreens().size() > 0) {
      this.activeScreen = activeScene.getScreens().get(activeScene.getScreens().size() - 1);
    } else {
      this.activeScreen = null;
    }
    this.updateScreensDropdown();
    previewContent(false);

    //Clear textfield input
    newSceneName.clear();
  }

  public void duplicateScene (String sceneName){
    Scene s = new Scene(sceneName);
    //Clone Screens
    ArrayList<Screen> screensList = activeScene.getScreens();
    ArrayList<Screen> clone = new ArrayList<Screen>(screensList.size());
    for(int i = 0; i< screensList.size();i++){
      clone.add(screensList.get(i).clone());
    }
    s.setScreens(clone);

    scenesList.add(s);
    scenesListDropdown.add(sceneName);
    updateScenesDropdown();

    //Clear textfield input
    newSceneName.clear();
  }

  void updateScenesDropdown(){
    cp5.remove("scenesDropdown");
    scenesDropdown = cp5.addScrollableList("scenesDropdown")
    .plugTo(this.parent,"scenesDropdown")
    .setLabel("Escenas")
    .setPosition(100, 120)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(scenesListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN);
  }

  public void addScreen (String screenName, String sceneName){
    Screen s = new Screen(screenName);
    Scene scene = null;
    for (int i = 0; i < scenesList.size(); i++){
      if (sceneName.equals(scenesList.get(i).getName())){
        scene = scenesList.get(i);
      }
    }
    if (scene != null){
      scene.addScreen(s);
    }
    activeScene.addScreen(s);

    int index = activeScene.getScreens().indexOf(s);
    this.activateScreen(index);

    //Clear textfield input
    newScreenName.clear();
    this.updateScreensDropdown();
  }

  public void activateScreen(int index) {

    toolsGroup.setVisible(true);
      toolSwitch.activate(2);
      activeScreen = screensList.get(index);
      gui.canvas.active = null;
      gui.canvas.closePolygon();
      //activeScreen.setContentClass(contentListDropdown.get(0));
      previewContent(false);

      //Set resize switch
      switch (activeScreen.getResizeMode()){
        case "Cover":
        resizeSwitch.activate(0);
        break;
        case "Contain":
        resizeSwitch.activate(1);
        break;
        case "Noresize":
        resizeSwitch.activate(2);
        break;
        default:
        resizeSwitch.activate(-1);
        break;
      }
      //Set content dropdown
      contentDropdown.setValue(contentViews.indexOf(activeScreen.getContentClass()));
      // switch (activeScreen.getContentClass()){
      //   case "ContentVideo":
      //   contentDropdown.setValue(0);
      //   break;
      //   case "ContentBalls":
      //   contentDropdown.setValue(1);
      //   break;
      //   case "ContentVideo1":
      //   contentDropdown.setValue(2);
      //   break;
      //   case "ContentVideo2":
      //   contentDropdown.setValue(3);
      //   break;
      //   case "ContentVideo3":
      //   contentDropdown.setValue(4);
      //   break;
      //
      // }
  }

  void updateScreensDropdown(){
    screensListDropdown = new ArrayList();

    if(this.activeScene != null){
      this.screensList = activeScene.getScreens();
      
      for(int i = 0; i < this.screensList.size() ;i++){
        screensListDropdown.add(this.screensList.get(i).getName());
      }
    }
    screensDropdown.setItems(screensListDropdown);
    if (this.activeScreen != null) {
      int pos = screensListDropdown.indexOf(this.activeScreen.getName());
      screensDropdown.setValue(pos);
    }else{
     screensDropdown = cp5.addScrollableList("screensDropdown")
    // .plugTo(this,"screensDropdown")
    .setLabel("Espacios")
    .setPosition(100, 280)
    .setSize(200, 120)
    .setBarHeight(50)
    .setItemHeight(30)
    .addItems(screensListDropdown)
    .close()
    .setType(ScrollableList.DROPDOWN)
    .moveTo(screensGroup);
    }
  }

  void deleteScene(){
    for(int i = 0 ; i<this.scenesList.size();i++){
      if (this.scenesList.get(i).getName() == this.activeScene.getName()){
        scenesList.remove(i);
        scenesListDropdown.remove(i);
      }
    }
    this.updateScenesDropdown();
    screensGroup.setVisible(false);
    toolsGroup.setVisible(false);
    sceneActionsGroup.setVisible(false);
  }

  void deleteScreen(){
    for(int i = 0 ; i<this.screensList.size();i++){
      if (this.screensList.get(i).getName() == this.activeScreen.getName()){
        this.screensList.remove(i);
        this.screensListDropdown.remove(i);
      }
    }
    this.activeScreen = null;
    this.updateScreensDropdown();
    toolsGroup.setVisible(false);
    activeScreen = null;

  }
  void loadScenes() {
    // parent.loadScenes();
    // selectInput("Select a file to process:", "fileSelected");
  }

  Boolean saveConfig(String filename){
    println("saving JSON config file");

    //Main Config
    JSONObject conf = new JSONObject();
    conf.setInt("canvasWidth",this.canvasWidth);
    conf.setInt("canvasHeight",this.canvasHeight);

    //Scenes
    println("saving scenesList");
    JSONArray scenes = new JSONArray();
    Scene scene;
    for (int i = 0; i < scenesList.size(); i++){
      scene = scenesList.get(i);
      JSONObject sceneObj = new JSONObject();
      sceneObj.setString("name", scene.getName());

      //Screens
      println("saving Screens");
      JSONArray screens = new JSONArray();
      Screen screen;
      String maskName;
      for (int j = 0; j < scene.getScreens().size(); j++) {
        screen = scene.getScreens().get(j);
        JSONObject screenObj = new JSONObject();
        screenObj.setString("name", screen.getName());
        screenObj.setInt("color", screen.getColor());
        screenObj.setString("contentClass", screen.getContentClass());
        screenObj.setJSONObject("contentTopLeft", screen.getContentTopLeft());
        screenObj.setJSONObject("contentBottomRight", screen.getContentBottomRight());
        screenObj.setString("resizeMode", screen.getResizeMode());
        screenObj.setJSONArray("shapes", screen.getCoords());

        //Screen mask
        maskName = "data/" + filename +"/maskE" + i + "S" + j + ".tif";
        screen.saveMask(maskName);
        screenObj.setString("mask", maskName);
        screens.append(screenObj);
      }
      sceneObj.setJSONArray("screens", screens);
      scenes.append(sceneObj);
      println("scenes saved");
    }

    conf.setJSONArray("scenes", scenes);

    return saveConfigSetup(conf, filename);
  }
  void keyPressed() {
    //Check if changing screen
    switch (key) {
      case ESC:
      key = 0;
      break;
    }
  }
  Screen getActiveScreen(){
    return activeScreen;
  }
  Scene getActiveScene(){
    return activeScene;
  }
  void draw() {
    background(0);
    pushStyle();
    stroke(255, 255, 255);
    line(0, 85, 1024,85);
    line(0, 250, 1024,250);
    line(0, 435, 1024,435);
    line(0, 655, 1024,655);
    popStyle();
  }
  void previewContent(Boolean value){
    if(value == true){
      previewBtn.hide();
      previewStopBtn.show();
      gui.setPreviewMode(true,this.activeScreen);
    }else{
      previewBtn.show();
      previewStopBtn.hide();
      gui.setPreviewMode(false,null);
    }
  }
}
