// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/1scFcY-xMrI

class Blob {
    PVector min;
    PVector max;
    float depth;
    PShape blobShape;
    int mass;
    int bColor = color(255, 255, 255);

    ArrayList<PVector> points;
    ArrayList<PVector> contour;

    Blob(float x, float y, float depth) {
        this.min = new PVector(x,y, depth);
        this.max = new PVector(x,y, depth);
        this.depth = depth;

        points = new ArrayList<PVector>();
        points.add(new PVector(x, y, depth));
    }

    Blob(PVector min, PVector max, int mass, ArrayList<PVector> points, ArrayList<PVector> contour) {
        this.min = min;
        this.max = max;
        this.mass = mass;

        this.points = points;
        this.contour = contour;
    }
    void show(int imgWidth, int imgHeight) {
        stroke(bColor);
        fill(bColor);
        strokeWeight(2);

        for (PVector v : contour) {
            v.x = map(v.x,0,640,0,imgWidth);
            v.y = map(v.y,0,480,0,imgHeight);
            point(v.x, v.y);
        }        
    }


    void add(float x, float y, float depth) {
        points.add(new PVector(x, y, depth));
        min.x = min(min.x, x);
        min.y = min(min.y, y);
        max.x = max(max.x, x);
        max.y = max(max.y, y);
        this.depth = depth;
    }

    float size() {
        // return (max.x-min.x)*(max.y-min.y);
        return points.size();
    }

    float height() {
        return max.y-min.y;
    }

    float width() {
        return max.x-min.x;
    }

    PVector getTopLeft() {
        return min;
    }
    PVector getBottomRight() {
        return max;
    }

    PVector getTop(){
        return new PVector (((max.x - min.x) / 2) + min.x, min.y);
        // return new PVector(((max.x - min.x) / 2) + min.x, ((max.y - min.y) / 2) + min.y);
    }
    PVector getCenter(){
        // return new PVector (((max.x - min.x) / 2) + min.x, min.y);
        return new PVector(((max.x - min.x) / 2) + min.x, ((max.y - min.y) / 2) + min.y);
    }

    ArrayList<PVector> getPoints(){
        return points;
    }

    ArrayList<PVector> getContour(boolean order){
        if (order){
            int contourSize = contour.size();
            int i;
            int j;
            PVector currentPt;
            float minDist;
            float curDist;
            int minIndex;

            for (i = 0; i < contourSize - 1; i++){
                currentPt = contour.get(i);
                minDist = Integer.MAX_VALUE;
                minIndex = i;
                // Find nearest point
                for (j = i + 1; j < contourSize; j++){
                    curDist = this.distSq(currentPt, contour.get(j));
                    if (curDist < minDist){
                        minDist = curDist;
                        minIndex = j;
                    }
                }
                swap(contour, i + 1, minIndex);
            }
        }
        return contour;
    }

    void swap(ArrayList<PVector> list, int index1, int index2) {
        PVector temp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, temp);
    }

    float distSq(PVector p1, PVector p2) {
        return this.distSq(p1.x, p1.y, p2.x, p2.y);
    }

    float distSq(float x1, float y1, float x2, float y2) {
        float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1);
        return d;
    }
    float distSq(float x1, float y1, float z1, float x2, float y2, float z2) {
        float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1);
        return d;
    }
}
