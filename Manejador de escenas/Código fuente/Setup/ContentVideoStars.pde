import processing.video.*;

class ContentVideoStars extends ContentView
{
    Movie movie;
    Movie myMovie;

    ContentVideoStars(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);
        myMovie = new Movie(container, "stars.mp4");
        myMovie.loop();
    }

    ContentVideoStars(PApplet container) {
        super(container);
        myMovie = new Movie(container, "stars.mp4");
        myMovie.loop();
    }

    @Override
    PImage draw() {
        if (myMovie.available()) {
            myMovie.read();
        }
        // image (myMovie, 500, 0);
        return myMovie.get();
    }
    @Override
    void destroy(){
      println("Destroy movie");
      this.myMovie.stop();
      this.myMovie.dispose();
    }
}
