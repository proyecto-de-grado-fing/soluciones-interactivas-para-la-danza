class Gui {
  public Controls controls;
  public Canvas canvas;
  public String shapeType = "Mouse";
  public int screenColor;
  public Boolean previewMode = false;
  public PApplet context;
  public ContentView content;

  Gui(PApplet context, int width, int height) {
    this.controls = new Controls(context, 1024, 768, "Controles");
    this.canvas = new Canvas(context, width, height, "Lienzo");
    this.context = context;
  }

  Screen getActiveScreen(){
    return this.controls.getActiveScreen();
  }
  Scene getActiveScene(){
    return this.controls.getActiveScene();
  }

  Boolean getPreviewMode(){
    return this.previewMode;
  }
  void setPreviewMode(Boolean value, Screen screen){
    if(value == true){
      canvas.preview = new Preview(screen, this.context);
      canvas.preview.saveMask();
    }else{
      if (canvas.preview != null) {
        canvas.preview.destroy();
        canvas.preview = null;
      }
    }
    this.previewMode = value;
  }
  void setShapeType(String type) {
    this.shapeType = type;
  }

}
