import processing.video.*;

class ContentVideo3 extends ContentView
{
    Movie movie;
    Movie myMovie;

    ContentVideo3(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);

        myMovie = new Movie(container, "4.mp4");
        myMovie.loop();
    }

    ContentVideo3(PApplet container) {
        super(container);
        myMovie = new Movie(container, "4.mp4");
        myMovie.loop();
    }

    @Override
    PImage draw() {
        if (myMovie.available()) {
            myMovie.read();
        }
        // image (myMovie, 500, 0);
        return myMovie.get();
    }
}
