interface Shape
{
  PShape display();
  PShape getVerticesGroup();
  void move(int x,int y,int pmx,int pmy);
  void moveStep(int x, int y);
  int getColor();
  String getType();
  Boolean inside(int x, int y,int c);
  void setSelected(Boolean selected);
  Boolean checkSelectedVertex(int x, int y);
  void moveSelectedVertex(int direction);
  JSONArray getCoords();
  ArrayList<PVector> getVerticesArray();
  int getMaxX();
  int getMinX();
  int getMaxY();
  int getMinY();
}
