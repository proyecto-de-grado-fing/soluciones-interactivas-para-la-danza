class Scene {
  String name;
  ArrayList<Screen> screens;


  Scene(String name){
    this.name = name;
    this.screens = new ArrayList<Screen>();
  }

  void draw(){
    for (int i = 0; i < this.screens.size(); i++){
      this.screens.get(i).draw();
    }
  }

  public void addScreen(Screen screen){
    this.screens.add(screen);
  }
  public void loadScreens(ArrayList<Screen> scr){
    this.screens = scr;
  }
  public void setScreens(ArrayList<Screen> screens){
     this.screens = screens;
  }
  public ArrayList<Screen> getScreens(){
    return this.screens;
  }
  public String getName(){
    return this.name;
  }
}
