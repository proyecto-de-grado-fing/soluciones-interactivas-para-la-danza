class Preview
{
  PVector contentTopLeft;
  PVector contentBottomRight;
  String resizeMode = "Cover"; //Cover // Contain //Noresize
  String mask,contentClass;
  PImage maskImage;
  PImage contentImage;
  PGraphics imageInCanvas;
  Screen screen;
  float positionX = 0;
  float positionY = 0;
  float scale = -1;
  int finalImageWidth;
  int finalImageHeight;
  boolean adjustedByWidth = false;
  ContentView content;
  PApplet parent;

  Preview(Screen screen, PApplet parent){
    this.screen = screen;
    //Set bounds
    JSONObject topLeft = screen.getContentTopLeft();
    JSONObject bottomRight = screen.getContentBottomRight();
    contentTopLeft = new PVector((int)topLeft.get("x"),(int)topLeft.get("y"));
    contentBottomRight = new PVector((int)bottomRight.get("x"),(int)bottomRight.get("y"));

    this.imageInCanvas = createGraphics(gui.canvas.canvasWidth, gui.canvas.canvasHeight);
    this.parent = parent;
    switch (screen.getContentClass()){
      case "ContentBalls":
      content = new ContentBalls(this.parent,gui.canvas.canvasWidth, gui.canvas.canvasHeight);
      break;
      case "ContentVideo":
      content = new ContentVideo(this.parent);
      break;
      case "ContentVideo1":
      content = new ContentVideo1(this.parent);
      break;
      case "ContentVideo2":
      content = new ContentVideo2(this.parent);
      break;
      case "ContentVideo3":
      content = new ContentVideo3(this.parent);
      break;
    }
  }
  public void setContentTopLeft(int x, int y){
    this.contentTopLeft = new PVector(x,y);
  };
  public void setContentBottomRight(int x, int y){
    this.contentBottomRight = new PVector(x,y);
  };
  public void setMask(String _mask){
    this.mask = _mask;
  };
  public void setContentClass(String cc){
    this.contentClass = cc;
  };
  public void setContent(ContentView content){
    this.content = content;
  };
  public void setResizeMode(String rm){
    this.resizeMode = rm;
  };

  PImage draw(){
    //Get content image
    contentImage = content.draw();

    if (this.scale == -1 && contentImage.width > 0 && contentImage.height > 0){
        //Compute scale only once
        this.adjustedByWidth = computeScale(contentImage);
    } else {
      //Resize image so it best fit screen container
      if (!this.screen.resizeMode.equals("Noresize")){
        resizeImage(contentImage);
      }

      //Compute position in canvas only once
      if (positionX == -1 && positionY == -1){
        computePosition(contentImage);
      }
      //insert image in canvas size
      imageInCanvas.beginDraw();
      // imageInCanvas.clear();
      switch(this.screen.resizeMode){
        case "Cover":
          imageInCanvas.imageMode(CENTER);
          imageInCanvas.image(contentImage, (contentBottomRight.x - contentTopLeft.x)/2, (contentBottomRight.y - contentTopLeft.y)/2);
          break;
        case "Contain":
          imageInCanvas.imageMode(CENTER);
          imageInCanvas.image(contentImage, (contentBottomRight.x - contentTopLeft.x)/2, (contentBottomRight.y - contentTopLeft.y)/2);
          break;
        default:
          imageInCanvas.imageMode(CORNER);
          imageInCanvas.image(contentImage, 0, 0);
          break;
      }
      imageInCanvas.endDraw();
      imageInCanvas.mask(maskImage);

      return imageInCanvas;
    }
    return imageInCanvas;
  }

  String getContentClass(){
    return this.contentClass;
  }

  JSONObject getContentTopLeft(){
    JSONObject topLeft = new JSONObject();
    topLeft.put("x", int(contentTopLeft.x));
    topLeft.put("y", int(contentTopLeft.y));
    return topLeft;
  }

  JSONObject getContentBottomRight(){
    JSONObject bottomRight = new JSONObject();
    bottomRight.setInt("x", int(contentBottomRight.x));
    bottomRight.setInt("y", int(contentBottomRight.y));
    return bottomRight;
  }

  String getResizeMode(){
    return this.screen.resizeMode;
  }

  void saveMask(){
    println("Saving mask image");
    ArrayList<PVector> verticesArray;
    // PGraphics mask = createGraphics(ceil(abs(contentBottomRight.x - contentTopLeft.x)), ceil(abs(contentBottomRight.y - contentTopLeft.y))); //<>// //<>//
    // PGraphics mask = createGraphics(1024,768);
    PGraphics mask = createGraphics(gui.canvas.canvasWidth, gui.canvas.canvasHeight);
    ArrayList<Shape> shapes = this.screen.getShapesList();

    mask.beginDraw();
    mask.background(0);
    mask.fill(255);
    mask.noStroke();
    for (int i = 0; i < shapes.size(); i++){

      Shape shape = shapes.get(i);
      verticesArray = shape.getVerticesArray();
      switch (shape.getType()){
        case "Polygon":
        mask.beginShape();
        for (int j = 0; j < verticesArray.size(); j++){
          mask.vertex(round(verticesArray.get(j).x - contentTopLeft.x), round(verticesArray.get(j).y - contentTopLeft.y));
        }
        mask.vertex(verticesArray.get(0).x - contentTopLeft.x , verticesArray.get(0).y - contentTopLeft.y);
        mask.endShape(CLOSE);
        break;
        case "Ellipse":
        mask.ellipseMode(CORNERS);
        mask.ellipse(shape.getMinX() - contentTopLeft.x, shape.getMinY() - contentTopLeft.y, shape.getMaxX() - contentTopLeft.x, shape.getMaxY() - contentTopLeft.y);
        break;
      }
    }
    //Save mask
    mask.beginDraw();
    mask.save("data/tmp_mask.tif");
    maskImage = loadImage("data/tmp_mask.tif");
    println("End Saving mask image");
  }

  boolean computeScale(PImage image){
    float boundingBoxWidth = this.contentBottomRight.x - this.contentTopLeft.x;
    float boundingBoxHeight = this.contentBottomRight.y - this.contentTopLeft.y;
    float boundingBoxRatio = boundingBoxWidth / boundingBoxHeight;
    
    float imageRatio = image.width / image.height;
    
    switch(this.screen.resizeMode) {
      case "Cover":
        if (boundingBoxRatio > imageRatio) {
            this.scale = boundingBoxWidth / image.width;
        } else {
            this.scale = boundingBoxHeight / image.height;
        }
      break;
      case "Contain":
        if (boundingBoxRatio > imageRatio) {
            this.scale =  boundingBoxHeight / image.height;
        } else {
            this.scale = boundingBoxWidth / image.width;
        }
      break;
      case "Noresize":
        this.scale = 1;
        break;
    }
    
    this.finalImageWidth = (int)Math.floor(image.width * this.scale);
    this.finalImageHeight = (int)Math.floor(image.height * this.scale);
    
    return false;
  }

  void resizeImage(PImage image){
    if (image.width > 0 && image.height > 0){
      image.resize(this.finalImageWidth, this.finalImageHeight);
    }
  }

  void computePosition(PImage image){
    positionX = contentTopLeft.x;
    positionY = contentTopLeft.y;
    switch(this.screen.resizeMode) {
      case "Cover":
      if (adjustedByWidth) {
        positionY = contentTopLeft.y - (image.height - (contentBottomRight.y - contentTopLeft.y))/2;
      }
      else {
        positionX = contentTopLeft.x - (image.width - (contentBottomRight.x - contentTopLeft.x))/2;
      }
      break;
      case "Contain":
      if (adjustedByWidth) {
        positionY =  contentTopLeft.y + (((contentBottomRight.y - contentTopLeft.y) - image.height)/2);
      }
      else {
        positionX =  contentTopLeft.x + (((contentBottomRight.x - contentTopLeft.x) - image.width)/2);
      }
      break;
    }
  }
  void destroy(){
    content.destroy();
    content = null;
  }
}
