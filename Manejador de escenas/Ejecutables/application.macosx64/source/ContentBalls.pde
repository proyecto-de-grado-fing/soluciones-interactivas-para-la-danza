class ContentBalls extends ContentView
{
  int numBalls = 5;
  float spring = 0.05; //0.05;
  float gravity = 0.13; //0.03
  float friction = -0.9; //-0.9;
  Ball[] balls = new Ball[numBalls];

  ContentBalls(PApplet container, int canvasWidth, int canvasHeight) {
    super(container, canvasWidth, canvasHeight);
    for (int i = 0; i < numBalls; i++) {
      balls[i] = new Ball(random(canvasWidth), random(canvasHeight), random(100, 200), i, balls);
    }
    noStroke();
    fill(255, 204);
  }

  ContentBalls(PApplet container) {
    super(container);
    for (int i = 0; i < numBalls; i++) {
      balls[i] = new Ball(random(100), random(200), random(30, 60), i, balls);
    }
    noStroke();
    fill(255, 204);
  }
  void setCanvasDimens(int w,int h){
    this.canvasHeight = h;
    this.canvasWidth = w;
  }
  @Override
  PImage draw() {
    PGraphics pg = createGraphics(this.canvasWidth, this.canvasHeight);
    pg.beginDraw();
    pg.background(0);
    for (Ball ball : balls) {
      ball.collide();
      ball.move(canvasWidth, canvasHeight);
      ball.display(pg);
    }
    pg.endDraw();
    PImage pi = pg.get();
    return pi;
  }
  @Override
  void destroy(){
    println("Destroy balls");
    balls = null;
  }
  class Ball {

    float x, y;
    float diameter;
    float vx = 0;
    float vy = 0;
    int id;
    Ball[] others;

    Ball(float xin, float yin, float din, int idin, Ball[] oin) {
      x = xin;
      y = yin;
      diameter = din;
      id = idin;
      others = oin;
    }

    void collide() {
      for (int i = id + 1; i < numBalls; i++) {
        float dx = others[i].x - x;
        float dy = others[i].y - y;
        float distance = sqrt(dx*dx + dy*dy);
        float minDist = others[i].diameter/2 + diameter/2;
        if (distance <= minDist) {
          float angle = atan2(dy, dx);
          float targetX = x + cos(angle) * minDist;
          float targetY = y + sin(angle) * minDist;
          float ax = (targetX - others[i].x) * spring;
          float ay = (targetY - others[i].y) * spring;
          vx -= ax;
          vy -= ay;
          others[i].vx += ax;
          others[i].vy += ay;
        }
      }
    }

    void move(int canvasWidth, int canvasHeight) {
      vy += gravity;
      x += vx;
      y += vy;
      if (x + diameter/2 > canvasWidth) {
        x = canvasWidth - diameter/2;
        vx *= friction;
        } else if (x - diameter/2 < 0) {
          x = diameter/2;
          vx *= friction;
        }
        if (y + diameter/2 > canvasHeight) {
          y = canvasHeight - diameter/2;
          vy *= friction;
          } else if (y - diameter/2 < 0) {
            y = diameter/2;
            vy *= friction;
          }
        }

        void display(PGraphics pg) {
          pg.ellipse(x, y, diameter, diameter);
        }
      }
    }
