class ContentView
{
  int canvasWidth;
  int canvasHeight;
  PApplet container;

  ContentView (PApplet container, int canvasWidth, int canvasHeight){
    this.container = container;
    this.canvasWidth = canvasWidth;
    this.canvasHeight = canvasHeight;
  };
  ContentView (PApplet container){
    this.container = container;
  };

  PImage draw(){
    return createGraphics(canvasWidth, canvasHeight);
  };

  void destroy(){

  }
}
