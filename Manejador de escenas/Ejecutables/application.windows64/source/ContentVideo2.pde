import processing.video.*;

class ContentVideo2 extends ContentView
{
  Movie movie;
  Movie myMovie;

  ContentVideo2(PApplet container, int canvasWidth, int canvasHeight) {
    super(container, canvasWidth, canvasHeight);

    myMovie = new Movie(container, "3.mp4");
    myMovie.loop();
  }

  ContentVideo2(PApplet container) {
    super(container);
    myMovie = new Movie(container, "3.mp4");
    myMovie.loop();
  }

  @Override
  PImage draw() {
    if (myMovie.available()) {
      myMovie.read();
    }
    // image (myMovie, 500, 0);
    return myMovie.get();
  }
  @Override
  void destroy(){
    println("Destroy movie");
    this.myMovie.stop();
    this.myMovie.dispose();
  }
}
