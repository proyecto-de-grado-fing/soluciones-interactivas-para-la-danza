import org.openkinect.freenect.*;
import org.openkinect.processing.*;
import controlP5.*;

Gui gui = null;
int canvasWidth = 1024;
int canvasHeight = 768;
Textfield canvasWidthInput,canvasHeightInput;
ControlP5 cp5;
// ContentView content;

void settings(){
  // size(canvasWidth, canvasHeight);
  size(240,500);
}

void setup() {
  // content = new ContentVideo1(this);
  getContentFiles();
  this.cp5 = new ControlP5(this);

  this.cp5.addTextlabel("dancing")
  .setText("DANCING")
  .setColor(0)
  .setFont(createFont("arial",15))
  .setPosition(85,20);
  this.cp5.addTextlabel("with")
  .setText("with")
  .setColor(0)
  .setFont(createFont("arial",15))
  .setPosition(105,40);
  this.cp5.addTextlabel("processing")
  .setText("PROCESSING")
  .setColor(0)
  .setFont(createFont("arial",15))
  .setPosition(70,60);

  //Canvas width and height
  canvasWidthInput = cp5.addTextfield("canvasWidthInput")
  .setLabel("Ancho")
  .setFont(createFont("arial",14))
  .setPosition(20, 110)
  .setText("1024")
  .setSize(200,50);

  Label labelCanvaswidth = canvasWidthInput.getCaptionLabel();
  labelCanvaswidth.setColor(0);
  labelCanvaswidth.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);

  canvasHeightInput = cp5.addTextfield("canvasHeightInput")
  .setLabel("Alto")
  .setFont(createFont("arial",14))
  .setPosition(20, 190)
  .setText("768")
  .setSize(200,50);

  Label labelCanvasHeight = canvasHeightInput.getCaptionLabel();
  labelCanvasHeight.setColor(0);
  labelCanvasHeight.align(ControlP5.LEFT, ControlP5.TOP_OUTSIDE);


  cp5.addButton("newConfig")
  .setLabel("Crear configuracion")
  .setPosition(20, 270)
  .setSize(200, 50);

  cp5.addButton("loadConfig")
  .setLabel("Cargar configuracion")
  .setPosition(20, 330)
  .setSize(200, 50);

  cp5.addButton("exit")
  .setLabel("Salir")
  .setPosition(70, 440)
  .setSize(100, 50);
}

void draw() {
  //Black background
  background(255);
}

void keyPressed() {
}

void controlEvent(ControlEvent theEvent) {
  //Get every event name
  switch(theEvent.getName()){
    case "loadConfig":
    loadScenes();
    break;
    case "newConfig":
    newConfig();
    break;
  }
}

void newConfig() {
  if(this.gui == null){
    this.canvasWidth = Integer.parseInt(trim(canvasWidthInput.getText()));
    this.canvasHeight = Integer.parseInt(trim(canvasHeightInput.getText()));
    this.gui = new Gui(this, this.canvasWidth, this.canvasHeight);
  }
  else
  {
    //TODO: destroy and create?
  }
}

void loadScenes() {

  selectInput("Select a file to process:", "fileSelected");
}

public void fileSelected(File selection) {
  if (selection == null) {
    println("No file selected");
  }
  else
  {
    String fileRelative = selection.getPath().substring(dataPath("").length(), selection.getPath().length());

    //Load config from file
    //1 - Get file
    JSONObject file = loadJSONObject("data" + fileRelative);
    //2 - get JSON data
    // JSONObject values = file.getJSONObject(0);
    //GET canvas height & width
    this.canvasWidth = file.getInt("canvasWidth");
    this.canvasHeight = file.getInt("canvasHeight");

    if(this.gui == null){
      this.gui = new Gui(this, this.canvasWidth, this.canvasHeight);
    }
    //3 - Get Scenes
    JSONArray scenes = file.getJSONArray("scenes");
    for (int i = 0; i < scenes.size(); i++) {
      JSONObject scene = (JSONObject) scenes.getJSONObject(i);

      Scene tmp = new Scene(scene.getString("name"));
      //this.gui.controls.addScene(scene.getString("name"));
      //Get & add sceens to scene
      JSONArray tmp_screens = scene.getJSONArray("screens");
      for (int j = 0; j < tmp_screens.size(); j++) {
        JSONObject scr = tmp_screens.getJSONObject(j);
        Screen tmp_screen = new Screen(scr.getString("name"));
        //gui.controls.addScreen(scr.getString("name"),scene.getString("name"));

        JSONObject _contentTopLeft = scr.getJSONObject("contentTopLeft");
        JSONObject _contentBottomRight = scr.getJSONObject("contentBottomRight");
        tmp_screen.setContentTopLeft(_contentTopLeft.getInt("x"),_contentTopLeft.getInt("y"));
        tmp_screen.setContentBottomRight(_contentBottomRight.getInt("x"),_contentBottomRight.getInt("y"));

        tmp_screen.setMask(scr.getString("mask"));
        tmp_screen.setResizeMode(scr.getString("resizeMode"));
        tmp_screen.setContentClass(scr.getString("contentClass"));

        JSONArray _shapes = (JSONArray) scr.getJSONArray("shapes");

        tmp_screen.setShapes(_shapes);
        tmp.addScreen(tmp_screen);
      }
      //Add scene
      this.gui.controls.addScene(tmp);
      //this.gui.controls.updateScenesDropdown();
    }
  }

}

boolean saveConfigSetup(JSONObject json, String filename){
  return saveJSONObject(json, "data/" + filename + "/" + filename + ".json");
}
