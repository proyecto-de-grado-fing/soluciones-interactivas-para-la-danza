import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import org.openkinect.freenect.*; 
import org.openkinect.processing.*; 
import gab.opencv.*; 
import processing.sound.*; 
import processing.video.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Controlador extends PApplet {



// import controlP5.*;
// import KinectProjectorToolkit.*;




final int SCENE_PARTICLES = 0;
final int SCENE_VIDEO = 1;
final int SCENE_LINES = 2;
final int SCENE_PANAL = 3;
final int SCENE_PICTURE = 4;

private ContentView contentLetras;
private ContentView contentFluid;
private ContentView contentLineas;
private ContentView contentVideo;
private ContentView contentPicture;
private ContentView content;
private PImage ret;
private int status = 0;
private Kinect kinect;
private Boolean showDepth = false;
private int scene = SCENE_PARTICLES;
private int[] background;
private SoundFile rue;
private SoundFile divenire;

public void setup(){
    // size(1024, 768, FX2D);
    
    noCursor();
    // context = new SimpleOpenNI(this);
    // this.background = context.depthMap();
    kinect = new Kinect(this);
    kinect.initDepth();
    kinect.enableMirror(false);
    background = new int[kinect.width*kinect.height];
    arrayCopy(kinect.getRawDepth(),background);

    contentLetras = new LetrasParticulas(this, width, height, kinect, background);
    contentVideo = new ContentVideo(this, width, height);
    contentLineas = new Lineas(this, width, height, kinect, background);
    contentFluid = new FluidWithBlobs(this, width, height, kinect, background);
    PImage backPic = loadImage("fondoEstrellas.jpg");
    contentPicture = new ContentMePicture(this, width, height, kinect, background, backPic);
    content = contentLetras;

    rue = new SoundFile(this, "music.mp3");
    // rue = new SoundFile(this, "rue-des-cascades-s.mp3");
    // rue.rate(0.72);
    // divenire = new SoundFile(this, "divenire.mp3");
}
public void draw(){
    ret = content.draw();
    if (ret != null){
        image(ret,0,0,width,height);
    }
    if(showDepth){
        image(kinect.getDepthImage(),0,0,320,240);
        println(frameRate);
    }
    if (content.isFinished() == 1) {
        this.nextScene();
    }
    // if(rue.isPlaying() && rue.duration()){
    //
    // }
    // stroke(255);
    // fill(255);
    // textSize(30);
    // text(frameRate, 20, 1000);
}
public void keyPressed(){
    //START - transition in
    if (key == '0') {
        status = 0;
    }
    //MAIN
    if (key == '1') {
        status = 1;
    }
    //END - transition Out
    if (key == '2') {
        status = 2;
    }
    // if (key == 'g') gravity = !gravity;
    // if (key == 'l') lines = !lines;
    if (key == 'I' || key == 'i') {

            println("Background loaded");
        if (contentLetras != null){
            contentLetras.loadInitialDepth();
        }
        if (contentFluid != null){
            contentFluid.loadInitialDepth();
        }
        if (contentLineas != null){
            contentLineas.loadInitialDepth();
        }
        if (contentPicture != null){
            contentPicture.loadInitialDepth();
        }

    }
    if ((key == 'd') || (key == 'D')) showDepth = !showDepth;
    // if (key == 't') psico+= 0.005;
    // if (key == 'f') psico-= 0.005;
    // if (keyCode == UP) influenceRadius+=5;
    // if (keyCode == DOWN) influenceRadius-=5;
    // if (keyCode == RIGHT) sWeightFluid ++;
    // if (keyCode == LEFT) sWeightFluid --;
    if (keyCode == RIGHT) {
        this.nextScene();
    }
    content.setStatus(status);
}
public void nextScene(){
    status = 0;
    scene = (scene+1) % 6;
    switch (scene){
        case SCENE_PARTICLES:
        kinect = new Kinect(this);
        content =  contentLetras;
        break;
        case SCENE_VIDEO:
        contentLetras = null;
        contentVideo.rewindMovie();
        content = contentVideo;
        break;
        case SCENE_LINES:
        contentVideo.rewindMovie();
        contentVideo = null;
        content = contentLineas;
        rue.play();
        content.setStartTime();
        break;
        case SCENE_PANAL:
        contentLineas = null;
        content = contentFluid;
        content.setStartTime();
        break;
        case SCENE_PICTURE:
        contentFluid = null;
        content = contentPicture;
        content.setStartTime();
        break;
    }
}
// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/1scFcY-xMrI

class Blob {
    PVector min;
    PVector max;
    float depth;
    PShape blobShape;
    int mass;
    int bColor = color(255, 255, 255);

    ArrayList<PVector> points;
    ArrayList<PVector> contour;

    Blob(float x, float y, float depth) {
        this.min = new PVector(x,y, depth);
        this.max = new PVector(x,y, depth);
        this.depth = depth;

        points = new ArrayList<PVector>();
        points.add(new PVector(x, y, depth));
    }

    Blob(PVector min, PVector max, int mass, ArrayList<PVector> points, ArrayList<PVector> contour) {
        this.min = min;
        this.max = max;
        this.mass = mass;

        this.points = points;
        this.contour = contour;
    }
    public void show(int imgWidth, int imgHeight) {
        stroke(bColor);
        fill(bColor);
        strokeWeight(2);

        for (PVector v : contour) {
            v.x = map(v.x,0,640,0,imgWidth);
            v.y = map(v.y,0,480,0,imgHeight);
            point(v.x, v.y);
        }        
    }


    public void add(float x, float y, float depth) {
        points.add(new PVector(x, y, depth));
        min.x = min(min.x, x);
        min.y = min(min.y, y);
        max.x = max(max.x, x);
        max.y = max(max.y, y);
        this.depth = depth;
    }

    public float size() {
        // return (max.x-min.x)*(max.y-min.y);
        return points.size();
    }

    public float height() {
        return max.y-min.y;
    }

    public float width() {
        return max.x-min.x;
    }

    public PVector getTopLeft() {
        return min;
    }
    public PVector getBottomRight() {
        return max;
    }

    public PVector getTop(){
        return new PVector (((max.x - min.x) / 2) + min.x, min.y);
        // return new PVector(((max.x - min.x) / 2) + min.x, ((max.y - min.y) / 2) + min.y);
    }
    public PVector getCenter(){
        // return new PVector (((max.x - min.x) / 2) + min.x, min.y);
        return new PVector(((max.x - min.x) / 2) + min.x, ((max.y - min.y) / 2) + min.y);
    }

    public ArrayList<PVector> getPoints(){
        return points;
    }

    public ArrayList<PVector> getContour(boolean order){
        if (order){
            int contourSize = contour.size();
            int i;
            int j;
            PVector currentPt;
            float minDist;
            float curDist;
            int minIndex;

            for (i = 0; i < contourSize - 1; i++){
                currentPt = contour.get(i);
                minDist = Integer.MAX_VALUE;
                minIndex = i;
                // Find nearest point
                for (j = i + 1; j < contourSize; j++){
                    curDist = this.distSq(currentPt, contour.get(j));
                    if (curDist < minDist){
                        minDist = curDist;
                        minIndex = j;
                    }
                }
                swap(contour, i + 1, minIndex);
            }
        }
        return contour;
    }

    public void swap(ArrayList<PVector> list, int index1, int index2) {
        PVector temp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, temp);
    }

    public float distSq(PVector p1, PVector p2) {
        return this.distSq(p1.x, p1.y, p2.x, p2.y);
    }

    public float distSq(float x1, float y1, float x2, float y2) {
        float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1);
        return d;
    }
    public float distSq(float x1, float y1, float z1, float x2, float y2, float z2) {
        float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1);
        return d;
    }
}
class BlobDetection{
    private int tolerance = 10;
    private ArrayList<Blob> blobs;
    private int offset;
    private BlobFinder finder;
    private int kinectWidth;
    private int kinectHeight;

    private boolean[] img;
    private Blob biggest = null;

    private OpenCV opencv;

    public BlobDetection(PApplet container, int kinectWidth, int kinectHeight) {
        this.kinectWidth = kinectWidth;
        this.kinectHeight = kinectHeight;
        this.finder = new BlobFinder(kinectWidth, kinectHeight);
        opencv = new OpenCV(container, kinectWidth, kinectHeight);
    }

    public Blob getBlob(int[] background, int[] foreground){
        img = new boolean[foreground.length];
        offset = 0;
        for (int x = 0; x < kinectWidth; x++) {
            for (int y = 0; y < kinectHeight; y++) {
                offset = x + y*kinectWidth;

                if ((foreground[offset] < 1500) && (abs(foreground[offset] - background[offset]) > tolerance)){
                    img[offset] = true;
                }
                else {
                    img[offset] = false;
                }
            }
        }
        if (blobs != null){
            blobs.clear();
        }

        //Using Blob Finder
        BlobFinder finder = new BlobFinder(kinectWidth, kinectHeight);
        blobs = finder.detectBlobs(img, 1000, -1);

        biggest = null;
        for (Blob b : blobs) {
            if (biggest == null || b.size() > biggest.size()){
                biggest = b;
            }
        }
        if (biggest != null){
            return biggest;
        }
        return null;
    }

    public ArrayList<PVector> getContour(int[] background, int[] foreground, boolean reduce){
        int[] img = new int[foreground.length];
        offset = 0;
        for (int x = 0; x < kinectWidth; x++) {
            for (int y = 0; y < kinectHeight; y++) {
                offset = x + y * kinectWidth;

                if ((foreground[offset] < 1500) && (abs(foreground[offset] - background[offset]) > tolerance)){
                    img[offset] = 255;
                }
                else {
                    img[offset] = 0;
                }
            }
        }
        PImage image = createImage(kinectWidth, kinectHeight, RGB);
        image.loadPixels();
        for (int i = 0; i < image.pixels.length; i++) {
            image.pixels[i] = img[i];
        }
        image.updatePixels();
        opencv.loadImage(image);
        ArrayList<Contour> contours = opencv.findContours();
        Contour biggest = null;
        for (Contour c : contours){
            if (biggest == null || c.area() > biggest.area()){
                biggest = c;
            }
        }
        if (biggest != null){
            ArrayList<PVector> points = biggest.getPoints();
            if (reduce){
                points = reduceSize(points, points.size()/2);
            }
            return points;
        } else {
            return null;
        }
    }

    public ArrayList<PVector> reduceSize(ArrayList<PVector> contour, int newSize){
        int index;
        int size = contour.size();
        while (size > newSize){
            index = PApplet.parseInt(random(0, size - 1));
            contour.remove(index);
            size--;
        }
        return contour;
    }
}
class BlobFinder {

    private int width;
    private int height;

    private int[] labelBuffer;

    private int[] labelTable;
    private int[] xMinTable;
    private int[] xMaxTable;
    private int[] yMinTable;
    private int[] yMaxTable;
    private int[] massTable;

    private PVector last;

    private ArrayList<PVector>[] contourVector;
    private ArrayList<PVector>[] pointsVector;

    public BlobFinder(int width, int height)
    {
        this.width = width;
        this.height = height;

        labelBuffer = new int[width * height];

        // The maximum number of blobs is given by an image filled with equally spaced single pixel
        // blobs. For images with less blobs, memory will be wasted, but this approach is simpler and
        // probably quicker than dynamically resizing arrays
        int tableSize = width * height / 4;

        labelTable = new int[tableSize];
        xMinTable = new int[tableSize];
        xMaxTable = new int[tableSize];
        yMinTable = new int[tableSize];
        yMaxTable = new int[tableSize];
        massTable = new int[tableSize];
        pointsVector = new ArrayList[tableSize];
        contourVector = new ArrayList[tableSize];
    }

    public ArrayList<Blob> detectBlobs(boolean[] srcData, int minBlobMass, int maxBlobMass)
    {
        // This is the neighbouring pixel pattern. For position X, A, B, C & D are checked
        // A B C
        // D X
        ArrayList<Blob> blobList = new ArrayList<Blob>();

        int srcPtr = 0;
        int aPtr = -width - 1;
        int bPtr = -width;
        int cPtr = -width + 1;
        int dPtr = -1;

        int label = 1;

        // Iterate through pixels looking for connected regions. Assigning labels
        for (int y=0 ; y<height ; y++)
        {
            for (int x=0 ; x<width ; x++)
            {
                labelBuffer[srcPtr] = 0;

                // Check if on foreground pixel
                if (srcData[srcPtr])
                {
                    // Find label for neighbours (0 if out of range)
                    int aLabel = (x > 0 && y > 0)			? labelTable[labelBuffer[aPtr]] : 0;
                    int bLabel = (y > 0)						? labelTable[labelBuffer[bPtr]] : 0;
                    int cLabel = (x < width-1 && y > 0)	? labelTable[labelBuffer[cPtr]] : 0;
                    int dLabel = (x > 0)						? labelTable[labelBuffer[dPtr]] : 0;

                    // Look for label with least value
                    int min = Integer.MAX_VALUE;
                    if (aLabel != 0 && aLabel < min) min = aLabel;
                    if (bLabel != 0 && bLabel < min) min = bLabel;
                    if (cLabel != 0 && cLabel < min) min = cLabel;
                    if (dLabel != 0 && dLabel < min) min = dLabel;

                    // If no neighbours in foreground
                    if (min == Integer.MAX_VALUE)
                    {
                        labelBuffer[srcPtr] = label;
                        labelTable[label] = label;

                        // Initialise min/max x,y for label
                        yMinTable[label] = y;
                        yMaxTable[label] = y;
                        xMinTable[label] = x;
                        xMaxTable[label] = x;
                        massTable[label] = 1;
                        pointsVector[label] = new ArrayList();
                        pointsVector[label].add(new PVector(x,y));

                        contourVector[label] = new ArrayList();
                        contourVector[label].add(new PVector(x,y));
                        last = contourVector[label].get(0);

                        label ++;
                    }

                    // Neighbour found
                    else
                    {
                        // Label pixel with lowest label from neighbours
                        labelBuffer[srcPtr] = min;

                        // Update min/max x,y for label
                        yMaxTable[min] = y;
                        massTable[min]++;

                        pointsVector[min].add(new PVector(x,y));

                        if (x < xMinTable[min]) xMinTable[min] = x;
                        if (x > xMaxTable[min]) xMaxTable[min] = x;

                        if (aLabel != 0) labelTable[aLabel] = min;
                        if (bLabel != 0) labelTable[bLabel] = min;
                        if (cLabel != 0) labelTable[cLabel] = min;
                        if (dLabel != 0) labelTable[dLabel] = min;

                        if (aLabel == 0 || bLabel == 0 || cLabel == 0 || dLabel == 0){
                            contourVector[min].add(new PVector(x,y));
                        }
                    }
                }

                srcPtr ++;
                aPtr ++;
                bPtr ++;
                cPtr ++;
                dPtr ++;
            }
        }

        // Iterate through labels pushing min/max x,y values towards minimum label
        if (blobList == null) blobList = new ArrayList<Blob>();

        for (int i=label-1 ; i>0 ; i--)
        {
            if (labelTable[i] != i)
            {
                if (xMaxTable[i] > xMaxTable[labelTable[i]]) xMaxTable[labelTable[i]] = xMaxTable[i];
                if (xMinTable[i] < xMinTable[labelTable[i]]) xMinTable[labelTable[i]] = xMinTable[i];
                if (yMaxTable[i] > yMaxTable[labelTable[i]]) yMaxTable[labelTable[i]] = yMaxTable[i];
                if (yMinTable[i] < yMinTable[labelTable[i]]) yMinTable[labelTable[i]] = yMinTable[i];
                massTable[labelTable[i]] += massTable[i];
                pointsVector[labelTable[i]].addAll(pointsVector[i]);
                contourVector[labelTable[i]].addAll(contourVector[i]);

                int l = i;
                while (l != labelTable[l]) l = labelTable[l];
                labelTable[i] = l;
            }
            else
            {
                // Ignore blobs that butt against corners
                if (i == labelBuffer[0]) continue;									// Top Left
                if (i == labelBuffer[width]) continue;								// Top Right
                if (i == labelBuffer[(width*height) - width + 1]) continue;	// Bottom Left
                if (i == labelBuffer[(width*height) - 1]) continue;			// Bottom Right

                if (massTable[i] >= minBlobMass && (massTable[i] <= maxBlobMass || maxBlobMass == -1))
                {
                    Blob blob = new Blob(new PVector(xMinTable[i], yMinTable[i]), new PVector(xMaxTable[i], yMaxTable[i]), massTable[i], pointsVector[i], contourVector[i]);
                    blobList.add(blob);
                }
            }
        }

        return blobList;
    }
}
class ContentMePicture extends ContentView
{
    Kinect kinect;
    BlobDetection blobDetection;
    int[] background;

    PImage backPic;
    PImage bImg;
    PImage img;
    // PImage bigImg;

    int canvasWidth;
    int canvasHeight;
    int status = 0;
    int finished = 0;
    int backgroundColor = color(17,51,77);
    float backgroundTransparency = 255;
    float startTime,timeRelative,timeRunning;
    float opacity = 255;

    ContentMePicture(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background, PImage backPic){
        super(container, canvasWidth, canvasHeight);
        this.kinect = kinect;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
        //Load image and redim to fit kinect size
        // backPic = backPic;
        PImage img1 = createImage(640, 480,ARGB);
        img1.copy(backPic, 0, 0, backPic.width, backPic.height, 0, 0, kinect.width, kinect.height);
        this.backPic = img1;
    };

    public void setStatus(int s){
        this.status = s;
    }
    public int isFinished(){
        return finished;
    }
    public void setStartTime(){
        this.startTime = millis();
        this.timeRelative = (millis() - startTime) / 1000;
    }

    public @Override
    PImage draw() {
        fill(backgroundColor, backgroundTransparency);
        noStroke();
        rect(0, 0, width, height);
        timeRunning = (millis() - startTime) / 1000;
        println(timeRunning);

        switch (this.status){
            case 0:
            this.transitionIN();
            if(timeRunning >= 15) {
                opacity = 0;
                this.status = 1;
            }
            break;
            case 1:
            this.mainContent();
            if(timeRunning >= 96){
                this.status = 2;
            }
            break;
            case 2:
            this.transitionOUT();
            // if(timeRunning > 96) {
            //     finished = 1;
            // }
            break;
        }
        if (this.status != 2){
            bImg = getMeImg();
            image(bImg,0,0);
        }

        fill(backgroundColor, opacity);
        rect(0, 0, width, height);
        return null;
    };

    public void transitionIN(){
        if (timeRunning > 5){
            if(timeRunning - timeRelative >= 0.25f) {
                //Foreground rect to fade in
                opacity = (opacity > 6.4f) ? opacity - 6.4f : 0;
                timeRelative = timeRunning;
            }
        }
    }

    public void mainContent(){
        //Set params para stela incremental
        if(timeRunning - timeRelative >= 1) {
            backgroundTransparency = (backgroundTransparency > 4.5f) ? backgroundTransparency - 4.5f : 0;
            timeRelative = timeRunning;
        }
        if(timeRunning >= 78){
            backgroundTransparency = 0;
            //set params para que quede lo q limpia
        }
    }

    public void transitionOUT(){
        backgroundColor = color(0, 0, 0);

        if(timeRunning - timeRelative >= 0.25f) {
            opacity = (opacity < 255) ? opacity + 0.5f : 255;
            timeRelative = timeRunning;
        }
    }

    public PImage getMeImg(){
        // PImage img = new PImage(this.kinect.width, this.kinect.height, ARGB);
        // img.loadPixels();
        //
        // int index;
        //
        // Blob blob = blobDetection.getBlob(background, kinect.getRawDepth());
        // if (blob != null){
        //     for (PVector v : blob.getPoints()){
        //         index = (int) (v.x + v.y * this.kinect.width);
        //         img.pixels[index] = backPic.pixels[index];
        //     }
        // }
        // img.updatePixels();
        // img.resize(canvasWidth, canvasHeight);
        // return img;


        PGraphics img = createGraphics(kinect.width, kinect.height);
        img.beginDraw();
        PShape user = createShape();
        user.beginShape();
        ArrayList<PVector> contour = blobDetection.getContour(background, kinect.getRawDepth(), true);
        if (contour != null){
            user.fill(255,255,255);
            user.noStroke();
            for (PVector point : contour){
                user.vertex(point.x, point.y);
            }
        }
        user.endShape(CLOSE);
        img.shape(user);
        img.endDraw();
        PImage cleanBack = backPic.copy();
        cleanBack.mask(img);
        cleanBack.resize(canvasWidth, canvasHeight);
        return cleanBack;
    }

    public void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
    }

}
class ContentVideo extends ContentView
{
    Movie movie;
    Movie myMovie;
    int status = 0;
    int canvasWidth;
    int canvasHeight;
    int opacity;
    PGraphics pg;
    PApplet container;
    int finished = 0;
    int startTime;

    ContentVideo(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        myMovie = new Movie(container, "2.mp4");
        myMovie.stop();
        opacity = 255;
    }

    ContentVideo(PApplet container) {
        super(container);
        myMovie = new Movie(container, "2.mp4");
        myMovie.stop();
        opacity = 255;
    }
    public void setStatus(int s){
        this.status = s;
    }
    public void rewindMovie(){
        myMovie.stop();
    }
    public int isFinished(){
        return finished;
    }

    public @Override
    PImage draw() {
        myMovie.play();
        if (myMovie.available()) {
            myMovie.read();
        }
        if(this.status==0){
            if((millis() / 300) % 1 == 0){
                if(opacity-8 > 0) {
                    opacity -= 8;
                }else{
                    opacity = 0;
                    this.status = 1;
                }
            }
        }
        if (this.status == 1){
            if(myMovie.time() >= (myMovie.duration())){
                this.status=2;
            }
        }
        if(this.status==2){
            if((millis() / 300) % 1 == 0){
                if(opacity+8 < 255) {
                    opacity += 8;
                }else{
                    opacity = 255;
                    finished = 1;
                }
            }
        }
        // pg = createGraphics(this.canvasWidth, this.canvasHeight,JAVA2D);
        // pg.beginDraw();
        image(myMovie.get(),0,0,this.canvasWidth,this.canvasHeight);
        fill(0,0,0,opacity);
        noStroke();
        rect(0,0,this.canvasWidth,this.canvasHeight);
        // pg.fill(0,0,0,opacity);
        // pg.rect(0,0,this.canvasWidth,this.canvasHeight);
        // pg.endDraw();
        // PImage pi = pg.get();
        return null;
    }
}
class ContentView
{
  int canvasWidth;
  int canvasHeight;
  PApplet container;

  ContentView (PApplet container, int canvasWidth, int canvasHeight){
    this.container = container;
    this.canvasWidth = canvasWidth;
    this.canvasHeight = canvasHeight;
  };
  ContentView (PApplet container){
    this.container = container;
  };

  public PImage draw(){
    return createGraphics(canvasWidth, canvasHeight);
  };
  public void loadInitialDepth(){
      background = new int[kinect.width*kinect.height];
      arrayCopy(kinect.getRawDepth(),background);
      println("back contentview");
  }
  public void setStatus(int s){

  }
  public void destroy(){

  }
  public void rewindMovie(){}
  public int isFinished(){
      return 0;
  }
  public void setStartTime(){}
}
class FluidWithBlobs extends ContentView{
    Fluid f;
    boolean hasDancers;
    public PGraphics pg;
    boolean reject = false;
    int backColFluid = color(0);
    float strokeColorR = 255;
    float strokeColorG = 0;
    float strokeColorB = 0;
    int strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
    float sWeightFluid = 200;
    PVector dPos;
    boolean lines = true;
    int influenceRadius = 1;
    boolean gravity = false;
    float psico = 0.02f;
    //
    BlobDetection blobDetection;
    Kinect kinect;
    int[] background;
    int opacity = 255;

    int status = 0;
    PImage ret;
    float startTime;
    float timeRunning;
    int finished = 0;
    float timeRelative;

    int loopCount = 0;
    int loopMod = 8;

    FluidWithBlobs(PApplet container, Kinect kinect){
        super(container);
        f = new Fluid();
        f.initialScene();
        this.kinect = kinect;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }
    FluidWithBlobs(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background){
        super(container,canvasWidth,canvasHeight);
        f = new Fluid();
        f.initialScene();
        this.kinect = kinect;
        // this.kinect.initDepth();
        // this.kinect.enableMirror(false);
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }

    public void setStatus(int status){
        this.status = status;
    }
    public void setStartTime(){
        this.startTime = millis();
        this.timeRelative = (millis() - startTime) / 1000;
    }

    public PImage draw(){
        background(0);
        timeRunning = (millis() - startTime) / 1000;
        println(timeRunning);
        switch (this.status){
            case 0:
            ret = this.transitionIN();
            if(timeRunning >= 29){
                this.status = 1;
            }
            break;
            case 1:
            ret = this.mainContent();
            if(timeRunning >= 209){
                this.opacity = 0;
                this.status = 2;
            }
            break;
            case 2:
            ret = this.transitionOUT();
            if(timeRunning > 234) {
                finished = 1;
            }
            break;
        }
        return ret;
    }

    public PImage transitionIN(){
        if(timeRunning - timeRelative >= 0.25f){
            if (timeRunning < 10){
                sWeightFluid = (sWeightFluid > 6.5f) ? sWeightFluid - 3.5f : 3;
            }
            if (timeRunning >= 10 && timeRunning < 20){
                sWeightFluid = (sWeightFluid > 4.5f) ? sWeightFluid - 1.5f : 3;
                influenceRadius = (influenceRadius < 120) ? influenceRadius + 4 : 120;
            }
            else {
                sWeightFluid = (sWeightFluid > 3.55f) ? sWeightFluid - 0.55f : 3;
                influenceRadius = (influenceRadius < 120) ? influenceRadius + 2 : 120;
            }
            timeRelative = timeRunning;
        }
        f.drawScene(pg);
        return null;
    }
    public PImage mainContent(){
        opacity = 255;
        if (timeRunning >= 68 && timeRunning < 129){
            this.status = 1;
            this.influenceRadius = 200;
            this.reject = false;
        }
        else if (timeRunning >= 129 && timeRunning < 152){
            this.reject = true;
            this.influenceRadius = 120;
            this.gravity = true;
        }
        else if (timeRunning >= 152 && timeRunning < 182){

            if(timeRunning - timeRelative >= 0.25f){
                strokeColorR = (strokeColorR > 2.225f) ? strokeColorR - 2.225f : 0;
                strokeColorB = (strokeColorB < 252.775f) ? strokeColorB + 2.225f : 255;
                timeRelative = timeRunning;
                loopCount ++;
                if (gravity){
                    loopMod = 3;
                } else {
                    loopMod = 8;
                }
                if (loopCount % loopMod == 0){
                    this.gravity = !this.gravity;
                    loopCount = 0;
                }
            }
        }
        else if (timeRunning >= 182 && timeRunning < 201){
            strokeColorR =  0;
            strokeColorB =  255;
            this.gravity = false;
            this.reject = true;
        }
        strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
        f.drawScene(pg);
        return null;
    }
    public PImage transitionOUT(){
        // 16 sec
        if(timeRunning - timeRelative >= 0.25f){
            strokeColorR = (strokeColorR < 16.73f) ? strokeColorR + 0.27f : 17;
            strokeColorG = (strokeColorG < 50.2f) ? strokeColorG + 0.8f : 51;
            strokeColorB = (strokeColorB > 79.8f) ? strokeColorB - 2.8f : 77;
            this.opacity = (this.opacity < 251) ? this.opacity + 4 : 255;
            timeRelative = timeRunning;
        }
        strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
        f.drawScene(pg);
        fill(17, 51, 77, this.opacity);
        noStroke();
        rect(0, 0 ,this.canvasWidth,this.canvasHeight);
        return null;
    }
    public int isFinished(){
        return finished;
    }
    public void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
    }

    class Fluid
    {
        // en total 1500 particulas
        int particleCount = 2000;
        Particle[] particles = new Particle[particleCount];

        public Fluid(){};

        public void closeScene(){};
        public void initialScene(){
            reject = true;
            for (int i = 0; i < particleCount; i++) {
                particles[i] = new Particle();
            }
        };
        public void drawScene(PGraphics pg){
            //    blendMode(BLEND);
            // background(backColFluid);
            // image(kinect.getDepthImage(),0,0, width, height);

            Blob blob = blobDetection.getBlob(background, kinect.getRawDepth());
            // buscamos al usuario
            if (blob != null){
                hasDancers = true;
                dPos = blob.getTop();
                // blob.show(width, height);
                dPos.x = map(dPos.x,0,640,0,width);
                dPos.y = map(dPos.y,0,480,0,height);
                // definimos color del fondo y del trazo
                fill(backColFluid);
                stroke(255);
                //    stroke (255 - backColFluid);
                // definimos el grosor del trazo
                strokeWeight(sWeightFluid);
                // actualizamos las posiciones de las particulas
                for (int i = 0; i < particleCount; i++) {
                    Particle particle = (Particle) particles[i];
                    particle.update(i);
                }
            }
            // return pg;
        };
        public String getSceneName(){return "Fluid";};

        class Particle {
            // coordenada x,y
            float x;
            float y;
            // velocidad en eje x y y
            float vx;
            float vy;
            // se incia en una posicion randomin=ca en la pantalla
            Particle() {
                x = random(10,width-10);
                y = random(10,height-10);
            }
            // devuelve la posicion x
            public float getx() {
                return x;
            }
            // devuelve la posicion y
            public float gety() {
                return y;
            }

            // para definir si dibujamos la linea o no
            boolean drawthis;
            Particle particle;
            float tx;
            float ty;
            float radius;
            float angle;
            int px;
            int py;
            int i;
            public void update(int num) {
                // simulamos la friccion
                vx *= 0.90f;
                vy *= 0.90f;
                // vx *= 0.87;
                // vy *= 0.87;
                // Loopeamos para checkear la influencia entre si de las particulas
                for (i = 0; i < particleCount; i++) {
                    // i = num no nos interesa porque seria calcular influencia de uno a uno mismo
                    if (i != num) {
                        drawthis = false;
                        // particula vecina
                        particle = (Particle) particles[i];
                        tx = particle.getx();
                        ty = particle.gety();
                        // la distancia entre la particula actual y la particula vecina
                        radius = dist(x,y,tx,ty);
                        // las lineas las dibujamos solo entre las particulas en radio menor de 35
                        if (radius < 35) {
                            drawthis = true;
                            // el angulo para poder dibujar la linea
                            angle = atan2(y-ty,x-tx);
                            // si esta muy cerca - se desvia
                            if (radius < 30) {
                                // cambiamos las velocidades
                                vx += (30 - radius) * 0.07f * cos(angle);
                                vy += (30 - radius) * 0.07f * sin(angle);
                            }
                            if (radius > 25) {
                                // si estan entre 25 y 30 se acercan
                                // vx -= (25 - radius) * 0.005 * cos(angle);
                                // vy -= (25 - radius) * 0.005 * sin(angle);
                                vx -= (25 - radius) * psico * cos(angle);
                                vy -= (25 - radius) * psico * sin(angle);
                            }
                        }
                        // si dibujamos lineas y deberiamos dibujar - dibujamos!
                        if (lines) {
                            if (drawthis) {
                                stroke(strokeColor);
                                //               stroke(255-backColFluid,lineTransparency);
                                line(x,y,tx,ty);
                            }
                        }
                    }
                }
                // aca consideramos la influencia del usuario
                if (hasDancers) {
                    // la posicion del usuario
                    tx = dPos.x;
                    ty = dPos.y;
                    radius = dist(x,y,tx,ty);
                    if (radius < influenceRadius) {
                        angle = atan2(ty-y,tx-x);
                        // atraemos o rebotamos?
                        if (reject) {
                            // rebotamos
                            vx -= radius * 0.07f * cos(angle);
                            vy -= radius * 0.07f * sin(angle);
                        }
                        else {
                            // atraemos
                            vx += radius * 0.07f * cos(angle);
                            vy += radius * 0.07f * sin(angle);
                        }
                    }
                }
                /* Previox x and y coordinates are set, for drawing the trail */
                px = (int)x;
                py = (int)y;
                // actualizamos las coordenadas de la particula
                x += vx;
                y += vy;
                // aplicamos la gravedad
                if (gravity == true) vy += 0.7f;
                // rebotamos contra los bordes
                if (x > width-10) {
                    if (abs(vx) == vx) vx *= -1.0f;
                    x = width-10;
                }
                if (x < 10) {
                    if (abs(vx) != vx) vx *= -1.0f;
                    x = 11;
                }
                if (y < 10) {
                    if (abs(vy) != vy) vy *= -1.0f;
                    y = 10;
                }
                if (y > height-10) {
                    if (abs(vy) == vy) vy *= -1.0f;
                    vx *= 0.6f;
                    y = height-10;
                }
                // si no dibujamos lineas, dibujamos particulas
                if (!lines) {
                    stroke(color(255,0,0));
                    fill(color(255,0,0));
                    strokeWeight(20);
                    //stroke (255 - backColFluid,lineTransparency);
                    line(px,py,PApplet.parseInt(x),PApplet.parseInt(y));
                    //ellipse(px,py,5,5);
                }
            }
        }
    }
}
class LetrasParticulas extends ContentView
{
    BlobDetection blobDetection;
    Kinect kinect;
    int[] background;

    public PGraphics pg;
    final int nbPartsPerLetter = 150;
    final int textCol = color(100, 100, 255);
    final int MARGIN = 5;
    final int STILL = 0;
    final int MOUSE = 1;
    final int BOMB  = 2;
    private int mode = STILL;
    private String word = "La magia de la búsqueda";
    private Particle[] parts;
    private int nbParts;
    private int[] userList;
    PVector dPos;
    Boolean hasDancers = true;
    private int status = 0;

    int particleCount = 150;

    LetrasParticulas(PApplet container, Kinect kinect) {
        super(container);
        process(word);
        this.kinect = kinect;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }

    LetrasParticulas(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect,int[] background) {
        super(container, canvasWidth, canvasHeight);
        process(word);
        this.kinect = kinect;
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }
    public void setStatus(int s){
        this.status = s;
    }
    public void process(String p_word)
    {
        println("process");
        // PFont f = loadFont("SnellRoundhand-Black-48.vlw");
        nbParts = nbPartsPerLetter * p_word.length();
        parts = new Particle[nbParts];
        pg = createGraphics(width, height);
        pg.beginDraw();
        pg.beginShape();
        // pg.textFont(f);
        pg.textSize(72);
        pg.fill(textCol);
        pg.text(p_word, MARGIN + 75, MARGIN + 100, width - MARGIN - 150, height - MARGIN - 100);
        pg.endShape();
        pg.endDraw();

        pg.loadPixels();
        println("init parts");
        for (int i = 0; i < nbParts; i ++)
        {
            parts[i] = new Particle();
        }
        println("end init parts");
        pg.updatePixels();
    }

    public PImage draw()
    {
        background(0);
        // pg = createGraphics(this.canvasWidth, this.canvasHeight);
        // pg.beginDraw();
        // background(0);
        Blob blob = blobDetection.getBlob(background, kinect.getRawDepth());
        // buscamos al usuario
        if (blob != null){
            mode = MOUSE;
            hasDancers = true;
            dPos = blob.getCenter();
            // blob.show(width, height);
            dPos.x = map(dPos.x,0,640,0,width);
            dPos.y = map(dPos.y,0,480,0,height);
            // textSize(32);
            // fill(255, 0, 0);
            // text("Mode: "+mode +" - dPOS: x:"+dPos.x+" y:"+dPos.y,50,50);

            }else{
                mode = STILL;
            }


            //image(context.depthImage(),0,0,1280,1024);

            fill(0, 50);

            // rect(0,0, width, height);

            for (int i = 0; i < nbParts; i ++)
            {
                parts[i].update(mode,i);
                parts[i].display(pg,this.status);
            }
            // pg.endDraw();
            // PImage pi = pg.get();
            // return pi;
            return null;
        }

        public void loadInitialDepth(){
            background = new int[kinect.width*kinect.height];
            arrayCopy(kinect.getRawDepth(),background);
            println("Background loaded");
        }
        class Particle
        {
            private final static float FRICTION1 = .98f; //.98
            private final static float G1 = 0.01f; //0.05
            private final static float CALM1 = .96f; //.92 //speed back to letter
            private final static float FRICTION2 = .98f; //.98
            private final static float G2 = 5;
            private final static float CALM2 = .96f; //Speed to mouse pointer
            private final float MASS = random(1, 3);
            // private final color col = color(random(140, 230), random(50, 120), random(34, 76));
            private final int col = color(255, 255, 250);

            private final float rad = random(1, 3);
            private PVector pos;
            private PVector speed;
            private PVector origin;
            private Boolean found = false;
            private int r,g,b,opacity;
            Particle()
            {
                opacity = 0;
                r = 255;
                g = 255;
                b = 255;
                speed = new PVector(0, 0);
                while (!found)
                {
                    int x = (int)random(width);
                    int y = (int)random(height);
                    if (pg.pixels[y * width + x] == textCol)
                    {
                        pos = new PVector(x, y);
                        origin = pos.get();
                        found = true;
                    }
                }
            }

            public void update(int p_mode,int num)
            {

                PVector target;
                float l;
                switch(p_mode)
                {
                    case MOUSE:
                    float covarX = random(-200,200);
                    float covarY = random(-400,400);
                    // target = new PVector(mouseX+covarX, mouseY+covarY);
                    target = new PVector(dPos.x+covarX, dPos.y+covarY);
                    target.sub(pos);
                    l = target.mag() < 60 ? 500 : target.mag();
                    target.normalize();
                    target.mult(FRICTION2 * G2 * MASS / (sqrt(l)));
                    speed.add(target);
                    speed.mult(CALM2);
                    break;
                    case STILL:
                    target = origin.get();
                    target.sub(pos);
                    target.limit(10);
                    target.mult(FRICTION1 * G1 * MASS);
                    speed.add(target);
                    speed.mult(CALM1);
                    break;
                    case BOMB:
                    target = new PVector(mouseX, mouseY);
                    target.sub(pos);
                    target.limit(70);
                    l = target.mag();
                    target.mult(-40 / l);
                    speed = target.get();
                    break;
                }
            }
            public void display(PGraphics pg,int status) {
                pos.add(speed);
                if (pos.x < MARGIN){
                    pos.x = MARGIN;
                    speed.x *= -1;
                    }else if (pos.x > width - MARGIN){
                        pos.x = width - MARGIN;
                        speed.x *= -1;
                    }

                    if (pos.y < MARGIN){
                        pos.y = MARGIN;
                        speed.y *= -1;
                        } else if (pos.y > height - MARGIN){
                            pos.y = height - MARGIN;
                            speed.y *= -1;
                        }

                        if(status == 0){
                            if((millis() / 500) % 1 == 0){
                                if(opacity+3 < 255) opacity += 3;
                            }
                        }
                        if(status == 1){
                            r = 255;
                            g = 255;
                            b = 255;
                            opacity = 255;
                        }
                        if(status == 2){
                            if(((millis() / 500) % 1 == 0) && (opacity > 0)){
                                opacity -= 3;
                            }

                        }
                        stroke(color(r,g,b,opacity));
                        strokeWeight(3);
                        line(pos.x,pos.y,pos.x-2,pos.y-2);

                    }
                };
            }
class Lineas extends ContentView
{
    public PGraphics pg;
    BlobDetection blobDetection;
    // private SimpleOpenNI context;
    private Kinect kinect;
    // private OpenCV opencv;
    // private KinectProjectorToolkit kpc;
    // ArrayList<ProjectedContour> projectedContours;
    PVector[] pts;
    int[] background;

    float lineWeight = 5;

    //int NUM_PTS = 256;

    float r = 255;
    float g = 255;
    float b = 255;
    float userColorR = 255;
    float userColorG = 255;
    float userColorB = 255;

    float alpha = 180;

    int status = 0;
    int back = 0;
    float opacity = 0;
    float startTime;
    float timeRunning;
    float timeRelative;
    int finished = 0;

    int NUM_PTS = 150;
    float LERP_RATE = 0.05f;

    Lineas(PApplet container, Kinect kinect, int[] background) {
        super(container);
        this.kinect = kinect;
        this.kinect.initDepth();
        this.kinect.enableMirror(false);
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);

        pts = new PVector[NUM_PTS];
        for (int i=0; i<NUM_PTS; i++)
        pts[i] = new PVector(width/2, height/2);
    }
    Lineas(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background){
        super(container, canvasWidth, canvasHeight);
        this.kinect = kinect;
        this.kinect.initDepth();
        this.kinect.enableMirror(false);
        this.background = background;

        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);

        pts = new PVector[NUM_PTS];
        for (int i=0; i<NUM_PTS; i++)
        pts[i] = new PVector(width/2, height/2);
    }
    public void setStartTime(){
        this.startTime = millis();
        this.timeRelative = (millis() - startTime) / 1000;
    }
    public int isFinished(){
        return finished;
    }
    //0:00 = 0sec
    //0:00 a 0:10 = 10sec transicion IN
    //0:10 a 2:14 = 134sec to mainContent
    //2:14 a 2:45 = 165sec transicion OUT
    //2:45 todo rojo
    public PImage draw(){
        background(0);
        timeRunning = (millis() - startTime) / 1000;
        println(timeRunning);
        switch (this.status){
            case 0:
            this.transitionIN();
            if(timeRunning >= 20) {
                this.status = 1;
            }
            break;
            case 1:
            this.mainContent();
            if(timeRunning > 134){
                this.status = 2;
            }
            break;
            case 2:
            this.transitionOUT();
            //2 min 45 sec = 165sec
            //193 en reunion
            if(timeRunning > 195){
                finished = 1;
            }

            break;
        }
        return null;
    }

    public void transitionIN(){
        lineWeight = 5;
        if(timeRunning - timeRelative >= 0.25f) {
            opacity = (opacity < 251.46f) ? opacity + 3.54f : 255;
            timeRelative = timeRunning;
        }
        this.drawScene();
    }
    public void mainContent(){
        opacity = 255;
        this.drawScene();
    }
    public void transitionOUT(){
        opacity = 255;

        if(timeRunning - timeRelative >= 0.25f) {
            if(timeRunning < 185) {
                lineWeight += 0.2f;
                timeRelative = timeRunning;
            }
            else {
                lineWeight += 3;
                timeRelative = timeRunning;
            }
            if(g > 0) g -= 1.2f;
            if(b > 0) b -= 1.2f;
            if(userColorG > 0) userColorG -= 1.2f;
            if(userColorB > 0) userColorB -= 1.2f;
        }
        this.drawScene();

    }
    public void drawScene()
    {
        ArrayList<PVector> contour = blobDetection.getContour(background, kinect.getRawDepth(), false);

        if (contour != null){
            PShape user = createShape();
            // stroke(color(0,0,0));
            user.beginShape();
            fill(color(userColorR, userColorG, userColorB), opacity);

            for (int j=0; j<NUM_PTS; j++) {
                float ang = map(j, 0, NUM_PTS, PI/2, TWO_PI+PI/2);

                //        float ang = (0.01*frameCount + map(j, 0, NUM_PTS, 0, TWO_PI)) % TWO_PI;
                PVector p1 = new PVector(width/2 + width * cos(ang), height/2 + width * sin(ang));
                PVector p2 = contour.get((int) map(j, 0, NUM_PTS-1, 0, contour.size()-1));
                pts[j] = PVector.lerp(pts[j], p2, LERP_RATE); //new PVector(lerp(pts[j].x, p2.x, LERP_RATE), lerp(pts[j].y, p2.y, LERP_RATE));

                strokeWeight(lineWeight);
                stroke(color(r, g, b), opacity);

                line(map(p1.x,0,640,0,width), map(p1.y,0,480,0,height), map(pts[j].x,0,640,0,width), map(pts[j].y,0,480,0,height));

                user.vertex(map(pts[j].x,0,640,0,width), map(pts[j].y,0,480,0,height));

            }

            user.noStroke();
            user.endShape(CLOSE);
            shape(user);
        }
    }

    public void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
        println("Background loaded");
    }

    public void setStatus(int status){
        this.status = status;
    }
}
  public void settings() {  fullScreen(P3D, 1); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Controlador" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
