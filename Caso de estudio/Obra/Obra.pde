import org.openkinect.freenect.*;
import org.openkinect.processing.*;
// import controlP5.*;
// import KinectProjectorToolkit.*;
import gab.opencv.*;
import processing.sound.*;
import processing.video.*;

final int SCENE_PARTICLES = 0;
final int SCENE_VIDEO = 1;
final int SCENE_LINES = 2;
final int SCENE_PANAL = 3;
final int SCENE_PICTURE = 4;

private ContentView contentLetras;
private ContentView contentFluid;
private ContentView contentLineas;
private ContentView contentVideo;
private ContentView contentPicture;
private ContentView content;
private PImage ret;
private int status = 0;
private Kinect kinect;
private Boolean showDepth = false;
private int scene = SCENE_PARTICLES;
private int[] background;
private SoundFile rue;
private SoundFile divenire;

void setup(){
    // size(1024, 768, FX2D);
    fullScreen(P3D, 1);
    noCursor();
    // context = new SimpleOpenNI(this);
    // this.background = context.depthMap();
    kinect = new Kinect(this);
    kinect.initDepth();
    kinect.enableMirror(false);
    background = new int[kinect.width*kinect.height];
    arrayCopy(kinect.getRawDepth(),background);

    contentLetras = new LetrasParticulas(this, width, height, kinect, background);
    contentVideo = new ContentVideo(this, width, height);
    contentLineas = new Lineas(this, width, height, kinect, background);
    contentFluid = new FluidWithBlobs(this, width, height, kinect, background);
    PImage backPic = loadImage("fondoEstrellas.jpg");
    contentPicture = new ContentMePicture(this, width, height, kinect, background, backPic);
    content = contentLetras;

    rue = new SoundFile(this, "music.mp3");
    // rue = new SoundFile(this, "rue-des-cascades-s.mp3");
    // rue.rate(0.72);
    // divenire = new SoundFile(this, "divenire.mp3");
}
void draw(){
    ret = content.draw();
    if (ret != null){
        image(ret,0,0,width,height);
    }
    if(showDepth){
        image(kinect.getDepthImage(),0,0,320,240);
        println(frameRate);
    }
    if (content.isFinished() == 1) {
        this.nextScene();
    }
    // if(rue.isPlaying() && rue.duration()){
    //
    // }
    // stroke(255);
    // fill(255);
    // textSize(30);
    // text(frameRate, 20, 1000);
}
void keyPressed(){
    //START - transition in
    if (key == '0') {
        status = 0;
    }
    //MAIN
    if (key == '1') {
        status = 1;
    }
    //END - transition Out
    if (key == '2') {
        status = 2;
    }
    // if (key == 'g') gravity = !gravity;
    // if (key == 'l') lines = !lines;
    if (key == 'I' || key == 'i') {

            println("Background loaded");
        if (contentLetras != null){
            contentLetras.loadInitialDepth();
        }
        if (contentFluid != null){
            contentFluid.loadInitialDepth();
        }
        if (contentLineas != null){
            contentLineas.loadInitialDepth();
        }
        if (contentPicture != null){
            contentPicture.loadInitialDepth();
        }

    }
    if ((key == 'd') || (key == 'D')) showDepth = !showDepth;
    // if (key == 't') psico+= 0.005;
    // if (key == 'f') psico-= 0.005;
    // if (keyCode == UP) influenceRadius+=5;
    // if (keyCode == DOWN) influenceRadius-=5;
    // if (keyCode == RIGHT) sWeightFluid ++;
    // if (keyCode == LEFT) sWeightFluid --;
    if (keyCode == RIGHT) {
        this.nextScene();
    }
    content.setStatus(status);
}
void nextScene(){
    status = 0;
    scene = (scene+1) % 6;
    switch (scene){
        case SCENE_PARTICLES:
        kinect = new Kinect(this);
        content =  contentLetras;
        break;
        case SCENE_VIDEO:
        contentLetras = null;
        contentVideo.rewindMovie();
        content = contentVideo;
        break;
        case SCENE_LINES:
        contentVideo.rewindMovie();
        contentVideo = null;
        content = contentLineas;
        rue.play();
        content.setStartTime();
        break;
        case SCENE_PANAL:
        contentLineas = null;
        content = contentFluid;
        content.setStartTime();
        break;
        case SCENE_PICTURE:
        contentFluid = null;
        content = contentPicture;
        content.setStartTime();
        break;
    }
}
