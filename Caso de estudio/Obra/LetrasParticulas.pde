class LetrasParticulas extends ContentView
{
    BlobDetection blobDetection;
    Kinect kinect;
    int[] background;

    public PGraphics pg;
    final int nbPartsPerLetter = 150;
    final color textCol = color(100, 100, 255);
    final int MARGIN = 5;
    final int STILL = 0;
    final int MOUSE = 1;
    final int BOMB  = 2;
    private int mode = STILL;
    private String word = "La magia de la búsqueda";
    private Particle[] parts;
    private int nbParts;
    private int[] userList;
    PVector dPos;
    Boolean hasDancers = true;
    private int status = 0;

    int particleCount = 150;

    LetrasParticulas(PApplet container, Kinect kinect) {
        super(container);
        process(word);
        this.kinect = kinect;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }

    LetrasParticulas(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect,int[] background) {
        super(container, canvasWidth, canvasHeight);
        process(word);
        this.kinect = kinect;
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }
    void setStatus(int s){
        this.status = s;
    }
    void process(String p_word)
    {
        println("process");
        // PFont f = loadFont("SnellRoundhand-Black-48.vlw");
        nbParts = nbPartsPerLetter * p_word.length();
        parts = new Particle[nbParts];
        pg = createGraphics(width, height);
        pg.beginDraw();
        pg.beginShape();
        // pg.textFont(f);
        pg.textSize(72);
        pg.fill(textCol);
        pg.text(p_word, MARGIN + 75, MARGIN + 100, width - MARGIN - 150, height - MARGIN - 100);
        pg.endShape();
        pg.endDraw();

        pg.loadPixels();
        println("init parts");
        for (int i = 0; i < nbParts; i ++)
        {
            parts[i] = new Particle();
        }
        println("end init parts");
        pg.updatePixels();
    }

    PImage draw()
    {
        background(0);
        // pg = createGraphics(this.canvasWidth, this.canvasHeight);
        // pg.beginDraw();
        // background(0);
        Blob blob = blobDetection.getBlob(background, kinect.getRawDepth());
        // buscamos al usuario
        if (blob != null){
            mode = MOUSE;
            hasDancers = true;
            dPos = blob.getCenter();
            // blob.show(width, height);
            dPos.x = map(dPos.x,0,640,0,width);
            dPos.y = map(dPos.y,0,480,0,height);
            // textSize(32);
            // fill(255, 0, 0);
            // text("Mode: "+mode +" - dPOS: x:"+dPos.x+" y:"+dPos.y,50,50);

            }else{
                mode = STILL;
            }


            //image(context.depthImage(),0,0,1280,1024);

            fill(0, 50);

            // rect(0,0, width, height);

            for (int i = 0; i < nbParts; i ++)
            {
                parts[i].update(mode,i);
                parts[i].display(pg,this.status);
            }
            // pg.endDraw();
            // PImage pi = pg.get();
            // return pi;
            return null;
        }

        void loadInitialDepth(){
            background = new int[kinect.width*kinect.height];
            arrayCopy(kinect.getRawDepth(),background);
            println("Background loaded");
        }
        class Particle
        {
            private final static float FRICTION1 = .98; //.98
            private final static float G1 = 0.01; //0.05
            private final static float CALM1 = .96; //.92 //speed back to letter
            private final static float FRICTION2 = .98; //.98
            private final static float G2 = 5;
            private final static float CALM2 = .96; //Speed to mouse pointer
            private final float MASS = random(1, 3);
            // private final color col = color(random(140, 230), random(50, 120), random(34, 76));
            private final color col = color(255, 255, 250);

            private final float rad = random(1, 3);
            private PVector pos;
            private PVector speed;
            private PVector origin;
            private Boolean found = false;
            private int r,g,b,opacity;
            Particle()
            {
                opacity = 0;
                r = 255;
                g = 255;
                b = 255;
                speed = new PVector(0, 0);
                while (!found)
                {
                    int x = (int)random(width);
                    int y = (int)random(height);
                    if (pg.pixels[y * width + x] == textCol)
                    {
                        pos = new PVector(x, y);
                        origin = pos.get();
                        found = true;
                    }
                }
            }

            void update(int p_mode,int num)
            {

                PVector target;
                float l;
                switch(p_mode)
                {
                    case MOUSE:
                    float covarX = random(-200,200);
                    float covarY = random(-400,400);
                    // target = new PVector(mouseX+covarX, mouseY+covarY);
                    target = new PVector(dPos.x+covarX, dPos.y+covarY);
                    target.sub(pos);
                    l = target.mag() < 60 ? 500 : target.mag();
                    target.normalize();
                    target.mult(FRICTION2 * G2 * MASS / (sqrt(l)));
                    speed.add(target);
                    speed.mult(CALM2);
                    break;
                    case STILL:
                    target = origin.get();
                    target.sub(pos);
                    target.limit(10);
                    target.mult(FRICTION1 * G1 * MASS);
                    speed.add(target);
                    speed.mult(CALM1);
                    break;
                    case BOMB:
                    target = new PVector(mouseX, mouseY);
                    target.sub(pos);
                    target.limit(70);
                    l = target.mag();
                    target.mult(-40 / l);
                    speed = target.get();
                    break;
                }
            }
            void display(PGraphics pg,int status) {
                pos.add(speed);
                if (pos.x < MARGIN){
                    pos.x = MARGIN;
                    speed.x *= -1;
                    }else if (pos.x > width - MARGIN){
                        pos.x = width - MARGIN;
                        speed.x *= -1;
                    }

                    if (pos.y < MARGIN){
                        pos.y = MARGIN;
                        speed.y *= -1;
                        } else if (pos.y > height - MARGIN){
                            pos.y = height - MARGIN;
                            speed.y *= -1;
                        }

                        if(status == 0){
                            if((millis() / 500) % 1 == 0){
                                if(opacity+3 < 255) opacity += 3;
                            }
                        }
                        if(status == 1){
                            r = 255;
                            g = 255;
                            b = 255;
                            opacity = 255;
                        }
                        if(status == 2){
                            if(((millis() / 500) % 1 == 0) && (opacity > 0)){
                                opacity -= 3;
                            }

                        }
                        stroke(color(r,g,b,opacity));
                        strokeWeight(3);
                        line(pos.x,pos.y,pos.x-2,pos.y-2);

                    }
                };
            }
