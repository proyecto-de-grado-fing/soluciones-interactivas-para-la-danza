class FluidWithBlobs extends ContentView{
    Fluid f;
    boolean hasDancers;
    public PGraphics pg;
    boolean reject = false;
    int backColFluid = color(0);
    float strokeColorR = 255;
    float strokeColorG = 0;
    float strokeColorB = 0;
    int strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
    float sWeightFluid = 200;
    PVector dPos;
    boolean lines = true;
    int influenceRadius = 1;
    boolean gravity = false;
    float psico = 0.02;
    //
    BlobDetection blobDetection;
    Kinect kinect;
    int[] background;
    int opacity = 255;

    int status = 0;
    PImage ret;
    float startTime;
    float timeRunning;
    int finished = 0;
    float timeRelative;

    int loopCount = 0;
    int loopMod = 8;

    FluidWithBlobs(PApplet container, Kinect kinect){
        super(container);
        f = new Fluid();
        f.initialScene();
        this.kinect = kinect;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }
    FluidWithBlobs(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background){
        super(container,canvasWidth,canvasHeight);
        f = new Fluid();
        f.initialScene();
        this.kinect = kinect;
        // this.kinect.initDepth();
        // this.kinect.enableMirror(false);
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
    }

    void setStatus(int status){
        this.status = status;
    }
    void setStartTime(){
        this.startTime = millis();
        this.timeRelative = (millis() - startTime) / 1000;
    }

    PImage draw(){
        background(0);
        timeRunning = (millis() - startTime) / 1000;
        println(timeRunning);
        switch (this.status){
            case 0:
            ret = this.transitionIN();
            if(timeRunning >= 29){
                this.status = 1;
            }
            break;
            case 1:
            ret = this.mainContent();
            if(timeRunning >= 209){
                this.opacity = 0;
                this.status = 2;
            }
            break;
            case 2:
            ret = this.transitionOUT();
            if(timeRunning > 234) {
                finished = 1;
            }
            break;
        }
        return ret;
    }

    PImage transitionIN(){
        if(timeRunning - timeRelative >= 0.25){
            if (timeRunning < 10){
                sWeightFluid = (sWeightFluid > 6.5) ? sWeightFluid - 3.5 : 3;
            }
            if (timeRunning >= 10 && timeRunning < 20){
                sWeightFluid = (sWeightFluid > 4.5) ? sWeightFluid - 1.5 : 3;
                influenceRadius = (influenceRadius < 120) ? influenceRadius + 4 : 120;
            }
            else {
                sWeightFluid = (sWeightFluid > 3.55) ? sWeightFluid - 0.55 : 3;
                influenceRadius = (influenceRadius < 120) ? influenceRadius + 2 : 120;
            }
            timeRelative = timeRunning;
        }
        f.drawScene(pg);
        return null;
    }
    PImage mainContent(){
        opacity = 255;
        if (timeRunning >= 68 && timeRunning < 129){
            this.status = 1;
            this.influenceRadius = 200;
            this.reject = false;
        }
        else if (timeRunning >= 129 && timeRunning < 152){
            this.reject = true;
            this.influenceRadius = 120;
            this.gravity = true;
        }
        else if (timeRunning >= 152 && timeRunning < 182){

            if(timeRunning - timeRelative >= 0.25){
                strokeColorR = (strokeColorR > 2.225) ? strokeColorR - 2.225 : 0;
                strokeColorB = (strokeColorB < 252.775) ? strokeColorB + 2.225 : 255;
                timeRelative = timeRunning;
                loopCount ++;
                if (gravity){
                    loopMod = 3;
                } else {
                    loopMod = 8;
                }
                if (loopCount % loopMod == 0){
                    this.gravity = !this.gravity;
                    loopCount = 0;
                }
            }
        }
        else if (timeRunning >= 182 && timeRunning < 201){
            strokeColorR =  0;
            strokeColorB =  255;
            this.gravity = false;
            this.reject = true;
        }
        strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
        f.drawScene(pg);
        return null;
    }
    PImage transitionOUT(){
        // 16 sec
        if(timeRunning - timeRelative >= 0.25){
            strokeColorR = (strokeColorR < 16.73) ? strokeColorR + 0.27 : 17;
            strokeColorG = (strokeColorG < 50.2) ? strokeColorG + 0.8 : 51;
            strokeColorB = (strokeColorB > 79.8) ? strokeColorB - 2.8 : 77;
            this.opacity = (this.opacity < 251) ? this.opacity + 4 : 255;
            timeRelative = timeRunning;
        }
        strokeColor = color(strokeColorR, strokeColorG, strokeColorB);
        f.drawScene(pg);
        fill(17, 51, 77, this.opacity);
        noStroke();
        rect(0, 0 ,this.canvasWidth,this.canvasHeight);
        return null;
    }
    int isFinished(){
        return finished;
    }
    void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
    }

    class Fluid
    {
        // en total 1500 particulas
        int particleCount = 2000;
        Particle[] particles = new Particle[particleCount];

        public Fluid(){};

        void closeScene(){};
        void initialScene(){
            reject = true;
            for (int i = 0; i < particleCount; i++) {
                particles[i] = new Particle();
            }
        };
        void drawScene(PGraphics pg){
            //    blendMode(BLEND);
            // background(backColFluid);
            // image(kinect.getDepthImage(),0,0, width, height);

            Blob blob = blobDetection.getBlob(background, kinect.getRawDepth());
            // buscamos al usuario
            if (blob != null){
                hasDancers = true;
                dPos = blob.getTop();
                // blob.show(width, height);
                dPos.x = map(dPos.x,0,640,0,width);
                dPos.y = map(dPos.y,0,480,0,height);
                // definimos color del fondo y del trazo
                fill(backColFluid);
                stroke(255);
                //    stroke (255 - backColFluid);
                // definimos el grosor del trazo
                strokeWeight(sWeightFluid);
                // actualizamos las posiciones de las particulas
                for (int i = 0; i < particleCount; i++) {
                    Particle particle = (Particle) particles[i];
                    particle.update(i);
                }
            }
            // return pg;
        };
        String getSceneName(){return "Fluid";};

        class Particle {
            // coordenada x,y
            float x;
            float y;
            // velocidad en eje x y y
            float vx;
            float vy;
            // se incia en una posicion randomin=ca en la pantalla
            Particle() {
                x = random(10,width-10);
                y = random(10,height-10);
            }
            // devuelve la posicion x
            float getx() {
                return x;
            }
            // devuelve la posicion y
            float gety() {
                return y;
            }

            // para definir si dibujamos la linea o no
            boolean drawthis;
            Particle particle;
            float tx;
            float ty;
            float radius;
            float angle;
            int px;
            int py;
            int i;
            void update(int num) {
                // simulamos la friccion
                vx *= 0.90;
                vy *= 0.90;
                // vx *= 0.87;
                // vy *= 0.87;
                // Loopeamos para checkear la influencia entre si de las particulas
                for (i = 0; i < particleCount; i++) {
                    // i = num no nos interesa porque seria calcular influencia de uno a uno mismo
                    if (i != num) {
                        drawthis = false;
                        // particula vecina
                        particle = (Particle) particles[i];
                        tx = particle.getx();
                        ty = particle.gety();
                        // la distancia entre la particula actual y la particula vecina
                        radius = dist(x,y,tx,ty);
                        // las lineas las dibujamos solo entre las particulas en radio menor de 35
                        if (radius < 35) {
                            drawthis = true;
                            // el angulo para poder dibujar la linea
                            angle = atan2(y-ty,x-tx);
                            // si esta muy cerca - se desvia
                            if (radius < 30) {
                                // cambiamos las velocidades
                                vx += (30 - radius) * 0.07 * cos(angle);
                                vy += (30 - radius) * 0.07 * sin(angle);
                            }
                            if (radius > 25) {
                                // si estan entre 25 y 30 se acercan
                                // vx -= (25 - radius) * 0.005 * cos(angle);
                                // vy -= (25 - radius) * 0.005 * sin(angle);
                                vx -= (25 - radius) * psico * cos(angle);
                                vy -= (25 - radius) * psico * sin(angle);
                            }
                        }
                        // si dibujamos lineas y deberiamos dibujar - dibujamos!
                        if (lines) {
                            if (drawthis) {
                                stroke(strokeColor);
                                //               stroke(255-backColFluid,lineTransparency);
                                line(x,y,tx,ty);
                            }
                        }
                    }
                }
                // aca consideramos la influencia del usuario
                if (hasDancers) {
                    // la posicion del usuario
                    tx = dPos.x;
                    ty = dPos.y;
                    radius = dist(x,y,tx,ty);
                    if (radius < influenceRadius) {
                        angle = atan2(ty-y,tx-x);
                        // atraemos o rebotamos?
                        if (reject) {
                            // rebotamos
                            vx -= radius * 0.07 * cos(angle);
                            vy -= radius * 0.07 * sin(angle);
                        }
                        else {
                            // atraemos
                            vx += radius * 0.07 * cos(angle);
                            vy += radius * 0.07 * sin(angle);
                        }
                    }
                }
                /* Previox x and y coordinates are set, for drawing the trail */
                px = (int)x;
                py = (int)y;
                // actualizamos las coordenadas de la particula
                x += vx;
                y += vy;
                // aplicamos la gravedad
                if (gravity == true) vy += 0.7;
                // rebotamos contra los bordes
                if (x > width-10) {
                    if (abs(vx) == vx) vx *= -1.0;
                    x = width-10;
                }
                if (x < 10) {
                    if (abs(vx) != vx) vx *= -1.0;
                    x = 11;
                }
                if (y < 10) {
                    if (abs(vy) != vy) vy *= -1.0;
                    y = 10;
                }
                if (y > height-10) {
                    if (abs(vy) == vy) vy *= -1.0;
                    vx *= 0.6;
                    y = height-10;
                }
                // si no dibujamos lineas, dibujamos particulas
                if (!lines) {
                    stroke(color(255,0,0));
                    fill(color(255,0,0));
                    strokeWeight(20);
                    //stroke (255 - backColFluid,lineTransparency);
                    line(px,py,int(x),int(y));
                    //ellipse(px,py,5,5);
                }
            }
        }
    }
}
