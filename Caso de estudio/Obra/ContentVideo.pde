class ContentVideo extends ContentView
{
    Movie movie;
    Movie myMovie;
    int status = 0;
    int canvasWidth;
    int canvasHeight;
    int opacity;
    PGraphics pg;
    PApplet container;
    int finished = 0;
    int startTime;

    ContentVideo(PApplet container, int canvasWidth, int canvasHeight) {
        super(container, canvasWidth, canvasHeight);
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        myMovie = new Movie(container, "2.mp4");
        myMovie.stop();
        opacity = 255;
    }

    ContentVideo(PApplet container) {
        super(container);
        myMovie = new Movie(container, "2.mp4");
        myMovie.stop();
        opacity = 255;
    }
    void setStatus(int s){
        this.status = s;
    }
    void rewindMovie(){
        myMovie.stop();
    }
    int isFinished(){
        return finished;
    }

    @Override
    PImage draw() {
        myMovie.play();
        if (myMovie.available()) {
            myMovie.read();
        }
        if(this.status==0){
            if((millis() / 300) % 1 == 0){
                if(opacity-8 > 0) {
                    opacity -= 8;
                }else{
                    opacity = 0;
                    this.status = 1;
                }
            }
        }
        if (this.status == 1){
            if(myMovie.time() >= (myMovie.duration())){
                this.status=2;
            }
        }
        if(this.status==2){
            if((millis() / 300) % 1 == 0){
                if(opacity+8 < 255) {
                    opacity += 8;
                }else{
                    opacity = 255;
                    finished = 1;
                }
            }
        }
        // pg = createGraphics(this.canvasWidth, this.canvasHeight,JAVA2D);
        // pg.beginDraw();
        image(myMovie.get(),0,0,this.canvasWidth,this.canvasHeight);
        fill(0,0,0,opacity);
        noStroke();
        rect(0,0,this.canvasWidth,this.canvasHeight);
        // pg.fill(0,0,0,opacity);
        // pg.rect(0,0,this.canvasWidth,this.canvasHeight);
        // pg.endDraw();
        // PImage pi = pg.get();
        return null;
    }
}
