class Lineas extends ContentView
{
    public PGraphics pg;
    BlobDetection blobDetection;
    // private SimpleOpenNI context;
    private Kinect kinect;
    // private OpenCV opencv;
    // private KinectProjectorToolkit kpc;
    // ArrayList<ProjectedContour> projectedContours;
    PVector[] pts;
    int[] background;

    float lineWeight = 5;

    //int NUM_PTS = 256;

    float r = 255;
    float g = 255;
    float b = 255;
    float userColorR = 255;
    float userColorG = 255;
    float userColorB = 255;

    float alpha = 180;

    int status = 0;
    int back = 0;
    float opacity = 0;
    float startTime;
    float timeRunning;
    float timeRelative;
    int finished = 0;

    int NUM_PTS = 150;
    float LERP_RATE = 0.05;

    Lineas(PApplet container, Kinect kinect, int[] background) {
        super(container);
        this.kinect = kinect;
        this.kinect.initDepth();
        this.kinect.enableMirror(false);
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);

        pts = new PVector[NUM_PTS];
        for (int i=0; i<NUM_PTS; i++)
        pts[i] = new PVector(width/2, height/2);
    }
    Lineas(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background){
        super(container, canvasWidth, canvasHeight);
        this.kinect = kinect;
        this.kinect.initDepth();
        this.kinect.enableMirror(false);
        this.background = background;

        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);

        pts = new PVector[NUM_PTS];
        for (int i=0; i<NUM_PTS; i++)
        pts[i] = new PVector(width/2, height/2);
    }
    void setStartTime(){
        this.startTime = millis();
        this.timeRelative = (millis() - startTime) / 1000;
    }
    int isFinished(){
        return finished;
    }
    //0:00 = 0sec
    //0:00 a 0:10 = 10sec transicion IN
    //0:10 a 2:14 = 134sec to mainContent
    //2:14 a 2:45 = 165sec transicion OUT
    //2:45 todo rojo
    PImage draw(){
        background(0);
        timeRunning = (millis() - startTime) / 1000;
        println(timeRunning);
        switch (this.status){
            case 0:
            this.transitionIN();
            if(timeRunning >= 20) {
                this.status = 1;
            }
            break;
            case 1:
            this.mainContent();
            if(timeRunning > 134){
                this.status = 2;
            }
            break;
            case 2:
            this.transitionOUT();
            //2 min 45 sec = 165sec
            //193 en reunion
            if(timeRunning > 195){
                finished = 1;
            }

            break;
        }
        return null;
    }

    void transitionIN(){
        lineWeight = 5;
        if(timeRunning - timeRelative >= 0.25) {
            opacity = (opacity < 251.46) ? opacity + 3.54 : 255;
            timeRelative = timeRunning;
        }
        this.drawScene();
    }
    void mainContent(){
        opacity = 255;
        this.drawScene();
    }
    void transitionOUT(){
        opacity = 255;

        if(timeRunning - timeRelative >= 0.25) {
            if(timeRunning < 185) {
                lineWeight += 0.2;
                timeRelative = timeRunning;
            }
            else {
                lineWeight += 3;
                timeRelative = timeRunning;
            }
            if(g > 0) g -= 1.2;
            if(b > 0) b -= 1.2;
            if(userColorG > 0) userColorG -= 1.2;
            if(userColorB > 0) userColorB -= 1.2;
        }
        this.drawScene();

    }
    void drawScene()
    {
        ArrayList<PVector> contour = blobDetection.getContour(background, kinect.getRawDepth(), false);

        if (contour != null){
            PShape user = createShape();
            // stroke(color(0,0,0));
            user.beginShape();
            fill(color(userColorR, userColorG, userColorB), opacity);

            for (int j=0; j<NUM_PTS; j++) {
                float ang = map(j, 0, NUM_PTS, PI/2, TWO_PI+PI/2);

                //        float ang = (0.01*frameCount + map(j, 0, NUM_PTS, 0, TWO_PI)) % TWO_PI;
                PVector p1 = new PVector(width/2 + width * cos(ang), height/2 + width * sin(ang));
                PVector p2 = contour.get((int) map(j, 0, NUM_PTS-1, 0, contour.size()-1));
                pts[j] = PVector.lerp(pts[j], p2, LERP_RATE); //new PVector(lerp(pts[j].x, p2.x, LERP_RATE), lerp(pts[j].y, p2.y, LERP_RATE));

                strokeWeight(lineWeight);
                stroke(color(r, g, b), opacity);

                line(map(p1.x,0,640,0,width), map(p1.y,0,480,0,height), map(pts[j].x,0,640,0,width), map(pts[j].y,0,480,0,height));

                user.vertex(map(pts[j].x,0,640,0,width), map(pts[j].y,0,480,0,height));

            }

            user.noStroke();
            user.endShape(CLOSE);
            shape(user);
        }
    }

    void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
        println("Background loaded");
    }

    void setStatus(int status){
        this.status = status;
    }
}
