class ContentMePicture extends ContentView
{
    Kinect kinect;
    BlobDetection blobDetection;
    int[] background;

    PImage backPic;
    PImage bImg;
    PImage img;
    // PImage bigImg;

    int canvasWidth;
    int canvasHeight;
    int status = 0;
    int finished = 0;
    int backgroundColor = color(17,51,77);
    float backgroundTransparency = 255;
    float startTime,timeRelative,timeRunning;
    float opacity = 255;

    ContentMePicture(PApplet container, int canvasWidth, int canvasHeight, Kinect kinect, int[] background, PImage backPic){
        super(container, canvasWidth, canvasHeight);
        this.kinect = kinect;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.background = background;
        blobDetection = new BlobDetection(container, this.kinect.width, this.kinect.height);
        //Load image and redim to fit kinect size
        // backPic = backPic;
        PImage img1 = createImage(640, 480,ARGB);
        img1.copy(backPic, 0, 0, backPic.width, backPic.height, 0, 0, kinect.width, kinect.height);
        this.backPic = img1;
    };

    void setStatus(int s){
        this.status = s;
    }
    int isFinished(){
        return finished;
    }
    void setStartTime(){
        this.startTime = millis();
        this.timeRelative = (millis() - startTime) / 1000;
    }

    @Override
    PImage draw() {
        fill(backgroundColor, backgroundTransparency);
        noStroke();
        rect(0, 0, width, height);
        timeRunning = (millis() - startTime) / 1000;
        println(timeRunning);

        switch (this.status){
            case 0:
            this.transitionIN();
            if(timeRunning >= 15) {
                opacity = 0;
                this.status = 1;
            }
            break;
            case 1:
            this.mainContent();
            if(timeRunning >= 96){
                this.status = 2;
            }
            break;
            case 2:
            this.transitionOUT();
            // if(timeRunning > 96) {
            //     finished = 1;
            // }
            break;
        }
        if (this.status != 2){
            bImg = getMeImg();
            image(bImg,0,0);
        }

        fill(backgroundColor, opacity);
        rect(0, 0, width, height);
        return null;
    };

    void transitionIN(){
        if (timeRunning > 5){
            if(timeRunning - timeRelative >= 0.25) {
                //Foreground rect to fade in
                opacity = (opacity > 6.4) ? opacity - 6.4 : 0;
                timeRelative = timeRunning;
            }
        }
    }

    void mainContent(){
        //Set params para stela incremental
        if(timeRunning - timeRelative >= 1) {
            backgroundTransparency = (backgroundTransparency > 4.5) ? backgroundTransparency - 4.5 : 0;
            timeRelative = timeRunning;
        }
        if(timeRunning >= 78){
            backgroundTransparency = 0;
            //set params para que quede lo q limpia
        }
    }

    void transitionOUT(){
        backgroundColor = color(0, 0, 0);

        if(timeRunning - timeRelative >= 0.25) {
            opacity = (opacity < 255) ? opacity + 0.5 : 255;
            timeRelative = timeRunning;
        }
    }

    PImage getMeImg(){
        // PImage img = new PImage(this.kinect.width, this.kinect.height, ARGB);
        // img.loadPixels();
        //
        // int index;
        //
        // Blob blob = blobDetection.getBlob(background, kinect.getRawDepth());
        // if (blob != null){
        //     for (PVector v : blob.getPoints()){
        //         index = (int) (v.x + v.y * this.kinect.width);
        //         img.pixels[index] = backPic.pixels[index];
        //     }
        // }
        // img.updatePixels();
        // img.resize(canvasWidth, canvasHeight);
        // return img;


        PGraphics img = createGraphics(kinect.width, kinect.height);
        img.beginDraw();
        PShape user = createShape();
        user.beginShape();
        ArrayList<PVector> contour = blobDetection.getContour(background, kinect.getRawDepth(), true);
        if (contour != null){
            user.fill(255,255,255);
            user.noStroke();
            for (PVector point : contour){
                user.vertex(point.x, point.y);
            }
        }
        user.endShape(CLOSE);
        img.shape(user);
        img.endDraw();
        PImage cleanBack = backPic.copy();
        cleanBack.mask(img);
        cleanBack.resize(canvasWidth, canvasHeight);
        return cleanBack;
    }

    void loadInitialDepth(){
        background = new int[kinect.width*kinect.height];
        arrayCopy(kinect.getRawDepth(),background);
    }

}
